﻿using System.Web.Http;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Services;
using System.Threading.Tasks;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Newtonsoft.Json;

namespace Seagull2.Questionnaire.WebApi.Controller
{
    /// <summary>
    ///问卷 控制器
    /// </summary>
    [AllowAnonymous]
    public class QuestionnaireController : BaseController
    {
        private readonly IQuestionnaireService _questionnaireService;
        public QuestionnaireController(IQuestionnaireService questionnaireService)
        {
            _questionnaireService = questionnaireService;
        }
        /// <summary>
        ///加载问卷
        /// </summary>
        /// <param name="pagination">number为空时，的分页信息</param>
        /// <param name="number">问卷编号，不为空时。只加载此问卷</param>
        /// <returns></returns> 
        [HttpGet]
        public async Task<IHttpActionResult> Load([FromUri]PaginationInfo pagination, string serialNumber = null)
        {
            if (string.IsNullOrWhiteSpace(serialNumber))
            {
                return Ok(await _questionnaireService.Load(pagination, CurrentSeagull2User()));
            }
            var data = await _questionnaireService.Load(serialNumber, CurrentSeagull2User(), true);
            return Ok(data == null ? new QuestionnairesModel() : data);
        }
        /// <summary>
        /// 添加或修改
        /// </summary>
        /// <param name="model">数据</param>
        /// <param name="operationType">操作类型</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> AddOrUpdate(QuestionnairesModel model, OperationType operationType)
        {
            if (OperationType.UpdateStatus == operationType)
            {
                return Ok(await _questionnaireService.SetQuestionnairesStatus(model, CurrentSeagull2User()));
            }
            var result = await _questionnaireService.AddOrUpdate(model, operationType, CurrentSeagull2User());
            return Ok(result);
        }
    }
}

