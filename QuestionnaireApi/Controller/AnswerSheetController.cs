﻿using Seagull2.Owin.File.Models;
using Seagull2.Owin.File.Services;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Seagull2.Questionnaire.WebApi.Controller
{
    [AllowAnonymous]
    public class AnswerSheetController : BaseController
    {
        private readonly IConsequenceService _consequenceService;
        private readonly IQuestionnaireService _questionnaireService;
        private readonly IClientFileService _fileService;
        private readonly IOptionResultService _choiceOption_ResultService;
        public AnswerSheetController(
            IConsequenceService consequenceService,
            IQuestionnaireService questionnaireService,
            IClientFileService fileService,
            IOptionResultService choiceOption_ResultService
            )
        {
            _consequenceService = consequenceService;
            _questionnaireService = questionnaireService;
            _fileService = fileService;
            _choiceOption_ResultService = choiceOption_ResultService;
        }
        /// <summary>
        /// 提交答案
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<IHttpActionResult> Dlivery(QuestionnairesModel model)
        {
            var questionnaire = await _questionnaireService.CheckSingleQuestionnaires(model.SerialNumber);
            if (questionnaire.Status != QuestionnaireStatus.Running.GetHashCode())
            {
                return BadRequest("问卷当前处于非运行状态,无法提交此问卷！");
            }
            var flag = await _consequenceService.CheckConsequenceAnswered(model.SerialNumber, CurrentSeagull2User(true).Id);
            if (flag)
            {
                return BadRequest("请勿重复答卷！");
            }
            var data = await _consequenceService.Add(model, CurrentSeagull2User(true));
            return Ok(data);
        }
        /// <summary>
        /// 加载问卷
        /// </summary>
        /// <param name="serialNumber">问卷编号</param>
        /// <returns></returns>  
        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Questions(string serialNumber)
        {
            var consequence = await _consequenceService.CheckConsequenceAnswered(serialNumber, CurrentSeagull2User(true).Id);
            if (consequence)
            {
                var qustionnaireInfo = await _questionnaireService.CheckSingleQuestionnaires(serialNumber);
                return Ok(new QuestionnairesModel()
                {
                    SerialNumber = serialNumber,
                    LoadState = QuestionnaireLoadStatus.Answered,
                    IsShowAnswer = qustionnaireInfo.IsShowAnswer,
                    IsAllowAnonymous = qustionnaireInfo.IsAllowAnonymous
                });
            }
            var questionnaire = await _questionnaireService.CheckSingleQuestionnaires(serialNumber);
            if (questionnaire == null)
            {
                return Ok(questionnaire);
            }
            CheckIsThrough(questionnaire);
            if (questionnaire.LoadState != QuestionnaireLoadStatus.Success)
            {
                return Ok(questionnaire);
            }
            questionnaire = await _questionnaireService.Load(serialNumber, CurrentSeagull2User(true), false);
            return Ok(questionnaire == null ? new QuestionnairesModel() : questionnaire);
        }
        /// <summary>
        /// 根据问题的Code加载投票信息
        /// </summary>
        /// <param name="questionCodes"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IHttpActionResult> LoadVoteResult([FromUri]List<string> questionCodes)
        {
            return Ok(await _choiceOption_ResultService.LoadVoteResult(questionCodes));
        }
        [HttpGet]
        public async Task<IHttpActionResult> QuestionsType(string serialNumber)
        {
            var questionnaire = await _questionnaireService.CheckSingleQuestionnaires(serialNumber);
            return Ok(questionnaire == null ? new QuestionnairesModel() : questionnaire);
        }
        /// <summary>
        /// 检查是否通过
        /// </summary>
        /// <param name="questionnaire"></param>
        private void CheckIsThrough(QuestionnairesModel questionnaire)
        {
            questionnaire.LoadState = QuestionnaireLoadStatus.Success;
            if (questionnaire != null && questionnaire.Status != QuestionnaireStatus.Running.GetHashCode() && questionnaire.Creator != CurrentSeagull2User().Id)
            {
                questionnaire.LoadState = QuestionnaireLoadStatus.NoRunning;
            }
            if (questionnaire.IsStartTime && questionnaire.StartTime > DateTimeOffset.Now)
            {
                questionnaire.LoadState = QuestionnaireLoadStatus.NotStarted;
            }
            if (questionnaire.IsEndTime && questionnaire.EndTime < DateTimeOffset.Now)
            {
                questionnaire.LoadState = QuestionnaireLoadStatus.Over;
            }
        }
        /// <summary>
        /// 加载已答的问卷
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="creator"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IHttpActionResult> AnswerData(string serialNumber, string creator)
        {
            return Ok(await _consequenceService.Load(serialNumber, creator));
        }
    }
}