﻿using Seagull2.Core.Security;
using Seagull2.Models;
using Seagull2.Owin;
using Seagull2.Owin.File.Configuration;
using Seagull2.Owin.File.Models;
using Seagull2.Owin.File.Services;
using Seagull2.Questionnaire.WebApi.Calculate;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Seagull2.Questionnaire.WebApi.Controller
{
    /// <summary>
    /// 统计Controller
    /// </summary>
    [AllowAnonymous]
    public class StatisticsSheetController : BaseController
    {
        private readonly IConsequenceService _consequenceService;
        private readonly IStatisticsSheetService _statisticsSheetService;
        private readonly IClientFileService _fileService;
        private readonly IOptionResultService _choiceOption_ResultService;
        public StatisticsSheetController(
            IConsequenceService consequenceService,
            IStatisticsSheetService statisticsSheetService,
            IClientFileService fileService,
            IOptionResultService fillInOption_ResultService)
        {
            _consequenceService = consequenceService;
            _statisticsSheetService = statisticsSheetService;
            _fileService = fileService;
            _choiceOption_ResultService = fillInOption_ResultService;
        }
        /// <summary>
        /// 获取问卷的统计信息
        /// </summary>
        /// <returns></returns>
        public Task<IHttpActionResult> QuestionnairesStatistics()
        {
            return null;
        }
        [HttpGet]
        public async Task<IHttpActionResult> CheckConsequenceCount(string serialNumber)
        {
            return Ok(await _consequenceService.CheckConsequenceCount(serialNumber));
        }
        [HttpGet]
        public async Task<IHttpActionResult> LoadQuestionStatistics(string serialNumber)
        {
            return Ok(await _statisticsSheetService.LoadQuestionStatistics(serialNumber, CurrentSeagull2User()));
        }
        [HttpGet]
        public async Task<IHttpActionResult> DetailsPage([FromUri]PaginationInfo pagination, string code)
        {

            return Ok(await _choiceOption_ResultService.DetailsPage(pagination, code));
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetFiles(string resourceId)
        {
            List<ClientFileInformation> files = _fileService.GetFilesByResourceId(resourceId);
            return Ok(files);
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<IHttpActionResult> ShowAnswerAnalysis(string serialNumber, string userId = "")
        {
            userId = string.IsNullOrWhiteSpace(userId) ? CurrentSeagull2User(true).Id : userId;
            return Ok(await _statisticsSheetService.ShowAnswerAnalysis(serialNumber, userId));
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<IHttpActionResult> AnswerSheet(string serialNumber, string userId = "")
        {
            userId = string.IsNullOrWhiteSpace(userId) ? CurrentSeagull2User(true).Id : userId;
            return Ok(await _statisticsSheetService.AnswerSheet(serialNumber, userId));
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<IHttpActionResult> LoadAnswer(string serialNumber, string userId = "")
        {
            userId = string.IsNullOrWhiteSpace(userId) ? CurrentSeagull2User(true).Id : userId;
            return Ok(await _consequenceService.LoadAnswer(serialNumber, userId));
        }
        [HttpGet]
        public async Task<IHttpActionResult> Consequences(string serialNumber)
        {
            return Ok(await _consequenceService.LoadConsequences(serialNumber));
        }
        //导出答案详情
        [AllowAnonymous]
        [HttpGet]
        public async Task<HttpResponseMessage> ExcelConsequences(string serialNumber)
        {
            var data = await _consequenceService.LoadConsequences(serialNumber);

            return this.DownLoad(GenerateExcelConsequencesAnalysis.StatisticsExcelStream(data));
        }
        //导出
        [AllowAnonymous]
        [HttpGet]
        public async Task<HttpResponseMessage> Excel(string serialNumber)
        {
            var data = await _statisticsSheetService.LoadQuestionStatistics(serialNumber, CurrentSeagull2User(true));

            return this.DownLoad(GenerateExcelAnalysis.StatisticsExcelStream(data));
        }
        private HttpResponseMessage DownLoad(Stream plainStream)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StreamContent(plainStream);
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = HttpUtility.UrlEncode("问卷数据统计表.xls", System.Text.Encoding.UTF8);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/ms-excel");
            response.Content.Headers.ContentLength = plainStream.Length;
            response.Headers.CacheControl = new CacheControlHeaderValue() { NoStore = true };

            return response;
        }

    }
}