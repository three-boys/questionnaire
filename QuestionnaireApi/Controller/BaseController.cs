﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace Seagull2.Questionnaire.WebApi.Controller
{
    public class BaseController : ApiController
    {
        /// <summary>
        /// 当前登录人
        /// </summary>
        /// <param name="isAllowAnonymous">是否允许匿名</param>
        /// <returns></returns>
        [AllowAnonymous]
        protected Seagull2User CurrentSeagull2User(bool isAllowAnonymous = false)
        {
            Seagull2User currentUser = new Seagull2User();
            try
            {
                var identity = (Seagull2Identity)User.Identity;
                currentUser.DisplayName = identity.DisplayName;
                currentUser.Id = identity.Id;
                currentUser.IsAllowAnonymous = false;
            }
            catch
            {
                GetAllowAnonymousUser(currentUser);
            }
            return currentUser;
        }

        /// <summary>
        /// 获取匿名用户信息(displayName=主机名，Id=主机IP)
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private Seagull2User GetAllowAnonymousUser(Seagull2User currentUser)
        {
            currentUser.DisplayName = "匿名用户(" + GetIP() + ")";
            currentUser.IsAllowAnonymous = true;
            currentUser.Id = GetIP();
            return currentUser;
        }
        /// <summary>
        /// 获取客户端IP地址
        /// </summary>
        /// <returns>若失败则返回回送地址</returns>
        public static string GetIP()
        {
            //如果客户端使用了代理服务器，则利用HTTP_X_FORWARDED_FOR找到客户端IP地址
            string userHostAddress = null;
            try
            {
                userHostAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString().Split(',')[0].Trim();
            }
            catch
            {
                //否则直接读取REMOTE_ADDR获取客户端IP地址
                if (string.IsNullOrEmpty(userHostAddress))
                {
                    userHostAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                //前两者均失败，则利用Request.UserHostAddress属性获取IP地址，但此时无法确定该IP是客户端IP还是代理IP
                if (string.IsNullOrEmpty(userHostAddress))
                {
                    userHostAddress = HttpContext.Current.Request.UserHostAddress;
                }
                //最后判断获取是否成功，并检查IP地址的格式（检查其格式非常重要）
                if (!string.IsNullOrEmpty(userHostAddress) && IsIP(userHostAddress))
                {
                    return userHostAddress;
                }
            }
            return "127.0.0.1";
        }

        /// <summary>
        /// 检查IP地址格式
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIP(string ip)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }
    }
}