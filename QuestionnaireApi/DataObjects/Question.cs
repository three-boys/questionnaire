﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.DataObjects
{
    /// <summary>
    /// 题目
    /// </summary>
    [Table("Questions", Schema = "dbo")]
    public class Question : BaseObject
    {
        [Key]
        [Column("Code")]
        public string Code { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int? NO { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 是否必答题
        /// </summary>
        public bool Required { get; set; }
        /// <summary>
        /// 是否无条件跳转
        /// </summary>
        public bool UnconditionalJMP { get; set; }
        //跳转到那一页
        public int UnconditionalJMPTo { get; set; }
        /// <summary>
        /// 是否关联逻辑
        /// </summary>
        public bool Related { get; set; }
        /// <summary>
        /// 关联题目
        /// </summary>
        public string RelevanceQuestion { get; set; }
        /// <summary>
        /// 是否选择
        /// </summary>
        public string WhetherSelect { get; set; }
        /// <summary>
        /// 关联选项
        /// </summary>
        public string LogicOption { get; set; }
        /// <summary>
        /// 是否提示
        /// </summary>
        public bool HasReminder { get; set; }
        /// <summary>
        /// 提示信息
        /// </summary>
        public string Reminder { get; set; }
        /// <summary>
        /// 选项跳转
        /// </summary>
        public bool OptionJMP { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public string QuestionType { get; set; }
        /// <summary>
        /// 选择比例
        /// </summary>
        public string OptionProportion { get; set; }
        /// <summary>
        /// 分值
        /// </summary>
        public double Score { get; set; }
        /// <summary>
        /// 问卷的Code
        /// </summary>
        public string QuestionnaireCode { get; set; }
        /// <summary>
        /// 显示数量
        /// </summary>
        public bool PollNumber { get; set; }
        /// <summary>
        /// 显示百分比
        /// </summary>
        public bool Percentage { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public int? PageIndex { get; set; }
        /// <summary>
        /// 停留最短时间
        /// </summary>
        public int? StandingMinTime { get; set; }
        /// <summary>
        /// 停留最长时间
        /// </summary>
        public int? StandingMaxTime { get; set; }

        /// <summary>
        /// 标题宽度
        /// </summary>
        public string TitleWidth { get; set; }
        /// <summary>
        /// 标题总宽度
        /// </summary>
        public string TitleAllWidth { get; set; }
        /// <summary>
        /// 题目的分类（考试、投票）
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 题目的选项性（单选多选）
        /// </summary>
        public string Selectivity { get; set; }
        /// <summary>
        /// 最少选几项
        /// </summary>
        public int? AtLeast { get; set; }
        /// <summary>
        /// 最多选几项
        /// </summary>
        public int? AtMost { get; set; }
        /// <summary>
        /// 最小值
        /// </summary>
        public int? MinValue { get; set; }
        /// <summary>
        /// 最大值
        /// </summary>
        public int? MaxValue { get; set; }
        /// <summary>
        /// 最小值文本
        /// </summary>
        public string MinValueShow { get; set; }
        /// <summary>
        /// 最大值文本
        /// </summary>
        public string MaxValueShow { get; set; }
        /// <summary>
        /// 是否选择题
        /// </summary>
        public string QuestionForm { get; set; }
        /// <summary>
        /// 量表题
        /// </summary>
        public string LikertPattern { get; set; }
        /// <summary>
        /// 右标题宽度
        /// </summary>
        public string RightTitleWidth { get; set; }
    }
}