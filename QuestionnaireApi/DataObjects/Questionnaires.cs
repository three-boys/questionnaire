﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.DataObjects
{
    /// <summary>
    /// 问卷
    /// </summary>
    [Table("Questionnaires", Schema = "dbo")]
    public class Questionnaires : BaseObject
    {
        /// <summary>
        /// 编码
        /// </summary>
        [Key]
        [Column("Code")]
        public string Code { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string SerialNumber { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [MaxLength(120, ErrorMessage = "标题不得超过120个字符")]
        public string Title { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [MaxLength(4000, ErrorMessage = "描述不得超过4000个字符")]
        public string Summary { get; set; }
        /// <summary>
        ///类别(问卷，考试，投票)
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 状态(草稿/发布/完成)
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 问卷开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 问卷结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 是否设置结束时间
        /// </summary>
        public bool IsEndTime { get; set; }
        /// <summary>
        /// 是否设置开始时间
        /// </summary>
        public bool IsStartTime { get; set; }
        /// <summary>
        /// 限时（秒）
        /// </summary>
        public Int64? TimeLimit { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        public int Versions { get; set; }
        /// <summary>
        /// 是否匿名登录
        /// </summary>
        public bool IsAllowAnonymous { get; set; }
        /// <summary>
        /// 是否统一发放
        /// </summary>
        public bool IsShowAnswer { get; set; }
        /// <summary>
        /// 用户是否可分享
        /// </summary>
        public bool IsUserShare { get; set; }
        /// <summary>
        /// 所有题目
        /// </summary>
        public string Questions { get; set; }
    }
}