﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.DataObjects
{
    /// <summary>
    /// 结果表（保存的是作答的结果，某个问卷下的题目选择的结果，填空的每个选项的值则填写在value里面,考试题是否正确）
    /// </summary>
    [Table("Consequences", Schema = "dbo")]
    public class Consequence : BaseObject
    {
        [Key]
        [Column("Code")]
        public string Code { get; set; }
        /// <summary>
        /// 问卷的Code
        /// </summary>
        public string QuestionaireCode { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string SerialNumber { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        public int Versions { get; set; }
        /// <summary>
        /// 答题所用时间
        /// </summary>
        public Int64 AnsweredTimes { get; set; }
        /// <summary>
        /// 总得分
        /// </summary>
        public double AggregateScore { get; set; }
        /// <summary>
        /// 答对题数
        /// </summary>
        public int CorrectQuestionCount { get; set; }
    }
}