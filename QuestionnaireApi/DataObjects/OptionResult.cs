﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.DataObjects
{
    [Table("OptionResults", Schema = "dbo")]
    public class OptionResult : BaseObject
    {
        [Key]
        [Column("Code")]
        public string Code { get; set; }
        public string QuestionnaireNumber { get; set; }
        public string ConsequenceCode { get; set; }
        /// <summary>
        /// 题目的Code
        /// </summary>
        public string QuestionCode { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string AnswerOption { get; set; }
        /// <summary>
        ///选项可填空的答案
        /// </summary>
        public string OptionGapFilling { get; set; }

    }
}