﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.DataObjects
{
    /// <summary>
    /// 选择题的选项表
    /// </summary>
    [Table("Options", Schema = "dbo")]
    public class Option : BaseObject
    {
        [Key]
        [Column("Code")]
        public string Code { get; set; }
        /// <summary>
        /// 题目的Code
        /// </summary>
        public string QuestionCode { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 跳转到某页
        /// </summary>
        public int? OptionJMPTo { get; set; }
        /// <summary>
        /// 允许填空
        /// </summary>
        public bool AlloweFillIn { get; set; }
        /// <summary>
        /// 填空是否必填
        /// </summary>
        public bool RequiredFillIn { get; set; }
        /// <summary>
        /// 是否默认选中
        /// </summary>
        public bool IsDefault { get; set; }
        /// <summary>
        /// 提示
        /// </summary>
        public string Reminder { get; set; }
        /// <summary>
        /// 是否多选
        /// </summary>
        public bool IsMutual { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        public int? Score { get; set; }
        /// <summary>
        /// 是否是正确答案
        /// </summary>
        public bool IsCorrect { get; set; }
        /// <summary>
        /// 是否有分数
        /// </summary>
        public bool NoRecord { get; set; }
        /// <summary>
        ///右标题
        /// </summary>
        public string RightTitle { get; set; }
        /// <summary>
        /// 标题宽度
        /// </summary>
        public string TitleWidth { get; set; }
        /// <summary>
        /// 高度
        /// </summary>
        public int? Height { get; set; }
        /// <summary>
        /// 宽度
        /// </summary>
        public int? Width { get; set; }
        /// <summary>
        /// 是否有下划线
        /// </summary>
        public bool Underline { get; set; }
        /// <summary>
        /// 默认值
        /// </summary>
        public string DefaultContent { get; set; }
        /// <summary>
        /// 是否允许重复
        /// </summary>
        public bool AlloweRepetition { get; set; }
        /// <summary>
        /// 最大字数
        /// </summary>
        public int? MaxNum { get; set; }
        /// <summary>
        /// 最小字数
        /// </summary>
        public int? MinNum { get; set; }
        /// <summary>
        /// 是否必答
        /// </summary>
        public bool Required { get; set; }
        /// <summary>
        /// 行高
        /// </summary>
        public string OptionProportionHeight { get; set; }
        /// <summary>
        /// 宽度
        /// </summary>
        public string OptionProportionWidth { get; set; }
        /// <summary>
        /// 是否有下划线
        /// </summary>
        public string UnderlineStyle { get; set; }
        /// <summary>
        /// 验证方式
        /// </summary>
        public string Validate { get; set; }
        /// <summary>
        /// 最小值
        /// </summary>
        public Int64? MinNumber { get; set; }
        /// <summary>
        /// 最大值
        /// </summary>
        public Int64? MaxNumber { get; set; }
        /// <summary>
        /// 是否重复
        /// </summary>
        public bool NotRepeat { get; set; }
        /// <summary>
        /// 是否默认下划线
        /// </summary>
        public bool IsDefauleUnderLinestyle { get; set; }
        /// <summary>
        /// 是否有默认值
        /// </summary>
        public bool IsDefauleValue { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string TheAnswer { get; set; }
        /// <summary>
        /// 是否包含答案即可得分
        /// </summary>
        public bool IsContainsAnswer { get; set; }
        /// <summary>
        /// 右侧输入框宽度
        /// </summary>
        public string RightInputWidth { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string AnswerOption { get; set; }
    }
}