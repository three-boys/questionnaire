﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.DataObjects
{
    /// <summary>
    ///上传文件
    /// </summary>
    [Table("FileUploadOptions", Schema = "dbo")]
    public class FileUploadOption : BaseObject
    {
        [Key]
        [Column("Code")]
        public string Code { get; set; }
        /// <summary>
        /// 题目的Code
        /// </summary>
        public string QuestionCode { get; set; }
        /// <summary>
        /// 是否上传图片
        /// </summary>
        public bool Picture { get; set; }
        /// <summary>
        /// 是否上传文件
        /// </summary>
        public bool File { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Files { get; set; }
        /// <summary>
        /// 是否上传压缩文件
        /// </summary>
        public bool Compress { get; set; }
        /// <summary>
        /// 扩展
        /// </summary>
        public string ExtensionDetail { get; set; }
        /// <summary>
        /// 最大文件
        /// </summary>
        public Int64? FileMax { get; set; }
    }
}