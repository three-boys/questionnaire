﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.DataObjects
{
    /// <summary>
    /// 实体基类
    /// </summary>
    public class BaseObject
    {
        /// <summary>
        /// 创建人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        public string CreatorName { get; set; }
        /// <summary>
        /// 创建人时间
        /// </summary>
        public DateTimeOffset CreateTime { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public string Modifier { get; set; }
        /// <summary>
        /// 修改人显示名称
        /// </summary>
        public string ModifierName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTimeOffset? ModifyTime { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int? SortNo { get; set; }
        /// <summary>
        /// 是否可用
        /// </summary>
        public bool ValidStatus { get; set; }
    }
}