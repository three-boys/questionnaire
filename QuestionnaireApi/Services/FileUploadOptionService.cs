﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Models.Extensions;
using Seagull2.Questionnaire.WebApi.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Services
{
    public class FileUploadOptionService : IFileUploadOptionService
    {
        private readonly IFileUploadOptionRepository _FileUploadOptionRepository;
        public FileUploadOptionService(IFileUploadOptionRepository FileUploadOptionRepository)
        {
            _FileUploadOptionRepository = FileUploadOptionRepository;
        }
        /// <summary>
        /// 添加或修改填空选项
        /// </summary>
        /// <param name="data">内容</param>
        /// <param name="questionCode">问卷</param>
        /// <param name="currentUser">登录人</param>
        /// <returns></returns>
        public async Task<bool> Add(Dictionary<string, IEnumerable<FileUploadOptionModel>> data, Seagull2User currentUser)
        {
            if (data == null || data.Count() == 0) return false;
            bool optionResult = await _FileUploadOptionRepository.Add(CollectData(data, currentUser));
            return optionResult;
        }
        private IEnumerable<FileUploadOption> CollectData(Dictionary<string, IEnumerable<FileUploadOptionModel>> data, Seagull2User currentUser)
        {
            List<FileUploadOption> list = new List<FileUploadOption>();
            foreach (var item in data)
            {
                if (item.Value != null)
                {
                    foreach (var model in item.Value)
                    {
                        list.Add(SetDefaultValue(model.ToFileUploadOption(item.Key), currentUser));
                    }
                }
            }
            return list;
        }
        private FileUploadOption SetDefaultValue(FileUploadOption model, Seagull2User currentUser)
        {
            model.Code = Guid.NewGuid().ToString();
            model.CreateTime = DateTimeOffset.Now;
            model.Creator = currentUser.Id;
            model.CreatorName = currentUser.DisplayName;
            model.ValidStatus = true;
            return model;
        }
        /// <summary>
        /// 加载填空选项
        /// </summary>
        /// <param name="questionCode">题目</param>
        /// <returns></returns>
        public async Task<IEnumerable<FileUploadOptionModel>> Load(string questionCode)
        {
            var db = new QuestionnaireDbContext();
            var data = await _FileUploadOptionRepository.Load(questionCode);
            return data.OrderBy(d => d.SortNo).Select(d => d.ToFileUploadOptionModel());
        }
        /// <summary>
        /// 加载填空选项
        /// </summary>
        /// <param name="questionCode">题目</param>
        /// <returns></returns>
        public async Task<IEnumerable<FileUploadOptionModel>> Load(List<string> questionCodes)
        {
            var db = new QuestionnaireDbContext();
            var data = await _FileUploadOptionRepository.Load(questionCodes);
            return data.OrderBy(d => d.SortNo).Select(d => d.ToFileUploadOptionModel());
        }
    }
}