﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Models.Extensions;
using Seagull2.Questionnaire.WebApi.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Services
{
    public class FileUploadOptionResultService : IFileUploadOptionResultService
    {
        private readonly IFileUploadOptionResultRepository _fileUploadOptionResultRepository;

        public FileUploadOptionResultService(IFileUploadOptionResultRepository fileUploadOptionResultRepository)
        {
            _fileUploadOptionResultRepository = fileUploadOptionResultRepository;
        }
        public async Task<bool> Add(IEnumerable<FileUploadOptionResultModel> data, string questionnaireNumber, string consequenceCode, Seagull2User currentUser)
        {
            if (data == null || data.Count() == 0) return false;
            return await _fileUploadOptionResultRepository.Add(data.ToFileUploadOptionCollectionResult(questionnaireNumber, consequenceCode, currentUser));
        }
        public async Task<IEnumerable<FileUploadOptionResultModel>> Load(string questionnaireNumber)
        {
            var data = await _fileUploadOptionResultRepository.Load(questionnaireNumber);
            return data.OrderBy(d => d.SortNo).Select(d => d.ToFileUploadOptionModelResult());
        }

        public async Task<IEnumerable<FileUploadOptionResultModel>> Load(string questionnaireNumber, string userId)
        {
            var data = await _fileUploadOptionResultRepository.Load(questionnaireNumber, userId);
            return data.OrderBy(d => d.SortNo).Select(d => d.ToFileUploadOptionModelResult());
        }

        public async Task<IEnumerable<FileUploadOptionResultModel>> LoadByConsequenceCode(string consequenceCode)
        {
            var data = await _fileUploadOptionResultRepository.LoadByConsequenceCode(consequenceCode);
            return data.OrderBy(d => d.SortNo).Select(d => d.ToFileUploadOptionModelResult());
        }

        public async Task<IEnumerable<FileUploadOptionResultModel>> LoadByConsequenceCode(string consequenceCode, string userId)
        {
            var data = await _fileUploadOptionResultRepository.LoadByConsequenceCode(consequenceCode, userId);
            return data.OrderBy(d => d.SortNo).Select(d => d.ToFileUploadOptionModelResult());
        }
    }
}