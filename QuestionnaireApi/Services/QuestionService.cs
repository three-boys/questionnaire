﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Repositories;
using Seagull2.Questionnaire.WebApi.Models.Extensions;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Newtonsoft.Json;
using Seagull2.Owin.File.Models;
using Seagull2.Owin.File.Services;

namespace Seagull2.Questionnaire.WebApi.Services
{
    /// <summary>
    /// 题目的服务
    /// </summary>
    public class QuestionService : IQuestionService
    {
        private readonly IOptionService _optionService;
        private readonly IQuestionRepository _questionRepository;
        private readonly IFileUploadOptionService _fileUploadOptionService;
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="optionService"></param>
        /// <param name="relationService"></param>
        public QuestionService(
            IOptionService optionService,
            IQuestionRepository questionRepository,
            IFileUploadOptionService fileUploadOptionService)
        {
            _optionService = optionService;
            _questionRepository = questionRepository;
            _fileUploadOptionService = fileUploadOptionService;
        }
        /// <summary>
        /// 添加题目
        /// </summary>
        /// <param name="data">题目内容</param>
        /// <param name="questionnaireCode">问卷</param>
        /// <param name="currentUser">当期登录人</param>
        /// <returns></returns>
        public async Task<bool> Add(IEnumerable<QuestionModel> data, string questionnaireCode, Seagull2User currentUser)
        {
            //设置好题目数据的初始值，放在数组中，然后一并插入到数据库中
            List<Question> insertQuestion = new List<Question>();
            Dictionary<string, IEnumerable<OptionModel>> insertChoiceOption = new Dictionary<string, IEnumerable<OptionModel>>();
            Dictionary<string, IEnumerable<FileUploadOptionModel>> insertFileUploadOption = new Dictionary<string, IEnumerable<FileUploadOptionModel>>();
            foreach (var item in data)
            {
                Question question = SetDefaultValue(item.ToQuestion(questionnaireCode), currentUser);
                insertQuestion.Add(question);
                //只保存到choice和fileload 
                if (item.QuestionForm.ToLower() == QuestionFromEnmu.FileUpload.ToString().ToLower())
                {
                    var fileUploadOption = item.Options == null ? null : JsonConvert.DeserializeObject<IEnumerable<FileUploadOptionModel>>(item.Options.ToString());
                    insertFileUploadOption.Add(question.Code, fileUploadOption);
                }
                else
                {
                    var choiceOption = item.Options == null ? null : JsonConvert.DeserializeObject<IEnumerable<OptionModel>>(item.Options.ToString());
                    insertChoiceOption.Add(question.Code, choiceOption);
                }
            }
            //取到所有的题目，执行添加操作
            var flag = await _questionRepository.Add(insertQuestion);
            await _optionService.Add(insertChoiceOption, currentUser);
            await _fileUploadOptionService.Add(insertFileUploadOption, currentUser);
            return true;
        }
        private Question SetDefaultValue(Question model, Seagull2User currentUser)
        {
            model.Code = Guid.NewGuid().ToString();
            model.CreateTime = DateTimeOffset.Now;
            model.Creator = currentUser.Id;
            model.CreatorName = currentUser.DisplayName;
            model.ValidStatus = true;
            return model;
        }
        /// <summary>
        /// 根据问卷Code加载题目
        /// </summary>
        /// <param name="questionnaireCode">问卷</param>
        /// <returns></returns>
        public async Task<IEnumerable<QuestionModel>> Load(string questionnaireCode)
        {
            var data = await _questionRepository.Load(questionnaireCode);
            return await ToQuestionModelCollection(data);
        }
        private async Task<IEnumerable<QuestionModel>> ToQuestionModelCollection(IEnumerable<Question> data)
        {
            if (data == null) return null;
            List<QuestionModel> result = new List<QuestionModel>();
            foreach (var model in data)
            {
                var obj = await ToQuestionModel(model);
                if (obj != null)
                {
                    result.Add(obj);
                }
            }
            return result.OrderBy(d => d.SortNo);
        }
        private async Task<QuestionModel> ToQuestionModel(Question model)
        {
            if (model == null) return null;
            QuestionModel data = new QuestionModel();
            data.Title = model.Title;
            data.Required = model.Required;
            data.UnconditionalJMP = model.UnconditionalJMP;
            data.UnconditionalJMPTo = model.UnconditionalJMPTo;
            data.RelevanceQuestion = JsonConvert.DeserializeObject<QuestionModel>(model.RelevanceQuestion);
            data.LogicOption = model.LogicOption;
            data.WhetherSelect = model.WhetherSelect;
            data.Related = model.Related;
            data.HasReminder = model.HasReminder;
            data.Reminder = model.Reminder;
            data.OptionJMP = model.OptionJMP;
            data.Percentage = model.Percentage;
            data.PollNumber = model.PollNumber;
            data.PageIndex = model.PageIndex;
            data.StandingMinTime = model.StandingMinTime;
            data.StandingMaxTime = model.StandingMaxTime;
            data.TitleWidth = model.TitleWidth;
            data.TitleAllWidth = model.TitleAllWidth;
            data.Selectivity = model.Selectivity;
            data.Category = model.Category;
            data.Score = model.Score;
            data.NO = model.NO;
            data.MinValue = model.MinValue;
            data.MinValueShow = model.MinValueShow;
            data.MaxValue = model.MaxValue;
            data.MaxValueShow = model.MaxValueShow;
            data.QuestionForm = model.QuestionForm;
            data.LikertPattern = model.LikertPattern;
            data.AtLeast = model.AtLeast;
            data.AtMost = model.AtMost;
            data.SortNo = model.SortNo;
            data.QuestionType = model.QuestionType;
            data.Category = model.Category;
            data.OptionProportion = model.OptionProportion;
            data.RightTitleWidth = model.RightTitleWidth;
            data.Code = model.Code;
            if (data.QuestionForm.ToLower() == QuestionFromEnmu.FileUpload.ToString().ToLower())
            {
                data.Options = (await _fileUploadOptionService.Load(model.Code)).ToList();
            }
            else
            {
                data.Options = (await _optionService.Load(model.Code)).ToList();
            }
            return data;
        }

    }
}