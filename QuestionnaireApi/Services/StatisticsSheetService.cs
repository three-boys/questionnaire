﻿using Newtonsoft.Json;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Calculate;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.Common.Extensions;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Models.StatisticsSheet;
using Seagull2.Questionnaire.WebApi.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Services
{
    /// <summary>
    ///  问卷统计服务
    /// </summary>
    public class StatisticsSheetService : IStatisticsSheetService
    { 
        private readonly IFileUploadOptionResultService _fileUploadOption_ResultService;
        private readonly IOptionResultService _choiceoption_resultservice;
        private readonly IQuestionnaireService _questionnaireService;
        private readonly IQuestionService _questionService;
        private readonly IConsequenceRepository _consequenceRepository;
        public StatisticsSheetService( 
            IOptionResultService choiceoption_resultservice,
            IQuestionnaireService questionnaireService,
            IQuestionService questionService,
            IFileUploadOptionResultService fileUploadOption_ResultService,
            IConsequenceRepository consequenceRepository
            )
        {
            _choiceoption_resultservice = choiceoption_resultservice;
            _questionnaireService = questionnaireService;
            _questionService = questionService;
            _fileUploadOption_ResultService = fileUploadOption_ResultService;
            _consequenceRepository = consequenceRepository;
        }
        public async Task<List<QuestionStatisticsModel>> LoadQuestionStatistics(string serialNumber, Seagull2User currentUser)
        {
            var data = await _questionnaireService.Load(serialNumber, currentUser, true);
            return await TransferQuestionStatistics(data);
        }
        private async Task<List<QuestionStatisticsModel>> TransferQuestionStatistics(QuestionnairesModel data)
        {
            var choiceResult = await _choiceoption_resultservice.Load(data.SerialNumber);
            return GenerateStatisticsAnalysis.CalculateStatistics(data.Questions, choiceResult).ToList();
        }
        /// <summary>
        /// 答案解析（成绩单，仅限考试时使用）
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<List<AnswerAnalysisModel>> ShowAnswerAnalysis(string serialNumber, string userId)
        {
            var data = await _consequenceRepository.Load(serialNumber, userId);
            if (data.Count() == 0)
            {
                return new List<AnswerAnalysisModel>();
            }
            return await LoadAnswerAnalysis(data.FirstOrDefault().Code, data.FirstOrDefault().QuestionaireCode, userId);
        }
        private async Task<List<AnswerAnalysisModel>> LoadAnswerAnalysis(string consequenceCode, string questionaireCode, string userId)
        {
            var choiceResult = await _choiceoption_resultservice.LoadByConsequenceCode(consequenceCode, userId);
            var answerAnaly = await _questionService.Load(questionaireCode);
            return GenerateAnswerAnalysis.AnswerAnalysis(choiceResult, answerAnaly);
        }
        /// <summary>
        /// 答题卡（加载答题情况）
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<AnswerAnalysisModel>> AnswerSheet(string serialNumber, string userId)
        {
            var data = await _consequenceRepository.Load(serialNumber, userId);
            if (data.Count() == 0)
            {
                return new List<AnswerAnalysisModel>();
            }
            return await LoadAnswerSheet(data.FirstOrDefault().Code, data.FirstOrDefault().QuestionaireCode, userId);
        }
        private async Task<List<AnswerAnalysisModel>> LoadAnswerSheet(string consequenceCode, string questionaireCode, string userId)
        {
            var choiceResult = await _choiceoption_resultservice.LoadByConsequenceCode(consequenceCode, userId);
            var fileUploadResult = await _fileUploadOption_ResultService.LoadByConsequenceCode(consequenceCode, userId);
            var answerAnaly = await _questionService.Load(questionaireCode);
            return GenerateAnswerAnalysis.AnswerSheet(choiceResult, fileUploadResult, answerAnaly);
        }
    }
}