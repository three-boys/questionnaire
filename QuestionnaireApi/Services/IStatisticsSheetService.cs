﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Models.StatisticsSheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Services
{
    /// <summary>
    /// 问卷统计服务
    /// </summary>
    public interface IStatisticsSheetService
    {
        Task<List<QuestionStatisticsModel>> LoadQuestionStatistics(string serialNumber, Seagull2User currentUser);

        /// <summary>
        /// 答案解析
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<AnswerAnalysisModel>> ShowAnswerAnalysis(string serialNumber, string userId);

        /// <summary>
        /// 答案卡
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<AnswerAnalysisModel>> AnswerSheet(string serialNumber, string userId);
    }
}
