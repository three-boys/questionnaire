﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Services
{
    /// <summary>
    /// 上传文件接口
    /// </summary>
    public interface IFileUploadOptionService
    {
        /// <summary>
        /// 加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<IEnumerable<FileUploadOptionModel>> Load(string questionCode);
        /// <summary>
        /// 加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<IEnumerable<FileUploadOptionModel>> Load(List<string> questionCode);
        /// <summary>
        /// 增加选项
        /// </summary>
        /// <param name="data">选项内容</param>
        /// <param name="questionCode">题目</param>
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns>
        Task<bool> Add(Dictionary<string, IEnumerable<FileUploadOptionModel>> data, Seagull2User currentUser);
    }
}
