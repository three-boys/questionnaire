﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Repositories;
using Seagull2.Questionnaire.WebApi.Models.Extensions;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;

namespace Seagull2.Questionnaire.WebApi.Services
{
    /// <summary>
    /// 选项的服务
    /// </summary>
    public class OptionService : IOptionService
    {
        private readonly IOptionRepository _optionRepository;
        public OptionService(IOptionRepository optionRepository)
        {
            _optionRepository = optionRepository;
        }
        /// <summary>
        /// 增加选项
        /// </summary>
        /// <param name="data">选项内容(key：题目Code，value：选项内容)</param> 
        /// <param name="currentUser">当前登录人</param>
        public async Task<bool> Add(Dictionary<string, IEnumerable<OptionModel>> data, Seagull2User currentUser)
        {
            bool optionResult = await _optionRepository.Add(CollectData(data, currentUser));
            return optionResult;
        }
        private IEnumerable<Option> CollectData(Dictionary<string, IEnumerable<OptionModel>> data, Seagull2User currentUser)
        {
            List<Option> list = new List<Option>();
            foreach (var item in data)
            {
                if (item.Value != null)
                {
                    foreach (var model in item.Value)
                    {
                        list.Add(SetDefaultValue(model.ToChoiceOption(item.Key), currentUser));
                    }
                }
            }
            return list;
        }
        private Option SetDefaultValue(Option model, Seagull2User currentUser)
        {
            model.Code = Guid.NewGuid().ToString();
            model.CreateTime = DateTimeOffset.Now;
            model.Creator = currentUser.Id;
            model.CreatorName = currentUser.DisplayName;
            model.ValidStatus = true;
            return model;
        }
        /// <summary>
        /// 加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        public async Task<IEnumerable<OptionModel>> Load(string questionCode)
        {
            var db = new QuestionnaireDbContext();
            var data = await _optionRepository.Load(questionCode);
            return data.OrderBy(d => d.SortNo).Select(d => d.ToChoiceOption());
        }
        public async Task<IEnumerable<OptionModel>> Load(List<string> questionCode)
        {
            var db = new QuestionnaireDbContext();
            var data = await _optionRepository.Load(questionCode);
            return data.OrderBy(d => d.SortNo).Select(d => d.ToChoiceOption());
        }
    }
}