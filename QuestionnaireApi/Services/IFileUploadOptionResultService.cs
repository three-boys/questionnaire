﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Services
{
    public interface IFileUploadOptionResultService
    {
        /// <summary>
        /// 加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<IEnumerable<FileUploadOptionResultModel>> Load(string questionnaireNumber);
        /// <summary>
        /// 答案解析加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<IEnumerable<FileUploadOptionResultModel>> Load(string questionnaireNumber, string userId);
        Task<IEnumerable<FileUploadOptionResultModel>> LoadByConsequenceCode(string consequenceCode);
        Task<IEnumerable<FileUploadOptionResultModel>> LoadByConsequenceCode(string consequenceCode, string userId);

        /// <summary>
        /// 增加选项
        /// </summary>
        /// <param name="data">选项内容</param> 
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns> 
        Task<bool> Add(IEnumerable<FileUploadOptionResultModel> data, string questionnaireNumber, string consequenceCode, Seagull2User currentUser);
    }
}
