﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using System.Text;
using System.Threading.Tasks;
using Seagull2.Questionnaire.WebApi.Common;

namespace Seagull2.Questionnaire.WebApi.Services
{

    public interface IOptionResultService
    {
        /// <summary>
        /// 加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<IEnumerable<OptionResultModel>> Load(string serialNumber);
        Task<IEnumerable<OptionResultModel>> Load(List<string> codes);
        /// <summary>
        /// 答案解析加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<IEnumerable<OptionResultModel>> Load(string serialNumber, string userId);
        Task<IEnumerable<OptionResultModel>> LoadByConsequenceCode(string consequenceCode, string userId);
        Task<IEnumerable<OptionResultModel>> LoadByConsequenceCode(string consequenceCode);
        /// <summary>
        /// 加载选项分页
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<PaginationCollection<OptionResultModel>> DetailsPage(PaginationInfo pagination, string code);
        /// <summary>
        /// 增加选项
        /// </summary>
        /// <param name="data">选项内容(key：questionCode value:选项内容)</param> 
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns> 
        Task<bool> Add(IEnumerable<OptionResultModel> data, string questionnaireNumber, string consequenceCode, Seagull2User currentUser);
        /// <summary>
        /// 根据问题的Code加载投票信息
        /// </summary>
        /// <param name="questionCodes"></param>
        /// <returns></returns>
        Task<IEnumerable<VoteResultModel>> LoadVoteResult(List<string> questionCodes);
    }
}