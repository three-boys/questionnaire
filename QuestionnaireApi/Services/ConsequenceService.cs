﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Repositories;
using Seagull2.Questionnaire.WebApi.DataObjects; 
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.Common.Extensions; 
using Seagull2.Questionnaire.WebApi.Models.Extensions; 
using Seagull2.Owin.File.Models;
using Seagull2.Owin.File.Services; 
using Seagull2.Questionnaire.WebApi.Calculate;

namespace Seagull2.Questionnaire.WebApi.Services
{
    public class ConsequenceService : IConsequenceService
    {
        private readonly IFileUploadOptionResultService _fileUploadOption_ResultService;
        private readonly IOptionResultService _choiceoption_resultservice;
        private readonly IOptionService _optionService;
        private readonly IConsequenceRepository _consequenceRepository;
        private readonly IQuestionnaireService _questionnaireService;
        private readonly IClientFileService _fileService;
        private readonly IStatisticsSheetService _statisticsSheetService;
        public ConsequenceService(
            IConsequenceRepository consequenceRepository,
            IOptionResultService choiceoption_resultservice,
            IQuestionnaireService questionnaireService,
            IClientFileService fileService,
            IStatisticsSheetService statisticsSheetService,
            IOptionService optionService,
            IFileUploadOptionResultService fileUploadOption_ResultService
            )
        {
            _consequenceRepository = consequenceRepository;
            _choiceoption_resultservice = choiceoption_resultservice;
            _questionnaireService = questionnaireService;
            _fileService = fileService;
            _statisticsSheetService = statisticsSheetService;
            _optionService = optionService;
            _fileUploadOption_ResultService = fileUploadOption_ResultService;
        }
        public async Task<QuestionnairesModel> Load(string serialNumber, string creator)
        {
            var data = await _consequenceRepository.Load(serialNumber, creator);
            var consequence = data.FirstOrDefault();
            var questionnaires = new Questionnaires
            {
                Code = consequence.Code,
                SerialNumber = consequence.SerialNumber,
                Title = consequence.Title,
                Versions = consequence.Versions,

            };
            return QuestionnairesModelExtension.TransferQuestionnairesModel(questionnaires, null, true);
        }
        public async Task<bool> Add(QuestionnairesModel data, Seagull2User currentUser)
        {
            if (currentUser.IsAllowAnonymous)
            {
                currentUserName(data, currentUser);
            }
            var consequence = await ToConsequenceModel(data, currentUser);
            await SaveResultOption(data, consequence.Code, currentUser);
            //保存问卷
            return await _consequenceRepository.Add(consequence);
        }
        /// <summary>
        /// 得到当前答题人的名字（数据来源：考题类型为“考试姓名”的内容）
        /// </summary>
        /// <param name="data"></param>
        /// <param name="currentUser"></param>
        private void currentUserName(QuestionnairesModel data, Seagull2User currentUser)
        {
            var inputName = data.Questions.FirstOrDefault(d => d.QuestionType == "考试姓名");
            if (inputName != null)
            {
                var options = inputName.Options.ToChoiceOption_ResultModelCollection();
                if (options.Count() > 0)
                {
                    currentUser.DisplayName = (options.FirstOrDefault() != null && !string.IsNullOrWhiteSpace(options.FirstOrDefault().AnswerOption)) ? options.FirstOrDefault().AnswerOption : currentUser.DisplayName;
                }
            }
        }
        /// <summary>
        /// 保存问卷
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        private FileInformation[] SaveFile(List<ClientFileInformation> files)
        {
            if (files == null && files.Count() == 0) return null;
            return _fileService.Update(files);
        }
        public async Task<bool> CheckConsequenceAnswered(string serialNumber, string creator)
        {
            var count = await _consequenceRepository.CheckConsequenceCount(serialNumber, creator);
            return count > 0 ? true : false;
        }
        /// <summary>
        /// 答对题目数（只针对考试题目）
        /// </summary>
        /// <param name="data">此数据来源于答卷之后提交的数据</param> 
        /// <returns></returns>
        private async Task<Consequence> AnsweredCount(QuestionnairesModel data, Consequence result)
        {
            ////0.取出所有的考试题目
            var examQuestions = data.Questions.Where(d => d.Category != null && d.Category.ToString().ToLower() == QuestionCategoryEnmu.Exam.ToString().ToLower()).ToList();
            ////通过题目的Code到选项表里取每一题的正确结果
             var rightQuestionCodes = examQuestions.Where(d =>
            d.QuestionForm != null &&
            (d.QuestionForm.ToLower() == QuestionFromEnmu.ChoiceQuestion.ToString().ToLower() ||
             d.QuestionForm.ToLower() != QuestionFromEnmu.ChoiceQuestion.ToString().ToLower())).Select(d => d.Code).ToList();
            ////所有考试题的所有正确答案
            var rightOptions = await _optionService.Load(rightQuestionCodes);
            return GenerateExamStatistics.CalculateExamGoal(examQuestions, rightOptions, result);
        }
        /// <summary>
        /// 转换实体类
        /// </summary>
        /// <param name="data"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<Consequence> ToConsequenceModel(QuestionnairesModel data, Seagull2User currentUser)
        {
            var result = new Consequence()
            {
                Code = Guid.NewGuid().ToString(),
                Title = data.Title,
                SerialNumber = data.SerialNumber,
                CreateTime = DateTime.Now,
                AnsweredTimes = data.AnsweredTimes,
                Creator = currentUser.Id,
                CreatorName = currentUser.DisplayName,
                ValidStatus = true,
                QuestionaireCode = data.Code,
                Versions = data.Versions
            };//保存问卷的答题信息 
            return await AnsweredCount(data, result);
        }
        /// <summary>
        /// 分类保存选项的结果
        /// </summary>
        /// <param name="questions"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<bool> SaveResultOption(QuestionnairesModel data, string consequenceCode, Seagull2User currentUser)
        {
            IEnumerable<QuestionModel> questions = data.Questions;
            List<OptionResultModel> choices = new List<OptionResultModel>();
            List<FileUploadOptionResultModel> files = new List<FileUploadOptionResultModel>();
            foreach (var question in questions.Where(d => d.QuestionType.ToLower() != "上传文件"))
            {
                var option = question.Options.ToChoiceOption_ResultModelCollection().Where(d => !string.IsNullOrWhiteSpace(d.AnswerOption));
                choices.AddRange(option);
            }
            //上传文件
            foreach (var question in questions.Where(d => d.QuestionType.ToLower() == "上传文件"))
            {
                foreach (var item in question.Options.ToFileUploadOptionModelCollection())
                {
                    if (item != null && item.Files != null)
                    {
                        var fileData = SaveFile(item.Files);
                        if (fileData != null)
                            foreach (var file in fileData)
                            {
                                files.Add(new FileUploadOptionResultModel()
                                {
                                    Code = Guid.NewGuid().ToString(),
                                    Creator = currentUser.Id,
                                    CreatorName = currentUser.DisplayName,
                                    QuestionCode = question.Code,
                                    QuestionnaireNumber = data.SerialNumber,
                                    ResourceId = file.ResourceId,
                                    FileName = file.FileName
                                });
                            }
                    }
                }
            }

            await _fileUploadOption_ResultService.Add(files, data.SerialNumber, consequenceCode, currentUser);
            return await _choiceoption_resultservice.Add(choices, data.SerialNumber, consequenceCode, currentUser);
        }

        public async Task<int> CheckConsequenceCount(string serialNumber)
        {
            return await _consequenceRepository.CheckConsequenceCount(serialNumber);
        }

        public async Task<ConsequenceModel> LoadAnswer(string serialNumber, string creator)
        {
            var data = await _consequenceRepository.Load(serialNumber, creator);
            var consequence = data.FirstOrDefault();
            return consequence.ToConsequenceModel();

        }
        public async Task<IEnumerable<ConsequenceModel>> LoadConsequences(string serialNumber)
        {
            var data = await _consequenceRepository.Load(serialNumber);
            var result = data.Select(d => d.ToConsequenceModel());
            return result;
        }

    }
}