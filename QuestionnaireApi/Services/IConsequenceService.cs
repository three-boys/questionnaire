﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Models.StatisticsSheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Services
{
    public interface IConsequenceService
    {
        Task<bool> Add(QuestionnairesModel data, Seagull2User currentUser);
        Task<bool> CheckConsequenceAnswered(string serialNumber, string creator);
        /// <summary>
        /// 检查已答问卷的数量
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        Task<int> CheckConsequenceCount(string serialNumber);
        /// <summary>
        /// 加载问卷
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="creator">答卷人</param>
        /// <returns></returns>
        Task<QuestionnairesModel> Load(string serialNumber, string creator);
        /// <summary>
        /// 加载问卷
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="creator">答卷人</param>
        /// <returns></returns>
        Task<ConsequenceModel> LoadAnswer(string serialNumber, string creator);
        /// <summary>
        /// 查询此问卷的所有信息
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        Task<IEnumerable<ConsequenceModel>> LoadConsequences(string serialNumber);
    }
}
