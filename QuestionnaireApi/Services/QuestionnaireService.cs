﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Repositories;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using System.Data.Entity.Migrations;
using Seagull2.Questionnaire.WebApi.Common.Extensions;
using Seagull2.Permission.Configuration;
using Seagull2.Permission;
using System.Data;
using System.ServiceModel;
using System.Configuration;
using Seagull2.Questionnaire.WebApi.Models.Extensions;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;
using Seagull2.Questionnaire.WebApi.Common;

namespace Seagull2.Questionnaire.WebApi.Services
{
    /// <summary>
    /// 问卷服务
    /// </summary>
    public class QuestionnaireService : IQuestionnaireService
    {
        private readonly IQuestionService _questionService;
        private readonly IQuestionnaireRepository _questionnaireRepository;
        public QuestionnaireService(IQuestionService questionService, IQuestionnaireRepository questionnaireRepository)
        {
            _questionService = questionService;
            _questionnaireRepository = questionnaireRepository;
        }
        /// <summary>
        /// 加载问卷
        /// </summary>
        /// <param name="pagination">分页信息</param>
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns>

        public async Task<PaginationCollection<QuestionnairesModel>> Load(PaginationInfo pagination, Seagull2User currentUser)
        {
            return await _questionnaireRepository.Load(pagination, currentUser);
        }
        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="serialNumber">问卷编号</param> 
        /// <param name="currentUser"></param>
        /// <param name="IsDesign">是否获取设计问卷的数据</param>
        /// <returns></returns>
        public async Task<QuestionnairesModel> Load(string serialNumber, Seagull2User currentUser, bool IsDesign)
        {
            var data = await _questionnaireRepository.Load(serialNumber, currentUser);
            var result = QuestionnairesModelExtension.TransferQuestionnairesModel(data, currentUser, IsDesign);
            if (IsDesign)
            {
                SetQuestionnairesModelHourAndMinute(result);
            }
            return result;
        }
        /// <summary>
        /// 添加或修改问卷
        /// </summary>
        /// <param name="model">问卷内容</param>
        /// <param name="operationType">操作类型</param>
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns>
        public async Task<QuestionnairesModel> AddOrUpdate(QuestionnairesModel model, OperationType operationType, Seagull2User currentUser)
        {
            SetQuestionnairesModelDate(model);
            Questionnaires questionnaire = await SetDefaultValue(model.ToQuestionnaires(), operationType, currentUser);
            if (operationType == OperationType.Delete) return null;
            if (model.Questions != null && model.Questions.Count() > 0 && operationType != OperationType.Add)
            {
                bool QuestionResult = await _questionService.Add(model.Questions, questionnaire.Code, currentUser);
            }
            Questionnaires questionnaireResult = await _questionnaireRepository.AddOrUpdate(await SetQuestionsValue(questionnaire));
            return QuestionnairesModelExtension.TransferQuestionnairesModel(questionnaireResult, currentUser, true);
        }
        /// <summary>
        /// 将开始时间和结束时间合并
        /// </summary>
        private void SetQuestionnairesModelDate(QuestionnairesModel model)
        {
            if (model.StartTime != null)
            {
                var startTime = (DateTime)model.StartTime;
                startTime = startTime.AddHours((model.StartHour == null ? 0 : (double)model.StartHour));
                startTime = startTime.AddMinutes((model.StartMinute == null ? 0 : (double)model.StartMinute));
                startTime = startTime.AddSeconds(0);
                model.StartTime = startTime;
            }
            if (model.EndTime != null)
            {
                var endTime = (DateTime)model.EndTime;
                endTime = endTime.AddHours((model.EndHour == null ? 0 : (double)model.EndHour));
                endTime = endTime.AddMinutes((model.EndMinute == null ? 0 : (double)model.EndMinute));
                endTime = endTime.AddSeconds(0);
                model.EndTime = endTime;
            }
        }
        /// <summary>
        /// 设置问卷的小时和分钟（问卷设计读取时使用）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private void SetQuestionnairesModelHourAndMinute(QuestionnairesModel model)
        {
            if (model.StartTime != null)
            {
                var startTime = (DateTimeOffset)model.StartTime;
                model.StartHour = startTime.Hour;
                model.StartMinute = startTime.Minute;
            }
            if (model.EndTime != null)
            {
                var endTime = (DateTimeOffset)model.EndTime;
                model.EndHour = endTime.Hour;
                model.EndMinute = endTime.Minute;
            }
        }
        public Task<bool> SetQuestionnairesStatus(QuestionnairesModel model, Seagull2User currentUser)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                var data = db.QuestionnairesTable
                .OrderByDescending(d => d.Versions)
                 .Where(d => d.ValidStatus == true && d.SerialNumber == model.SerialNumber)
                 .FirstOrDefault();
                data.Status = model.Status;
                return db.SaveChanges() > 0 ? true : false;
            });
        }
        private async Task<Questionnaires> SetQuestionsValue(Questionnaires model)
        {
            if (model.ValidStatus)
            {
                var questions = await _questionService.Load(model.Code);
                model.Questions = JsonConvert.SerializeObject(questions);
            }
            return model;
        }
        private async Task<Questionnaires> SetDefaultValue(Questionnaires item, OperationType operationType, Seagull2User currentUser)
        {
            string numberMessage = item == null || string.IsNullOrWhiteSpace(item.SerialNumber) ? "空" : item.SerialNumber;
            var model = operationType == OperationType.Add ? item : await _questionnaireRepository.Load(item.SerialNumber, currentUser);
            if (model == null)
            {
                throw new Exception("没有找到编号为" + numberMessage + "的问卷！");
            }
            if (!string.IsNullOrWhiteSpace(model.Creator) && model.Creator != currentUser.Id)
            {
                throw new Exception("您没有权限操作编号为" + numberMessage + "的问卷！");
            }
            var currentDateTime = DateTime.Now;
            string serialNumber = currentDateTime.Year.ToString() + currentDateTime.Month.ToString() + currentDateTime.Day.ToString() + new Random().Next(1000, 9999);
            switch (operationType)
            {
                case OperationType.Add:
                    model.Code = Guid.NewGuid().ToString();
                    model.SerialNumber = serialNumber;
                    model.CreateTime = DateTimeOffset.Now;
                    model.Title = item.Title;
                    model.Summary = item.Summary;
                    model.Creator = currentUser.Id;
                    model.CreatorName = currentUser.DisplayName;
                    model.Status = QuestionnaireStatus.Sketch.GetHashCode();
                    model.ValidStatus = true;
                    model.StartTime = item.StartTime;
                    model.EndTime = item.EndTime;
                    model.TimeLimit = item.TimeLimit;
                    model.IsAllowAnonymous = item.IsAllowAnonymous;
                    model.IsShowAnswer = item.IsShowAnswer;
                    model.IsUserShare = item.IsUserShare;
                    model.IsEndTime = item.IsEndTime;
                    model.IsStartTime = item.IsStartTime;
                    model.Type = item.Type;
                    model.Versions = 1;
                    break;
                case OperationType.Delete:
                    await _questionnaireRepository.LogicDelete(item.SerialNumber, currentUser);
                    break;
                case OperationType.Update:
                    if (model.Versions > 1)
                    {
                        //只保留当前最新版本
                        var delObj = model;
                        // var del = await SetDefaultValue(delObj, OperationType.Delete, currentUser);
                        // await _questionnaireRepository.AddOrUpdate(await SetQuestionsValue(del));
                        model.Code = Guid.NewGuid().ToString();
                    }
                    model.ModifyTime = DateTimeOffset.Now;
                    model.Modifier = currentUser.Id;
                    model.ModifierName = currentUser.DisplayName;
                    //
                    model.Title = item.Title;
                    model.Summary = item.Summary;
                    model.StartTime = item.StartTime;
                    model.EndTime = item.EndTime;
                    model.TimeLimit = item.TimeLimit;
                    model.IsEndTime = item.IsEndTime;
                    model.IsStartTime = item.IsStartTime;
                    model.IsAllowAnonymous = item.IsAllowAnonymous;
                    model.IsShowAnswer = item.IsShowAnswer;
                    model.IsUserShare = item.IsUserShare;
                    model.ValidStatus = true;
                    //model.Type = item.Type;
                    model.Versions += 1;
                    break;
                default:
                    break;
            }
            return model;
        }
        /// <summary>
        /// 检查问卷信息
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public async Task<QuestionnairesModel> CheckSingleQuestionnaires(string serialNumber)
        {
            return await _questionnaireRepository.CheckSingleQuestionnaires(serialNumber);
        }
    }
}

