﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Repositories;
using Seagull2.Questionnaire.WebApi.Models.Extensions;
using Seagull2.Models;
using System.Threading.Tasks;
using Seagull2.Questionnaire.WebApi.Common;

namespace Seagull2.Questionnaire.WebApi.Services
{
    public class OptionResultService : IOptionResultService
    {
        private readonly IOptionResultRepository _choiceOptionResultRepository;
        public OptionResultService(IOptionResultRepository choiceOptionResultRepository)
        {
            _choiceOptionResultRepository = choiceOptionResultRepository;
        }
        public async Task<bool> Add(IEnumerable<OptionResultModel> data, string questionnaireNumber, string consequenceCode, Seagull2User currentUser)
        {
            if (data == null || data.Count() == 0) return false;
            return await _choiceOptionResultRepository.Add(data.ToChoiceOptionCollectionResult(questionnaireNumber, consequenceCode, currentUser));
        }
        /// <summary>
        /// 根据问卷的编号加载选项的信息
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public async Task<IEnumerable<OptionResultModel>> Load(string serialNumber)
        {
            var data = await _choiceOptionResultRepository.Load(serialNumber);
            return data.Select(d => d.ToChoiceOptionModelResult());
        }
        public async Task<PaginationCollection<OptionResultModel>> DetailsPage(PaginationInfo pagination, string code)
        {
            return await _choiceOptionResultRepository.Load(pagination, code);

        }
        /// <summary>
        /// 根据选项的Code加载选项结果信息
        /// </summary>
        /// <param name="codes"></param>
        /// <returns></returns>
        public async Task<IEnumerable<OptionResultModel>> Load(List<string> codes)
        {
            var data = await _choiceOptionResultRepository.Load(codes);
            return data.Select(d => d.ToChoiceOptionModelResult());
        }
        /// <summary>
        /// 根据问题的Code加载投票信息
        /// </summary>
        /// <param name="questionCodes"></param>
        /// <returns></returns>
        public async Task<IEnumerable<VoteResultModel>> LoadVoteResult(List<string> questionCodes)
        {
            var data = await _choiceOptionResultRepository.LoadByQuestionCode(questionCodes);
            var df = data.GroupBy(s => s.Title);
            List<VoteResultModel> votes = new List<VoteResultModel>();
            var dataGroup = data.GroupBy(d => d.QuestionCode);
            foreach (var item in dataGroup)
            {
                foreach (var single in item.GroupBy(d => d.Title))
                {
                    VoteResultModel v = new Models.VoteResultModel();
                    v.QuestionCode = item.Key;
                    v.OptionTitle = single.Key;
                    v.PollNumber = single.Count();
                    v.Percentage = (double)single.Count() / (double)item.Count();
                    votes.Add(v);
                }
            }
            return votes;
        }
        /// <summary>
        /// 根据选项的Code加载某人的选项结果信息
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<OptionResultModel>> Load(string serialNumber, string userId)
        {
            var data = await _choiceOptionResultRepository.Load(serialNumber, userId);
            return data.Select(d => d.ToChoiceOptionModelResult());
        }
        /// <summary>
        /// 根据答题结果的Code加载选项的结果
        /// </summary>
        /// <param name="consequenceCode"></param>
        /// <returns></returns>
        public async Task<IEnumerable<OptionResultModel>> LoadByConsequenceCode(string consequenceCode)
        {
            var data = await _choiceOptionResultRepository.LoadByConsequenceCode(consequenceCode);
            return data.Select(d => d.ToChoiceOptionModelResult());
        }
        /// <summary>
        /// 根据答题结果的Code加载某人的选项结果
        /// </summary>
        /// <param name="consequenceCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<OptionResultModel>> LoadByConsequenceCode(string consequenceCode, string userId)
        {
            var data = await _choiceOptionResultRepository.LoadByConsequenceCode(consequenceCode, userId);
            return data.Select(d => d.ToChoiceOptionModelResult());
        }
    }
}