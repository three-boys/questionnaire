﻿using System.Diagnostics;
using Microsoft.Owin;
using Owin;
using Seagull2.Owin;
using Seagull2.Questionnaire.WebApi;

[assembly: OwinStartup(typeof(Startup))]
namespace Seagull2.Questionnaire.WebApi
{
    public class Startup : DefaultStartup
    {
        public override void Configuration(IAppBuilder app)
        {
            Debug.WriteLine("项目启动");
            base.Configuration(app);
        }
    }
}