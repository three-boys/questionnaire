﻿using Seagull2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi
{
    public class Seagull2User : Seagull2UserBase
    {
        /// <summary>
        /// 是否匿名登录
        /// </summary>
        public bool IsAllowAnonymous { get; set; }
    }
}