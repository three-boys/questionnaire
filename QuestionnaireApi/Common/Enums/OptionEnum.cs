﻿using Seagull2.Questionnaire.WebApi.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Common.Enums
{
    /// <summary>
    /// 选项类型
    /// </summary>
    public enum OptionType
    {
        [EnumDescriptionAttribute("单选")]
        Radio = 1,
        [EnumDescriptionAttribute("多选")]
        CheckBox = 2,
        [EnumDescriptionAttribute("填空")]
        Text = 4,
        [EnumDescriptionAttribute("下拉")]
        Dropdown = 8,
        [EnumDescriptionAttribute("简答")]
        TextArea = 16,
        [EnumDescriptionAttribute("判断")]
        Estimate = 32,
        [EnumDescriptionAttribute("滑动条")]
        Slider = 64,
        [EnumDescriptionAttribute("排序")]
        Rank = 128,
        [EnumDescriptionAttribute("上传文件")]
        File = 256

    }
}