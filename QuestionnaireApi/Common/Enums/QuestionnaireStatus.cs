﻿using Seagull2.Questionnaire.WebApi.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Common.Enums
{
    /// <summary>
    /// 问卷状态
    /// </summary>
    public enum QuestionnaireStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        [EnumDescriptionAttribute("草稿")]
        Sketch = 1,
        /// <summary>
        /// 运行
        /// </summary>
        [EnumDescriptionAttribute("运行")]
        Running = 2,
        /// <summary>
        /// 已停止
        /// </summary>
        [EnumDescriptionAttribute("已停止")]
        Stopped = 4
    }
    /// <summary>
    /// 问卷加载状态
    /// </summary>
    public enum QuestionnaireLoadStatus
    {

        /// <summary>
        /// 加载成功
        /// </summary>
        Success = 1,
        /// <summary>
        /// 考试还未开始
        /// </summary> 
        NotStarted = 2,
        /// <summary>
        /// 考试已结束
        /// </summary> 
        Over = 4,
        /// <summary>
        /// 问卷当前处于非运行状态
        /// </summary> 
        NoRunning = 8,
        /// <summary>
        /// 已答
        /// </summary>
        Answered = 16
    }

}