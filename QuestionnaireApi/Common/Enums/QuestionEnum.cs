﻿using Seagull2.Questionnaire.WebApi.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Common.Enums
{
    /// <summary>
    /// 题目的必答性
    /// </summary>
    public enum QuestionNecessity
    {
        /// <summary>
        /// 必答
        /// </summary>
        [EnumDescriptionAttribute("必答")]
        Required = 1,
        /// <summary>
        /// 非必答
        /// </summary>
        [EnumDescriptionAttribute("非必答")]
        Optional = 2
    }
    /// <summary>
    /// 考题的分类
    /// </summary>
    public enum QuestionCategoryEnmu
    {
        /// <summary>
        /// 考试
        /// </summary>
        [EnumDescriptionAttribute("考试")]
        Exam = 1,
        /// <summary>
        /// 投票
        /// </summary>
        [EnumDescriptionAttribute("投票")]
        Vote = 2,
        /// <summary>
        /// 其他
        /// </summary>
        [EnumDescriptionAttribute("其他")]
        Other = 4
    } 
    public enum QuestionSelectivityEnmu
    {
        /// <summary>
        /// 单项
        /// </summary>
        [EnumDescriptionAttribute("单项")]
        Single = 1,
        /// <summary>
        /// 多项
        /// </summary>
        [EnumDescriptionAttribute("多项")]
        Multiple = 2
    }
    public enum QuestionFromEnmu
    {
        /// <summary>
        /// 选择题
        /// </summary>
        ChoiceQuestion = 1,
        /// <summary>
        /// 填空题
        /// </summary>
        FillInQuestion = 2,
        /// <summary>
        /// 上传文件
        /// </summary>
        FileUpload = 4
    }
    public enum QuestionTypeEnmu
    {
        单选题 = 1,
        多选题 = 2,
        考试单选题 = 3,
        考试多选题 = 4,
        下拉框单选 = 5,
        评分单选题 = 6,
        评分多选题 = 7,
        投票多选题 = 8,
        投票单选题 = 9,
        单项填空 = 10,
        矩阵填空 = 11,
        量表题 = 12,
        判断题 = 13,
        考试单项填空题 = 14,
        考试多项填空题 = 15,
        考试简答 = 16,
        考试姓名 = 17,
        基本信息 = 18,
        分页 = 19,
        段落说明 = 20,
        滚动条 = 21,
        排序 = 22,
        上传文件 = 23,
        手机 = 24,
        email = 25
    }
}