﻿using Seagull2.Questionnaire.WebApi.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Common.Enums
{
    /// <summary>
    /// 问卷类型
    /// </summary>
    public enum questionnairetype
    {
        /// <summary>
        /// 调查
        /// </summary>
        Investigation = 1,
        /// <summary>
        /// 考试
        /// </summary>
        Exam = 2,
        /// <summary>
        /// 投票
        /// </summary>
        Vote = 4,
        /// <summary>
        /// 评测
        /// </summary>
        Evaluating = 8

    }
}