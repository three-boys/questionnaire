﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Common.Enums
{
    /// <summary>
    /// 对数据的操作方式
    /// </summary>
    public enum OperationType
    {
        /// <summary>
        /// 增加
        /// </summary>
        Add = 1,
        /// <summary>
        /// 删除
        /// </summary>
        Delete = 2,
        /// <summary>
        /// 修改
        /// </summary>
        Update = 4,
        /// <summary>
        /// 复制
        /// </summary>
        Copy = 8,
        UpdateStatus=16
    }
}