﻿using Seagull2.Questionnaire.WebApi.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Common.Extensions
{
    public static class EnumDescription
    {
        /// <summary>
        /// 获取枚举的中文注释
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetEnumDescription<TEnum>(this TEnum value)
        {
            Type enumType = typeof(TEnum);
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("不支持该类型。");
            }
            FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
            if (fieldInfo == null) return value.ToString();
            object[] attribArray = fieldInfo.GetCustomAttributes(false);
            EnumDescriptionAttribute attrib = (EnumDescriptionAttribute)attribArray[0];
            return attrib.Description;
        }
    }
}