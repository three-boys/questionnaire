﻿using Newtonsoft.Json;
using Seagull2.Questionnaire.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Common.Extensions
{
    /// <summary>
    /// 选项的扩展
    /// </summary>
    public static class OptionExtensions
    {
        /// <summary>
        /// 将字符串的填空选项转换成填空选项类型的集合
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<OptionModel> ToChoiceOptionModelCollection(this object data)
        {
            return JsonConvert.DeserializeObject<List<OptionModel>>(data.ToString());
        }
        /// <summary>
        /// 将字符串的填空选项转换成填空选项类型的集合（结果选项）
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<OptionResultModel> ToChoiceOption_ResultModelCollection(this object data)
        {
            return JsonConvert.DeserializeObject<List<OptionResultModel>>(data.ToString());
        }
        /// <summary>
        /// 将字符串的文件上传选项转换成填空选项类型的集合
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static List<FileUploadOptionModel> ToFileUploadOptionModelCollection(this object data)
        {
            return JsonConvert.DeserializeObject<List<FileUploadOptionModel>>(data.ToString());
        }
    }
}