﻿using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Models.StatisticsSheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Calculate
{
    /// <summary>
    /// 生成统计信息
    /// </summary>
    public static class GenerateStatisticsAnalysis
    {
        /// <summary>
        /// 计算统计信息
        /// </summary>
        /// <param name="questions">所有题目</param>
        /// <param name="choiceResult">选则项的答案</param>
        /// <param name="fillResult">填空的答案</param>
        /// <returns>所有题目的统计结果</returns>
        public static IEnumerable<QuestionStatisticsModel> CalculateStatistics(IEnumerable<QuestionModel> questions, IEnumerable<OptionResultModel> choiceResult)
        {
            List<QuestionStatisticsModel> list = new List<QuestionStatisticsModel>();

            foreach (var item in questions)
            {
                QuestionStatisticsModel qsm = new QuestionStatisticsModel();
                qsm.NO = item.NO;
                qsm.QuestionType = item.QuestionType;
                qsm.Title = item.Title;
                qsm.Code = item.Code;
                List<OptionStatisticsModel> osms = new List<OptionStatisticsModel>();
                if (item.QuestionForm.ToLower() == QuestionFromEnmu.ChoiceQuestion.ToString().ToLower())
                {
                    var op = item.Options == null ? null : (List<OptionModel>)item.Options;
                    //取出正确答案 
                    var correctnumber = op.Where(p => p.IsCorrect).ToList();
                    //取出当前题目已答信息并根据人分组
                    var choiceResults = choiceResult.Where(p => p.QuestionCode == item.Code && p.AnswerOption == "True").GroupBy(p => p.Creator);
                    var count = 0;
                    foreach (var j in choiceResults)
                    {
                        if (CalculateResult.CompareOptionRezulti(correctnumber, j.ToList()))
                        {
                            count += 1;
                        }
                    }
                    qsm.CorrectNumber = count;
                    foreach (var o in op)
                    {
                        OptionStatisticsModel osm = new OptionStatisticsModel();

                        double answerAmount = choiceResult.Where(d => d.AnswerOption == "True" && d.Title == o.Title && d.QuestionCode == item.Code).Count();
                        double answerRateCount = choiceResult.Where(d => d.AnswerOption == "True" && d.QuestionCode == item.Code).Count();
                        if (answerRateCount > 0)
                        {
                            osm.AnswerRate = (answerAmount / answerRateCount) * 100;
                        }

                        osm.AnswerAmount = Convert.ToInt32(answerAmount);
                        osm.Title = o.Title;

                        osms.Add(osm);
                    }
                }
                else if (item.QuestionForm.ToLower() != QuestionFromEnmu.ChoiceQuestion.ToString().ToLower() && item.QuestionType != "上传文件")
                {
                    var ofs = item.Options == null ? null : (List<OptionModel>)item.Options;

                    //取出正确答案 
                    var correctnumber = ofs.Where(p => p.TheAnswer != null).ToList();
                    //取出当前题目已答信息并根据人分组
                    var fillResults = choiceResult.Where(p => p.QuestionCode == item.Code && !string.IsNullOrWhiteSpace(p.AnswerOption)).GroupBy(p => p.Creator);

                    int fillcount = 0;

                    //如果包含答案为true进行判断，否则返回false
                    foreach (var j in fillResults)
                    {
                        if (CalculateResult.CompareOptionRezulti(correctnumber, j.ToList()))
                        {
                            fillcount += 1;
                        }
                    }
                    qsm.CorrectNumber = fillcount;

                    foreach (var o in ofs)
                    {
                        OptionStatisticsModel osm = new OptionStatisticsModel();

                        double answerAmount = choiceResult.Where(d => d.AnswerOption != null && d.Title == o.Title && d.QuestionCode == item.Code).Count();
                        double answerRateCount = choiceResult.Where(d => d.AnswerOption != null && d.QuestionCode == item.Code).Count();
                        if (answerRateCount > 0)
                        {
                            osm.AnswerRate = (answerAmount / answerRateCount) * 100;
                        }


                        osm.AnswerAmount = Convert.ToInt32(answerAmount);
                        osm.Title = o.Title;
                        osms.Add(osm);

                    }
                }
                qsm.Options = osms;
                list.Add(qsm);
            }
            return list;
        }
    }
}