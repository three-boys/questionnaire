﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Seagull2.Questionnaire.WebApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Calculate
{
    /// <summary>
    /// 生成答题详情统计Excel表
    /// </summary>
    public static class GenerateExcelConsequencesAnalysis
    {
        public static Stream StatisticsExcelStream(IEnumerable<ConsequenceModel> ConsequenceList)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet = workbook.CreateSheet("答题详情统计表");
            IRow row = sheet.CreateRow(0);
            row.CreateCell(0).SetCellValue("答题人");
            row.CreateCell(1).SetCellValue("答题所用时间（秒）");
            row.CreateCell(2).SetCellValue("得分");
            row.CreateCell(3).SetCellValue("答对题数");
            row.CreateCell(4).SetCellValue("开始答题时间");
            //水平对齐
            row.Cells[4].CellStyle.Alignment = HorizontalAlignment.Center;
            //垂直居中
            row.Cells[4].CellStyle.VerticalAlignment = VerticalAlignment.Center;
            //新建一个字体样式对象
            ICellStyle style = workbook.CreateCellStyle();
            IFont font = workbook.CreateFont();
            //设置字体加粗样式
            font.Boldweight = short.MaxValue;
            //使用SetFont方法将字体样式添加到单元格样式中 
            style.SetFont(font);
            style.Alignment = HorizontalAlignment.Center;
            style.VerticalAlignment = VerticalAlignment.Center;

            row.Cells[0].CellStyle = style;
            row.Cells[1].CellStyle = style;
            row.Cells[2].CellStyle = style;
            row.Cells[3].CellStyle = style;
            row.Cells[4].CellStyle = style;

            //设置单元格宽度高度
            row.Height = 30 * 20;
            sheet.SetColumnWidth(0, 5000);
            sheet.SetColumnWidth(1, 5000);
            sheet.SetColumnWidth(2, 5000);
            sheet.SetColumnWidth(3, 5000);
            sheet.SetColumnWidth(4, 5000);
            int count = 1;
            var result = ConsequenceList.ToList();

            for (int i = 0; i < result.Count(); i++)
            {
                IRow row1 = sheet.CreateRow(count++);
                row1.CreateCell(0).SetCellValue(result[i].CreatorName);
                row1.CreateCell(1).SetCellValue(result[i].AnsweredTimes);
                row1.CreateCell(2).SetCellValue(result[i].AggregateScore);
                row1.CreateCell(3).SetCellValue(result[i].CorrectQuestionCount);
                row1.CreateCell(4).SetCellValue(result[i].CreateTime.ToString("yyyy:MM:ss HH:mm:ss"));
            }
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            workbook.Write(ms);

            ms.Position = 0;

            return ms;
        }

    }
}