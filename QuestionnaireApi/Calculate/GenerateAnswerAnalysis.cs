﻿using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Models.StatisticsSheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Calculate
{
    /// <summary>
    /// 生成答题信息
    /// </summary>
    public static class GenerateAnswerAnalysis
    {
        /// <summary>
        /// 生成答案解析(考试题时使用)
        /// </summary>
        /// <param name="choiceResult">选择的结果</param>
        /// <param name="fillResult">填空的结果</param>
        /// <param name="answerAnaly">所有的题目</param>
        /// <returns></returns>
        public static List<AnswerAnalysisModel> AnswerAnalysis(
            IEnumerable<OptionResultModel> choiceResult,
            IEnumerable<QuestionModel> answerAnaly)
        {
            List<AnswerAnalysisModel> lst = new List<AnswerAnalysisModel>();

            foreach (var item in answerAnaly.Where(p => p.Category == QuestionCategoryEnmu.Exam.ToString().ToLower()).ToList())
            {
                AnswerAnalysisModel list = new AnswerAnalysisModel();
                list.Code = item.Code;
                list.No = item.NO;
                list.QuestionType = item.QuestionType;
                list.Title = item.Title;
                list.Score = item.Score;
                if (item.QuestionForm.ToLower() == QuestionFromEnmu.ChoiceQuestion.ToString().ToLower())
                {
                    var aamop = item.Options == null ? null : (List<OptionModel>)item.Options;
                    foreach (var aop in aamop.Where(p => p.IsCorrect))
                    {
                        list.RightKey += (string.IsNullOrWhiteSpace(list.RightKey) ? aop.Title : ("|" + aop.Title));
                    }
                    var youamop = choiceResult.Where(p => p.AnswerOption == "True" && p.QuestionCode == item.Code).ToList();
                    foreach (var j in youamop)
                    {
                        list.YourAnswer += (string.IsNullOrWhiteSpace(list.YourAnswer) ? j.Title : ("|" + j.Title));
                    }
                    list.IsRight = CalculateResult.CompareOptionRezulti(aamop, youamop);
                }
                if (item.QuestionForm.ToLower() != QuestionFromEnmu.ChoiceQuestion.ToString().ToLower())
                {
                    var aamfi = item.Options == null ? null : (List<OptionModel>)item.Options;
                    foreach (var afi in aamfi)
                    {
                        list.RightKey = afi.TheAnswer;
                    }
                    var youfis = choiceResult.Where(p => !string.IsNullOrWhiteSpace(p.AnswerOption) && p.QuestionCode == item.Code).ToList();
                    foreach (var s in youfis)
                    {
                        list.YourAnswer += (string.IsNullOrWhiteSpace(list.YourAnswer) ? s.AnswerOption : ("|" + s.AnswerOption));
                    }
                    list.IsRight = CalculateResult.CompareOptionRezultiFillIn(aamfi, youfis);
                }
                lst.Add(list);
            }
            return lst;
        }
        /// <summary>
        /// 生成答案卡（所有题型通用）
        /// </summary>
        /// <param name="choiceResult">选择的结果</param>
        /// <param name="fillResult">填空的结果</param>
        /// <param name="fileUploadResult">文件上传的结果</param>
        /// <param name="answerAnaly">所有的题目</param>
        public static List<AnswerAnalysisModel> AnswerSheet(
            IEnumerable<OptionResultModel> choiceResult,
            IEnumerable<FileUploadOptionResultModel> fileUploadResult,
            IEnumerable<QuestionModel> answerAnaly)
        {
            List<AnswerAnalysisModel> lst = new List<AnswerAnalysisModel>();

            foreach (var item in answerAnaly.ToList())
            {
                AnswerAnalysisModel list = new AnswerAnalysisModel();
                list.Code = item.Code;
                list.No = item.NO;
                list.QuestionType = item.QuestionType;
                list.Title = item.Title;
                list.Score = item.Score;
                if (item.QuestionForm.ToLower() == QuestionFromEnmu.ChoiceQuestion.ToString().ToLower())
                {
                    var youamop = choiceResult.Where(p => p.AnswerOption == "True" && p.QuestionCode == item.Code).ToList();
                    foreach (var j in youamop)
                    {
                        list.YourAnswer += (string.IsNullOrWhiteSpace(list.YourAnswer) ? j.Title : ("|" + j.Title));
                    }
                }
                if (item.QuestionForm.ToLower() != QuestionFromEnmu.ChoiceQuestion.ToString().ToLower() && item.QuestionType != "文件上传")
                {
                    var youfis = choiceResult.Where(p => !string.IsNullOrWhiteSpace(p.AnswerOption) && p.QuestionCode == item.Code).ToList();
                    foreach (var s in youfis)
                    {
                        list.YourAnswer += (string.IsNullOrWhiteSpace(list.YourAnswer) ? s.AnswerOption : ("|" + s.AnswerOption));
                    }
                }
                if (item.QuestionType == "上传文件")
                {
                    var youfis = fileUploadResult.Where(p => p.QuestionCode == item.Code).ToList();
                    list.YourAnswer = youfis.FirstOrDefault() == null ? null : youfis.FirstOrDefault().ResourceId;
                }
                lst.Add(list);
            }
            return lst;
        }
    }
}