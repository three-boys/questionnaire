﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Seagull2.Questionnaire.WebApi.Models.StatisticsSheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Seagull2.Questionnaire.WebApi.Calculate
{
    /// <summary>
    /// 生成统计分析Excel表
    /// </summary>
    public static class GenerateExcelAnalysis
    {
        public static Stream StatisticsExcelStream(List<QuestionStatisticsModel> QuestionList)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            ISheet sheet = workbook.CreateSheet("统计分析表");
            IRow row = sheet.CreateRow(0);

            row.CreateCell(0).SetCellValue("题号");
            row.CreateCell(1).SetCellValue("标题");
            row.CreateCell(2).SetCellValue("题型");
            row.CreateCell(3).SetCellValue("选项");
            row.CreateCell(4).SetCellValue("计数");
            row.CreateCell(5).SetCellValue("比例");
            //水平对齐
            row.Cells[5].CellStyle.Alignment = HorizontalAlignment.Center;
            //垂直居中
            row.Cells[5].CellStyle.VerticalAlignment = VerticalAlignment.Center;
            //新建一个字体样式对象
            ICellStyle style = workbook.CreateCellStyle();
            IFont font = workbook.CreateFont();
            //设置字体加粗样式
            font.Boldweight = short.MaxValue;
            //使用SetFont方法将字体样式添加到单元格样式中 
            style.SetFont(font);
            style.Alignment = HorizontalAlignment.Center;
            style.VerticalAlignment = VerticalAlignment.Center;

            row.Cells[0].CellStyle = style;
            row.Cells[1].CellStyle = style;
            row.Cells[2].CellStyle = style;
            row.Cells[3].CellStyle = style;
            row.Cells[4].CellStyle = style;
            row.Cells[5].CellStyle = style;

            //设置单元格宽度高度
            row.Height = 30 * 20;
            sheet.SetColumnWidth(0, 5000);
            sheet.SetColumnWidth(1, 5000);
            sheet.SetColumnWidth(2, 5000);
            sheet.SetColumnWidth(3, 5000);
            sheet.SetColumnWidth(4, 5000);
            sheet.SetColumnWidth(5, 5000);
            int count = 1;
            var result = QuestionList.ToList();

            int RowCount = 1;

            for (int i = 0; i < result.Count(); i++)
            {
                for (int o = 0; o < result[i].Options.Count(); o++)
                {
                    var optionsTitle = "";
                    IRow row1 = sheet.CreateRow(count++);
                    //总行数
                    RowCount = RowCount + 1;
                    row1.CreateCell(0).SetCellValue(Convert.ToInt32(result[i].NO));
                    //去掉标题p标签
                    optionsTitle = ReplaceHtmlTag(result[i].Title);

                    row1.CreateCell(1).SetCellValue(optionsTitle);
                    row1.CreateCell(2).SetCellValue(result[i].QuestionType);
                    row1.CreateCell(3).SetCellValue(result[i].Options[o].Title);
                    row1.CreateCell(4).SetCellValue(result[i].Options[o].AnswerAmount);
                    row1.CreateCell(5).SetCellValue(result[i].Options[o].AnswerRate + "%");
                    //合并单元格
                    if ((o + 1) == result[i].Options.Count())
                    {
                        sheet.AddMergedRegion(new CellRangeAddress(RowCount - result[i].Options.Count(), RowCount - 1, 0, 0));
                        sheet.AddMergedRegion(new CellRangeAddress(RowCount - result[i].Options.Count(), RowCount - 1, 1, 1));
                        sheet.AddMergedRegion(new CellRangeAddress(RowCount - result[i].Options.Count(), RowCount - 1, 2, 2));
                    }
                }
            }
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            workbook.Write(ms);

            ms.Position = 0;

            return ms;
        }

        public static string ReplaceHtmlTag(string html, int length = 0)
        {
            string strText = System.Text.RegularExpressions.Regex.Replace(html, "<[^>]+>", "");
            strText = System.Text.RegularExpressions.Regex.Replace(strText, "&[^;]+;", "");

            if (length > 0 && strText.Length > length)
                return strText.Substring(0, length);

            return strText;
        }


    }
}