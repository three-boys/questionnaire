﻿using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.Common.Extensions;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Calculate
{
    /// <summary>
    /// 考试统计
    /// </summary>
    public static class GenerateExamStatistics
    {
        /// <summary>
        /// 计算考试分数
        /// </summary>
        /// <param name="examQuestions"></param>
        /// <param name="rightOptions"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static Consequence CalculateExamGoal(List<QuestionModel> examQuestions, IEnumerable<OptionModel> rightOptions, Consequence result)
        {
            // 总得分 
            double aggregateScore = 0;
            // 答对题数 
            int correctQuestionCount = 0;
            //1.取设置的正确选项 //2.和当前的选项进行对比
            foreach (var exam in examQuestions)
            {
                if (exam.QuestionForm.ToLower() == QuestionFromEnmu.ChoiceQuestion.ToString().ToLower())
                {
                    //取出已设置好的正确结果的Option
                    var rightChoice = rightOptions.Where(d => d.QuestionCode == exam.Code);
                    //RightChoice与exam.Options对比是否正确，并且得出分数
                    if (CalculateResult.CompareOptionRezulti(rightChoice.ToList(), exam.Options.ToChoiceOption_ResultModelCollection()))
                    {
                        correctQuestionCount++;
                        aggregateScore += exam.Score;
                    }
                }
                else
                {
                    //取出已设置好的正确结果的Option
                    var rightFillIn = rightOptions.Where(d => d.QuestionCode == exam.Code);
                    //RightChoice与exam.Options对比是否正确，并且得出分数
                    if (CalculateResult.CompareOptionRezultiFillIn(rightOptions.ToList(), exam.Options.ToChoiceOption_ResultModelCollection()))
                    {
                        correctQuestionCount++;
                        aggregateScore += exam.Score;
                    }
                }
            }
            //赋值
            result.AggregateScore = aggregateScore;
            result.CorrectQuestionCount = correctQuestionCount;
            return result;
        }
    }
}