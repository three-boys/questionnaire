﻿using Seagull2.Questionnaire.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Calculate
{
    /// <summary>
    /// 计算结果静态类
    /// </summary>
    public static class CalculateResult
    {
        /// <summary>
        ///比较单个选择题的选项集合是否相同
        /// </summary>
        /// <param name="optionlist">保存正确答案的选项集合</param>
        /// <param name="optionResultlist">作答的结果</param>
        /// <returns>相同返回True，反之False</returns>
        public static bool CompareOptionRezulti(List<OptionModel> optionlist, List<OptionResultModel> optionResultlist)
        {
            if (optionlist.Count() == 0 || optionResultlist.Count() == 0)
            {
                return false;
            }
            optionlist = optionlist.Where(d => d.IsCorrect).OrderBy(d => d.SortNo).ToList();
            optionResultlist = optionResultlist.Where(d => d.AnswerOption == "True").OrderBy(d => d.SortNo).ToList();

            if (optionlist.Count == optionResultlist.Count)
            {
                for (int i = 0; i < optionlist.Count(); i++)
                {
                    if (optionlist[i].Title != optionResultlist[i].Title)
                    {
                        return false;
                    }
                }
                return true;
            }

            return false;
        }
        /// <summary>
        ///比较单个填空题的选项集合是否相同
        /// </summary>
        /// <param name="optionlist">保存正确答案的选项集合</param>
        /// <param name="optionResultlist">作答的结果</param>
        /// <returns>相同返回True，反之False</returns>
        /// <returns></returns>
        public static bool CompareOptionRezultiFillIn(List<OptionModel> optionlist, List<OptionResultModel> optionResultlist)
        {
            optionlist = optionlist.Where(d => !string.IsNullOrWhiteSpace(d.TheAnswer)).OrderBy(d => d.SortNo).ToList();
            optionResultlist = optionResultlist.Where(d => !string.IsNullOrWhiteSpace(d.AnswerOption)).OrderBy(d => d.SortNo).ToList();
            if (optionlist.Count() == 0)
            {
                return true;
            }
            if (optionResultlist.Count() == 0)
            {
                return false;
            }
            foreach (var option in optionlist)
            {
                foreach (var optionResult in optionResultlist)
                {
                    foreach (var item in option.TheAnswer.Split('|'))
                    {
                        if (option.IsContainsAnswer == true)
                        {
                            if (optionResult.AnswerOption.Trim().Contains(item.Trim()))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if (optionResult.AnswerOption.Trim() == item.Trim())
                            {
                                return true;
                            }
                        }
                    }

                }
            }

            return false;
        }
    }
}