﻿using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Repositories.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    public class FileUploadOptionResultRepository : IFileUploadOptionResultRepository
    {
        public Task<bool> Add(IEnumerable<FileUploadOptionResult> data)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                db.FileUploadOption_ResultTable.AddRange(data);
                return db.SaveChanges() > 0 ? true : false;
            });
        }

        public Task<IEnumerable<FileUploadOptionResult>> Load(string questionnaireNumber)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.FileUploadOption_ResultTable.Where(d => d.QuestionnaireNumber == questionnaireNumber && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }

        public Task<IEnumerable<FileUploadOptionResult>> Load(string questionnaireNumber, string userId)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.FileUploadOption_ResultTable.Where(d => d.QuestionnaireNumber == questionnaireNumber && d.ValidStatus == true && d.Creator == userId).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }

        public Task<IEnumerable<FileUploadOptionResult>> LoadByConsequenceCode(string consequenceCode)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.FileUploadOption_ResultTable.Where(d => d.ConsequenceCode == consequenceCode && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }

        public Task<IEnumerable<FileUploadOptionResult>> LoadByConsequenceCode(string consequenceCode, string userId)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.FileUploadOption_ResultTable.Where(d => d.ConsequenceCode == consequenceCode && d.ValidStatus == true && d.Creator == userId).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }
    }
}