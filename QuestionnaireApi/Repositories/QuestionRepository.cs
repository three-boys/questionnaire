﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Repositories;
using Seagull2.Questionnaire.WebApi.Models.Extensions;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Newtonsoft.Json;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    /// <summary>
    /// 题目的服务
    /// </summary>
    public class QuestionRepository : IQuestionRepository
    {
        public Task<bool> Add(IEnumerable<Question> data)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                db.QuestionTable.AddRange(data);
                return db.SaveChanges() > 0 ? true : false;
            });
        }
        /// <summary>
        /// 根据问卷Code加载题目
        /// </summary>
        /// <param name="questionnaireCode">问卷</param>
        /// <returns></returns>
        public Task<IEnumerable<Question>> Load(string questionnaireCode)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.QuestionTable.Where(d => d.QuestionnaireCode == questionnaireCode && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }
    }
}