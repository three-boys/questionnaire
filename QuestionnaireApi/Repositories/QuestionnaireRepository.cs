﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Repositories;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using System.Data.Entity.Migrations;
using Seagull2.Questionnaire.WebApi.Common.Extensions;
using Seagull2.Permission.Configuration;
using Seagull2.Permission;
using System.Data;
using System.ServiceModel;
using System.Configuration;
using Seagull2.Questionnaire.WebApi.Models.Extensions;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;
using Seagull2.Questionnaire.WebApi.Common;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    /// <summary>
    /// 问卷服务
    /// </summary>
    public class QuestionnaireRepository : IQuestionnaireRepository
    {
        /// <summary>
        /// 加载问卷
        /// </summary>
        /// <param name="pagination">分页信息</param>
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns>Seagull2User
        public Task<PaginationCollection<QuestionnairesModel>> Load(PaginationInfo pagination, Seagull2User currentUser)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                var list = from p in db.QuestionnairesTable
                          .Where(d => d.ValidStatus == true && d.Creator == currentUser.Id)
                           group p by p.SerialNumber into grp
                           let maxVersions = grp.Max(p => p.Versions)
                           from row in grp
                           where (row.Versions == maxVersions)
                           select row;
                var data = list.OrderByDescending(d => d.CreateTime).Skip(pagination.PageSize * (pagination.PageIndex - 1)).Take(pagination.PageSize)
                 .Select(d => new QuestionnairesModel()
                 {
                     Code = d.Code,
                     SerialNumber = d.SerialNumber,
                     Title = d.Title,
                     Status = d.Status,
                     CreateTime = d.CreateTime,
                     Type = (questionnairetype)d.Type,
                     Versions = d.Versions,
                     AnsweredCount = db.ConsequenceTable.Where(f => f.SerialNumber == d.SerialNumber && f.ValidStatus == true).Count()
                 }).ToList();

                return new PaginationCollection<QuestionnairesModel>(pagination)
                {
                    Data = data,
                    PaginationInfo = pagination,
                    TotalItems = list.Count()
                };
            });
        }
        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="serialNumber">问卷编号</param> 
        /// <returns></returns>Seagull2User
        public Task<Questionnaires> Load(string serialNumber, Seagull2User currentUser)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.QuestionnairesTable
             .Where(d => d.ValidStatus == true && d.SerialNumber == serialNumber).OrderByDescending(p => p.Versions)
             .FirstOrDefault();
            });
        }
        public Task<bool> SetQuestionnairesStatus(QuestionnairesModel model, Seagull2User currentUser)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                var data = db.QuestionnairesTable
                 .Where(d => d.ValidStatus == true && d.SerialNumber == model.SerialNumber)
                 .FirstOrDefault();
                data.Status = model.Status;
                return db.SaveChanges() > 0 ? true : false;
            });
        }
        /// <summary>
        /// 添加或修改
        /// </summary>
        /// <param name="model">需要先SetDefaultValue(),设置初始值</param> 
        /// <returns></returns>
        public Task<Questionnaires> AddOrUpdate(Questionnaires model)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                db.QuestionnairesTable.AddOrUpdate(model);
                db.SaveChanges();
                return model;
            });

        }
        public Task<bool> LogicDelete(string serialNumber, Seagull2User currentUser)
        {

            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                string sql = string.Format(" UPDATE [dbo].[Questionnaires] SET [ValidStatus]=0,[Modifier]='{0}',[ModifierName]='{1}',[ModifyTime]=GETDATE() WHERE SerialNumber='{2}'",
                    currentUser.Id, currentUser.DisplayName, serialNumber);
                db.Database.ExecuteSqlCommand(sql);
                return db.SaveChanges() > 0 ? true : false;
            });
        }

        public Task<QuestionnairesModel> CheckSingleQuestionnaires(string serialNumber)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                var da = db.QuestionnairesTable.Where(d => d.ValidStatus == true && d.SerialNumber == serialNumber).OrderByDescending(d => d.Versions).ToList();
                var data = db.QuestionnairesTable
                 .Where(d => d.ValidStatus == true && d.SerialNumber == serialNumber)
                 .OrderByDescending(d => d.Versions)
                 .Select(
                   d => new QuestionnairesModel()
                   {
                       SerialNumber = d.SerialNumber,
                       Status = d.Status,
                       Creator = d.Creator,
                       EndTime = d.EndTime,
                       IsEndTime = d.IsEndTime,
                       IsStartTime = d.IsStartTime,
                       IsAllowAnonymous = d.IsAllowAnonymous,
                       IsShowAnswer = d.IsShowAnswer,
                       IsUserShare = d.IsUserShare,
                       Code = d.Code,
                       StartTime = d.StartTime,
                       Type = (questionnairetype)d.Type
                   }
                    )
                 .FirstOrDefault();
                return data;
            });
        }

    }
}

