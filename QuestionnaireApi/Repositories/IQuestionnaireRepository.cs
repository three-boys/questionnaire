﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    /// <summary>
    /// 问卷服务接口
    /// </summary>
    public interface IQuestionnaireRepository
    {
        /// <summary>
        /// 加载问卷
        /// </summary>
        /// <param name="pagination">分页信息</param>
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns>
        Task<PaginationCollection<QuestionnairesModel>> Load(PaginationInfo pagination, Seagull2User currentUser);
        /// <summary>
        /// 获取单条
        /// </summary>
        /// <param name="number">问卷编号</param> 
        /// <returns></returns>
        Task<Questionnaires> Load(string number, Seagull2User currentUser);
        /// <summary>
        /// 添加或修改问卷
        /// </summary>
        /// <param name="model">问卷内容</param>
        /// <returns></returns>
        Task<Questionnaires> AddOrUpdate(Questionnaires model);
        /// <summary>
        /// 设置状态
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<bool> SetQuestionnairesStatus(QuestionnairesModel model, Seagull2User currentUser);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="number"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<bool> LogicDelete(string number, Seagull2User currentUser);
        /// <summary>
        /// 检查问卷信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<QuestionnairesModel> CheckSingleQuestionnaires(string serialNumber);
    }
}
