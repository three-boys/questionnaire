﻿using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    public class FileUploadOptionRepository : IFileUploadOptionRepository
    {
        /// <summary>
        /// 添加或修改填空选项
        /// </summary>
        /// <param name="data">内容</param> 
        /// <returns></returns> 
        public Task<bool> Add(IEnumerable<FileUploadOption> data)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                db.FileUploadOptionTable.AddRange(data);
                return db.SaveChanges() > 0 ? true : false;
            });
        }
        /// <summary>
        /// 加载填空选项
        /// </summary>
        /// <param name="questionCode">题目</param>
        /// <returns></returns>
        public Task<IEnumerable<FileUploadOption>> Load(string questionCode)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.FileUploadOptionTable.Where(d => d.QuestionCode == questionCode && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }
        /// <summary>
        /// 加载填空选项
        /// </summary>
        /// <param name="questionCode">题目</param>
        /// <returns></returns>
        public Task<IEnumerable<FileUploadOption>> Load(List<string> questionCodes)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.FileUploadOptionTable.Where(d => questionCodes.Contains(d.QuestionCode) && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }
    }
}