﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Seagull2.Questionnaire.WebApi.DataObjects;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    public class ConsequenceRepository : IConsequenceRepository
    {
        public Task<bool> Add(Consequence data)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                var count = db.ConsequenceTable.Where(d =>
                        d.QuestionaireCode == data.QuestionaireCode &&
                        d.Creator == data.Creator).Count();
                if (count <= 0)
                {
                    db.ConsequenceTable.Add(data);
                    return db.SaveChanges() > 0 ? true : false;
                }
                return false;
            });
        }
        /// <summary>
        /// 加载问卷
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <returns></returns>
        public Task<IEnumerable<Consequence>> Load(string serialNumber)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.ConsequenceTable.Where(d => d.SerialNumber == serialNumber && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }
        /// <summary>
        /// 加载问卷
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="creator">答卷人</param>
        /// <returns></returns>
        public Task<IEnumerable<Consequence>> Load(string serialNumber, string creator)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.ConsequenceTable.Where(d => d.SerialNumber == serialNumber && d.Creator == creator && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }
        /// <summary>
        /// 检查已答问卷的数量
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="creator"></param>
        /// <returns></returns>
        public Task<int> CheckConsequenceCount(string serialNumber, string creator)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.ConsequenceTable.Where(d => d.SerialNumber == serialNumber && d.Creator == creator && d.ValidStatus == true).Count();
            });
        }
        /// <summary>
        /// 检查已答问卷的数量
        /// </summary>
        /// <param name="questionaireCode"></param>
        /// <returns></returns>
        public Task<int> CheckConsequenceCount(string serialNumber)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.ConsequenceTable.Where(d => d.SerialNumber == serialNumber && d.ValidStatus == true).Count();
            });
        }
    }
}