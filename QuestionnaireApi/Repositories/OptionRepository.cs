﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using Seagull2.Questionnaire.WebApi.Repositories;
using Seagull2.Questionnaire.WebApi.Models.Extensions;
using Seagull2.Models;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    /// <summary>
    /// 选项的服务
    /// </summary>
    public class OptionRepository : IOptionRepository
    {
        public Task<bool> Add(IEnumerable<Option> data)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                db.ChoiceOptionTable.AddRange(data);
                return db.SaveChanges() > 0 ? true : false;
            });
        }
        /// <summary>
        /// 加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        public Task<IEnumerable<Option>> Load(string questionCode)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.ChoiceOptionTable.Where(d => d.QuestionCode == questionCode && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }
        public Task<IEnumerable<Option>> Load(List<string> questionCode)
        {
            return Task.Run(() =>
            {
                var db = new QuestionnaireDbContext();
                return db.ChoiceOptionTable.Where(d => questionCode.Contains(d.QuestionCode) && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();
            });
        }
    }
}