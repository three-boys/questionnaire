﻿using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    /// <summary>
    /// 答卷的结果接口
    /// </summary>
    public interface IConsequenceRepository
    {
        Task<bool> Add(Consequence data);

        Task<IEnumerable<Consequence>> Load(string serialNumber);
        Task<int> CheckConsequenceCount(string serialNumber, string creator);
        /// <summary>
        /// 检查已答问卷的数量
        /// </summary>
        /// <param name="questionaireCode"></param>
        /// <returns></returns>
        Task<int> CheckConsequenceCount(string serialNumber);
        /// <summary>
        /// 加载问卷
        /// </summary>
        /// <param name="serialNumber"></param>
        /// <param name="creator">答卷人</param>
        /// <returns></returns>
        Task<IEnumerable<Consequence>> Load(string serialNumber, string creator);
    }
}
