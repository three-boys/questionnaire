﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    /// <summary>
    /// 题目的服务接口
    /// </summary>
    public interface IQuestionRepository
    {
        /// <summary>
        /// 根据问卷Code加载题目
        /// </summary>
        /// <param name="questionnaireCode">问卷</param>
        /// <returns></returns>
        Task<IEnumerable<Question>> Load(string questionnaireCode);
        /// <summary>
        /// 添加题目
        /// </summary>
        /// <param name="data">题目内容</param> 
        Task<bool> Add(IEnumerable<Question> data);
    }
}
