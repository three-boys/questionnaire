﻿using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    public interface IFileUploadOptionResultRepository
    {
        /// <summary>
        /// 加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<IEnumerable<FileUploadOptionResult>> Load(string questionnaireNumber);
        /// <summary>
        /// 答案解析加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<IEnumerable<FileUploadOptionResult>> Load(string questionnaireNumber, string userId);
        Task<IEnumerable<FileUploadOptionResult>> LoadByConsequenceCode(string consequenceCode);
        Task<IEnumerable<FileUploadOptionResult>> LoadByConsequenceCode(string consequenceCode, string userId);
        /// <summary>
        /// 增加选项
        /// </summary>
        /// <param name="data">选项内容</param>
        /// <param name="questionCode">题目</param>
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns>
        Task<bool> Add(IEnumerable<FileUploadOptionResult> data);
    }
}
