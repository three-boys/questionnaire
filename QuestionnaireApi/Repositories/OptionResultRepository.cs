﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Models;
using System.Threading.Tasks;
using Seagull2.Questionnaire.WebApi.Repositories.Context;
using Seagull2.Questionnaire.WebApi.Models;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    public class OptionResultRepository : IOptionResultRepository
    {
        public Task<bool> Add(IEnumerable<OptionResult> data)
        {
            return Task.Run(() =>
                {
                    var db = new AnswerSheetDbContext();
                    db.ChoiceOption_ResultTable.AddRange(data);
                    return db.SaveChanges() > 0 ? true : false;
                });

        }
        /// <summary>
        /// 加载答题选项
        /// </summary>
        /// <param name="QuestionnaireNumber"></param>
        /// <returns></returns>
        public Task<IEnumerable<OptionResult>> Load(string questionnaireNumber)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.ChoiceOption_ResultTable.Where(d => d.QuestionnaireNumber == questionnaireNumber && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();

            });
        }
        public Task<PaginationCollection<OptionResultModel>> Load(PaginationInfo pagination, string code)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                var list = db.ChoiceOption_ResultTable.Where(d => d.QuestionCode == code && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList();
                var data = list.Skip(pagination.PageSize * (pagination.PageIndex - 1)).Take(pagination.PageSize)
                .Select(d => new OptionResultModel()
                {
                    Code = d.Code,
                    QuestionCode = d.QuestionCode,
                    Title = d.Title,
                    QuestionnaireNumber = d.QuestionnaireNumber,
                    ConsequenceCode = d.ConsequenceCode,
                    AnswerOption = d.AnswerOption,
                    Creator = d.Creator,
                    CreatorName = d.CreatorName,
                    CreateTime = d.CreateTime
                });
                var count = list.Count();
                return new PaginationCollection<OptionResultModel>(pagination)
                {
                    Data = data,
                    PaginationInfo = pagination,
                    TotalItems = count
                };
            });
        }
        public Task<IEnumerable<OptionResult>> Load(List<string> codes)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.ChoiceOption_ResultTable.Where(d => codes.Contains(d.Code) && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();

            });
        }

        public Task<IEnumerable<OptionResult>> Load(string questionnaireNumber, string userId)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.ChoiceOption_ResultTable.Where(d => d.QuestionnaireNumber == questionnaireNumber && d.ValidStatus == true && d.Creator == userId).OrderBy(d => d.SortNo).ToList().AsEnumerable();

            });
        }

        public Task<IEnumerable<OptionResult>> LoadByConsequenceCode(string consequenceCode)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.ChoiceOption_ResultTable.Where(d => d.ConsequenceCode == consequenceCode && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();

            });
        }
        /// <summary>
        /// 根据问题的Code加载选项的结果
        /// </summary>
        /// <param name="questionCode"></param>
        /// <returns></returns>
        public Task<IEnumerable<OptionResult>> LoadByQuestionCode(List<string> questionCodes)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.ChoiceOption_ResultTable.Where(d => questionCodes.Contains(d.QuestionCode) && d.ValidStatus == true).OrderBy(d => d.SortNo).ToList().AsEnumerable();

            });
        }

        public Task<IEnumerable<OptionResult>> LoadByConsequenceCode(string consequenceCode, string userId)
        {
            return Task.Run(() =>
            {
                var db = new AnswerSheetDbContext();
                return db.ChoiceOption_ResultTable.Where(d => d.ConsequenceCode == consequenceCode && d.ValidStatus == true && d.Creator == userId).OrderBy(d => d.SortNo).ToList().AsEnumerable();

            });
        }
    }
}