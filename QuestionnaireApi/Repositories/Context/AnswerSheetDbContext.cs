﻿using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Repositories.Context
{
    public class AnswerSheetDbContext : DbContext
    {
        public AnswerSheetDbContext() : base("AnswerSheet")
        {
        }
        /// <summary>
        /// 选择的选项结果
        /// </summary>
        public virtual DbSet<OptionResult> ChoiceOption_ResultTable { get; set; }
        /// <summary>
        /// 上传文件
        /// </summary>
        public virtual DbSet<FileUploadOptionResult> FileUploadOption_ResultTable { get; set; }

    }
}