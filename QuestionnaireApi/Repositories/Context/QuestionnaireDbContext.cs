﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Seagull2.Questionnaire.WebApi.DataObjects;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    public class QuestionnaireDbContext : DbContext
    {
        public QuestionnaireDbContext() : base("Questionnaire")
        {
        }
        /// <summary>
        /// 问卷表
        /// </summary>
        public virtual DbSet<Questionnaires> QuestionnairesTable { get; set; }
        /// <summary>
        ///题目表
        /// </summary>
        public virtual DbSet<Question> QuestionTable { get; set; }
        /// <summary>
        /// 选择题的选项表
        /// </summary>
        public virtual DbSet<Option> ChoiceOptionTable { get; set; }
        /// <summary>
        /// 结果表
        /// </summary>
        public virtual DbSet<Consequence> ConsequenceTable { get; set; }
        /// <summary>
        /// 上传文件
        /// </summary>
        public virtual DbSet<FileUploadOption> FileUploadOptionTable { get; set; }
    }
}