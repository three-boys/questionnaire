﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    /// <summary>
    /// 答题选项服务接口
    /// </summary>
    public interface IOptionResultRepository
    {
        /// <summary>
        /// 加载选项
        /// </summary>
        /// <param name="QuestionnaireNumber">问卷编号</param> 
        /// <returns></returns>
        Task<IEnumerable<OptionResult>> Load(string questionnaireNumber);
        Task<IEnumerable<OptionResult>> Load(List<string> codes);
        /// <summary>
        /// 答案解析加载选项
        /// </summary>
        /// <param name="QuestionnaireNumber">问卷编号</param> 
        /// <returns></returns>
        Task<IEnumerable<OptionResult>> Load(string questionnaireNumber, string userId);
        Task<IEnumerable<OptionResult>> LoadByConsequenceCode(string consequenceCode);
        Task<IEnumerable<OptionResult>> LoadByConsequenceCode(string consequenceCode, string userId);
        /// <summary>
        /// 加载选项分页
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<PaginationCollection<OptionResultModel>> Load(PaginationInfo pagination, string code);
        /// <summary>
        /// 增加选项
        /// </summary>
        /// <param name="data">选项内容</param>
        /// <param name="questionCode">题目</param>
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns>
        Task<bool> Add(IEnumerable<OptionResult> data);
        /// <summary>
        /// 根据问题的Code加载选项的结果
        /// </summary>
        /// <param name="questionCode"></param>
        /// <returns></returns>
        Task<IEnumerable<OptionResult>> LoadByQuestionCode(List<string> questionCodes);
    }
}