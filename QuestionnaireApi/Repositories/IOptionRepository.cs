﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Questionnaire.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seagull2.Questionnaire.WebApi.Repositories
{
    /// <summary>
    /// 选项的服务接口
    /// </summary>
    public interface IOptionRepository
    {
        /// <summary>
        /// 加载选项
        /// </summary>
        /// <param name="questionCode">题目</param> 
        /// <returns></returns>
        Task<IEnumerable<Option>> Load(string questionCode);
        Task<IEnumerable<Option>> Load(List<string> questionCode);
        /// <summary>
        /// 增加选项
        /// </summary>
        /// <param name="data">选项内容</param>
        /// <param name="questionCode">题目</param>
        /// <param name="currentUser">当前登录人</param>
        /// <returns></returns>
        Task<bool> Add(IEnumerable<Option> data);
    }
}
