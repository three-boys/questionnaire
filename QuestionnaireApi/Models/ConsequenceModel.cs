﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models
{
    public class ConsequenceModel
    {
        private string Code { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string SerialNumber { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        public int Versions { get; set; }
        /// <summary>
        /// 答卷的详细
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        public string CreatorName { get; set; }
        /// <summary>
        /// 创建人时间
        /// </summary>
        public DateTimeOffset CreateTime { get; set; }
        /// <summary>
        /// 答题所用时间
        /// </summary>
        public Int64 AnsweredTimes { get; set; }
        /// <summary>
        /// 总得分
        /// </summary>
        public double AggregateScore { get; set; }
        /// <summary>
        /// 答对题数
        /// </summary>
        public int CorrectQuestionCount { get; set; }
        public string Creator { get; set; }
    }
}