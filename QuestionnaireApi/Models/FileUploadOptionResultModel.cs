﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models
{
    public class FileUploadOptionResultModel
    {
        public string Code { get; set; }
        public string QuestionnaireNumber { get; set; }
        public string ConsequenceCode { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 题目的Code
        /// </summary>
        public string QuestionCode { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string ResourceId { get; set; }
        /// <summary>
        /// 登录人code
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 登录人
        /// </summary>
        public string CreatorName { get; set; }
    }
}