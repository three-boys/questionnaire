﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models
{
    /// <summary>
    /// 选项的基类
    /// </summary>
    public class OptionBaseModel
    {
        /// <summary>
        /// 题目的Code
        /// </summary>
        public string QuestionCode { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
    }
}