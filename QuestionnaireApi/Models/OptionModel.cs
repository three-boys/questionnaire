﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models
{
    /// <summary>
    /// 选项
    /// </summary>
    public class OptionModel : OptionModelBase
    {
        /// <summary>
        /// 是否是正确答案
        /// </summary>
        public bool IsCorrect { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string TheAnswer { get; set; }
    }
}