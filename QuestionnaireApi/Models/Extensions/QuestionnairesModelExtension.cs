﻿using Newtonsoft.Json;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.Extensions
{
    public static class QuestionnairesModelExtension
    {
        public static Questionnaires ToQuestionnaires(this QuestionnairesModel model)
        {
            if (model == null) return null;
            return new Questionnaires()
            {
                Code = model.Code,
                Title = model.Title,
                Summary = model.Summary,
                IsAllowAnonymous = model.IsAllowAnonymous,
                IsShowAnswer = model.IsShowAnswer,
                IsUserShare = model.IsUserShare,
                SerialNumber = model.SerialNumber,
                Type = model.Type.GetHashCode(),
                Status = model.Status,
                CreateTime = model.CreateTime,
                StartTime = model.StartTime,
                EndTime = model.EndTime,
                TimeLimit = model.TimeLimit,
                IsEndTime = model.IsEndTime,
                IsStartTime = model.IsStartTime
            };
        }
        public static QuestionnairesModel ToQuestionnairesModel(this Questionnaires model)
        {
            if (model == null) return null;
            return new QuestionnairesModel()
            {
                Code = model.Code,
                Title = model.Title,
                Summary = model.Summary,
                SerialNumber = model.SerialNumber,
                Status = model.Status,
                IsAllowAnonymous = model.IsAllowAnonymous,
                IsShowAnswer = model.IsShowAnswer,
                IsUserShare = model.IsUserShare,
                CreateTime = model.CreateTime,
                StartTime = model.StartTime,
                EndTime = model.EndTime,
                TimeLimit = model.TimeLimit,
                IsEndTime = model.IsEndTime,
                IsStartTime = model.IsStartTime
            };
        }
        /// <summary>
        /// Question字符串转换成IEnumerable<QuestionModel>
        /// </summary>
        /// <param name="model"></param>
        /// <param name="IsDesign">是否是设计问卷的数据</param>
        /// <returns></returns>
        public static IEnumerable<QuestionModel> DeserializeQuestions(Questionnaires model, bool IsDesign)
        {
            var questions = string.IsNullOrWhiteSpace(model.Questions) ? null : JsonConvert.DeserializeObject<List<QuestionModel>>(model.Questions);
            if (questions == null) return null;
            for (int i = 0; i < questions.Count(); i++)
            {
                if (questions[i].QuestionForm.ToLower() == QuestionFromEnmu.ChoiceQuestion.ToString().ToLower())
                {
                    if (IsDesign)
                    {
                        questions[i].Options = questions[i].Options == null ? null : JsonConvert.DeserializeObject<List<OptionModel>>(questions[i].Options.ToString());
                    }
                    else
                    {
                        questions[i].Options = questions[i].Options == null ? null : JsonConvert.DeserializeObject<List<OptionResultModel>>(questions[i].Options.ToString());
                    }
                }
                else if (questions[i].QuestionForm.ToLower() == QuestionFromEnmu.FileUpload.ToString().ToLower())
                {
                    questions[i].Options = questions[i].Options == null ? null : JsonConvert.DeserializeObject<List<FileUploadOptionModel>>(questions[i].Options.ToString());
                }
                else
                {
                    if (IsDesign)
                    {
                        questions[i].Options = questions[i].Options == null ? null : JsonConvert.DeserializeObject<List<OptionModel>>(questions[i].Options.ToString());
                    }
                    else
                    {
                        questions[i].Options = questions[i].Options == null ? null : JsonConvert.DeserializeObject<List<OptionResultModel>>(questions[i].Options.ToString());
                    }
                }
            }
            return questions.OrderBy(d => d.SortNo);
        }

        /// <summary>
        /// 问卷实体转换成ViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser">当前登录人</param>
        /// <param name="IsDesign">是否是设计问卷的数据</param>
        /// <returns></returns>
        public static QuestionnairesModel TransferQuestionnairesModel(Questionnaires model, Seagull2User currentUser, bool IsDesign)
        {
            if (model == null) return null;
            var data = new QuestionnairesModel()
            {
                Title = model.Title,
                Summary = model.Summary,
                SerialNumber = model.SerialNumber,
                IsAlterAuthority = (currentUser != null && model.Creator == currentUser.Id) ? true : false,
                Status = model.Status,
                Questions = QuestionnairesModelExtension.DeserializeQuestions(model, IsDesign),
                StartTime = model.StartTime,
                EndTime = model.EndTime,
                TimeLimit = model.TimeLimit,
                IsEndTime = model.IsEndTime,
                IsStartTime = model.IsStartTime,
                Code = model.Code,
                Type = (questionnairetype)model.Type,
                IsAllowAnonymous = model.IsAllowAnonymous,
                IsShowAnswer = model.IsShowAnswer,
                IsUserShare = model.IsUserShare,
                Versions = model.Versions
            };
            if (model.Questions != null)
            {
                var questions = JsonConvert.DeserializeObject<List<QuestionModel>>(model.Questions);
                data.VoteQuestionCodes = questions.Where(d => d.Category == QuestionCategoryEnmu.Vote.ToString().ToLower()).Select(d => d.Code).ToList();
            }
            return data;
        }
    }
}