﻿using Newtonsoft.Json;
using Seagull2.Owin.File.Models;
using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.Extensions
{
    public static class FileUploadOptionModelExtension
    {
        public static FileUploadOption ToFileUploadOption(this FileUploadOptionModel model, string questionCode)
        {
            if (model == null) return null;
            return new FileUploadOption()
            {
                QuestionCode = questionCode,
                Code = model.Code,
                Compress = model.Compress,
                ExtensionDetail = model.ExtensionDetail,
                File = model.File,
                FileMax = model.FileMax,
                Files = JsonConvert.SerializeObject(model.Files),
                Picture = model.Picture
            };
        }

        public static FileUploadOptionModel ToFileUploadOptionModel(this FileUploadOption model)
        {
            if (model == null) return null;
            return new FileUploadOptionModel()
            {
                QuestionCode = model.QuestionCode,
                Code = model.Code,
                Compress = model.Compress,
                ExtensionDetail = model.ExtensionDetail,
                File = model.File,
                FileMax = model.FileMax,
                Picture = model.Picture,
                Files = JsonConvert.DeserializeObject<List<ClientFileInformation>>(model.Files)
            };
        }
    }
}