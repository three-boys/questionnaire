﻿using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.Extensions
{
    public static class OptionModelExtension
    {
        public static Option ToChoiceOption(this OptionModel model, string questionCode)
        {
            if (model == null) return null;
            return new Option()
            {
                QuestionCode = questionCode,
                Title = model.Title,
                OptionJMPTo = model.OptionJMPTo,
                AlloweFillIn = model.AlloweFillIn,
                RequiredFillIn = model.RequiredFillIn,
                IsDefault = model.IsDefault,
                Reminder = model.Reminder,
                IsMutual = model.IsMutual,
                SortNo = model.SortNo,
                Score = model.Score,
                IsCorrect = model.IsCorrect,
                NoRecord = model.NoRecord,
                AlloweRepetition = model.AlloweRepetition,
                DefaultContent = model.DefaultContent,
                Height = model.Height,
                IsContainsAnswer = model.IsContainsAnswer,
                IsDefauleUnderLinestyle = model.IsDefauleUnderLinestyle,
                IsDefauleValue = model.IsDefauleValue,
                MaxNum = model.MaxNum,
                MaxNumber = model.MaxNumber,
                MinNum = model.MinNum,
                MinNumber = model.MinNumber,
                OptionProportionHeight = model.OptionProportionHeight,
                OptionProportionWidth = model.OptionProportionWidth,
                NotRepeat = model.NotRepeat,
                Required = model.Required,
                RightInputWidth = model.RightInputWidth,
                RightTitle = model.RightTitle,
                TheAnswer = model.TheAnswer,
                TitleWidth = model.TitleWidth,
                Underline = model.Underline,
                UnderlineStyle = model.UnderlineStyle,
                Validate = model.Validate,
                Width = model.Width
            };
        }
        public static IEnumerable<Option> ToChoiceOptionCollection(this IEnumerable<OptionModel> list, string questionCode)
        {
            if (list == null) return null;
            List<Option> data = new List<Option>();
            foreach (var item in list)
            {
                data.Add(item.ToChoiceOption(questionCode));
            }
            return data;
        }
        public static OptionModel ToChoiceOption(this Option model)
        {
            if (model == null) return null;
            return new OptionModel()
            {
                Title = model.Title,
                OptionJMPTo = model.OptionJMPTo,
                AlloweFillIn = model.AlloweFillIn,
                RequiredFillIn = model.RequiredFillIn,
                IsDefault = model.IsDefault,
                Reminder = model.Reminder,
                IsMutual = model.IsMutual,
                SortNo = model.SortNo,
                Score = model.Score,
                IsCorrect = model.IsCorrect,
                NoRecord = model.NoRecord,
                QuestionCode = model.QuestionCode,

                AlloweRepetition = model.AlloweRepetition,
                DefaultContent = model.DefaultContent,
                Height = model.Height,
                IsContainsAnswer = model.IsContainsAnswer,
                IsDefauleUnderLinestyle = model.IsDefauleUnderLinestyle,
                IsDefauleValue = model.IsDefauleValue,
                MaxNum = model.MaxNum,
                MaxNumber = model.MaxNumber,
                MinNum = model.MinNum,
                MinNumber = model.MinNumber,
                OptionProportionHeight = model.OptionProportionHeight,
                OptionProportionWidth = model.OptionProportionWidth,
                NotRepeat = model.NotRepeat,
                Required = model.Required,
                RightInputWidth = model.RightInputWidth,
                RightTitle = model.RightTitle,
                TheAnswer = model.TheAnswer,
                TitleWidth = model.TitleWidth,
                Underline = model.Underline,
                UnderlineStyle = model.UnderlineStyle,
                Validate = model.Validate,
                Width = model.Width
            };
        }
    }
}