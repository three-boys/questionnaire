﻿using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.Extensions
{
    public static class ConsequenceModelExtension
    {
        public static IEnumerable<ConsequenceModel> ToConsequenceModelCollection(this IEnumerable<Consequence> list)
        {
            if (list == null) return null;
            List<ConsequenceModel> data = new List<ConsequenceModel>();
            foreach (var item in list)
            {
                data.Add(item.ToConsequenceModel());
            }
            return data;
        }
        public static ConsequenceModel ToConsequenceModel(this Consequence model)
        {
            if (model == null) return null;
            return new ConsequenceModel()
            {
                SerialNumber = model.SerialNumber,
                Versions = model.Versions,
                CreatorName = model.CreatorName,
                CreateTime = model.CreateTime,
                AggregateScore = model.AggregateScore,
                CorrectQuestionCount = model.CorrectQuestionCount,
                AnsweredTimes = model.AnsweredTimes,
                Creator = model.Creator
            };
        }
    }
}