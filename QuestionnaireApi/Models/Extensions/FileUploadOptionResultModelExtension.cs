﻿using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;
using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.Extensions
{
    public static class FileUploadOptionResultModelExtension
    {
        public static FileUploadOptionResult ToFileUploadOptionResult(this FileUploadOptionResultModel model, string consequenceCode, Seagull2User currentUser)
        {
            if (model == null) return null;
            return new FileUploadOptionResult()
            {
                QuestionCode = model.QuestionCode,
                Code = Guid.NewGuid().ToString(),
                QuestionnaireNumber = model.QuestionnaireNumber,
                FileName = model.FileName,
                ConsequenceCode = consequenceCode,
                ResourceId = model.ResourceId,
                CreateTime = DateTime.Now,
                Creator = currentUser.Id,
                CreatorName = currentUser.DisplayName,
                ValidStatus = true
            };
        }
        public static IEnumerable<FileUploadOptionResult> ToFileUploadOptionCollectionResult(this IEnumerable<FileUploadOptionResultModel> list, string questionnaireNumber, string consequenceCode, Seagull2User currentUser)
        {
            if (list == null) return null;
            List<FileUploadOptionResult> data = new List<FileUploadOptionResult>();
            foreach (var item in list)
            {
                item.QuestionnaireNumber = questionnaireNumber;
                data.Add(item.ToFileUploadOptionResult(consequenceCode, currentUser));
            }
            return data;
        }
        public static FileUploadOptionResultModel ToFileUploadOptionModelResult(this FileUploadOptionResult model)
        {
            if (model == null) return null;
            return new FileUploadOptionResultModel()
            {
                Code = model.Code,
                QuestionCode = model.QuestionCode,
                FileName = model.FileName,
                QuestionnaireNumber = model.QuestionnaireNumber,
                ConsequenceCode = model.ConsequenceCode,
                ResourceId = model.ResourceId,
                CreatorName = model.CreatorName,
                Creator = model.Creator
            };
        }
    }
}