﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Seagull2.Questionnaire.WebApi.DataObjects;
using Seagull2.Models;
using Seagull2.Questionnaire.WebApi.Common;

namespace Seagull2.Questionnaire.WebApi.Models.Extensions
{
    public static class OptionResultModelExtension
    {
        public static OptionResult ToChoiceOptionResult(this OptionResultModel model, string consequenceCode, Seagull2User currentUser)
        {
            if (model == null) return null;
            return new OptionResult()
            {
                QuestionCode = model.QuestionCode,
                QuestionnaireNumber = model.QuestionnaireNumber,
                ConsequenceCode = consequenceCode,
                Title = model.Title,
                Code = Guid.NewGuid().ToString(),
                AnswerOption = model.AnswerOption,
                OptionGapFilling = model.OptionGapFilling,
                SortNo = model.SortNo,
                CreateTime = DateTime.Now,
                Creator = currentUser.Id,
                CreatorName = currentUser.DisplayName,
                ValidStatus = true
            };
        }

        public static IEnumerable<OptionResult> ToChoiceOptionCollectionResult(this IEnumerable<OptionResultModel> list, string questionnaireNumber, string consequenceCode, Seagull2User currentUser)
        {
            if (list == null) return null;
            List<OptionResult> data = new List<OptionResult>();
            foreach (var item in list)
            {
                item.QuestionnaireNumber = questionnaireNumber;
                data.Add(item.ToChoiceOptionResult(consequenceCode, currentUser));
            }
            return data;
        }

        public static OptionResultModel ToChoiceOptionModelResult(this OptionResult model)
        {
            if (model == null) return null;
            return new OptionResultModel()
            {
                Title = model.Title,
                Code = model.Code,
                QuestionCode = model.QuestionCode,
                QuestionnaireNumber = model.QuestionnaireNumber,
                ConsequenceCode = model.ConsequenceCode,
                Creator = model.Creator,
                AnswerOption = model.AnswerOption,
                OptionGapFilling = model.OptionGapFilling
            };
        }
    }
}