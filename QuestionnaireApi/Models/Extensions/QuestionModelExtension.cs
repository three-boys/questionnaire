﻿using Newtonsoft.Json;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.Extensions
{
    public static class QuestionModelExtension
    {
        /// <summary>
        /// 扩展
        /// </summary>
        /// <param name="model"></param>
        /// <param name="questionnaireCode">问卷Code</param>
        /// <returns></returns>
        public static Question ToQuestion(this QuestionModel model, string questionnaireCode)
        {
            if (model == null) return null;
            return new Question()
            {
                Title = model.Title,
                Required = model.Required,
                UnconditionalJMP = model.UnconditionalJMP,
                UnconditionalJMPTo = model.UnconditionalJMPTo,
                RelevanceQuestion = JsonConvert.SerializeObject(model.RelevanceQuestion),
                LogicOption = model.LogicOption,
                WhetherSelect = model.WhetherSelect,
                Related = model.Related,
                HasReminder = model.HasReminder,
                Reminder = model.Reminder,
                OptionJMP = model.OptionJMP,
                PageIndex = model.PageIndex,
                Percentage = model.Percentage,
                PollNumber = model.PollNumber,
                StandingMinTime = model.StandingMinTime,
                StandingMaxTime = model.StandingMaxTime, 
                TitleWidth = model.TitleWidth,
                TitleAllWidth = model.TitleAllWidth,
                Category = model.Category,
                Selectivity = model.Selectivity, 
                QuestionType = model.QuestionType,
                OptionProportion = model.OptionProportion,
                QuestionnaireCode = questionnaireCode,
                NO = model.NO,
                SortNo = model.SortNo,
                Score = model.Score,
                AtLeast = model.AtLeast,
                AtMost = model.AtMost,
                MinValue = model.MinValue,
                MinValueShow = model.MinValueShow,
                MaxValue = model.MaxValue,
                MaxValueShow = model.MaxValueShow,
                QuestionForm = model.QuestionForm,
                LikertPattern = model.LikertPattern,
                RightTitleWidth = model.RightTitleWidth
            };
        }
    }
}