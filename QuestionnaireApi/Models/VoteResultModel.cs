﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models
{
    public class VoteResultModel
    {
        public string QuestionCode { get; set; }
        public string OptionTitle { get; set; }
        /// <summary>
        /// 投票的票数
        /// </summary>
        public int PollNumber { get; set; }
        /// <summary>
        /// 投票的百分比
        /// </summary>
        public double Percentage { get; set; }
    }
}