﻿using Seagull2.Questionnaire.WebApi.Common.Attributes;
using Seagull2.Questionnaire.WebApi.Common.Enums;
using Seagull2.Questionnaire.WebApi.Common.Extensions;
using Seagull2.Questionnaire.WebApi.DataObjects;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Seagull2.Questionnaire.WebApi.Models
{
    /// <summary>
    /// 问卷Model
    /// </summary>
    public class QuestionnairesModel
    {
        public string Code { get; set; }
        /// <summary>
        /// 是否加载成功
        /// </summary>
        public QuestionnaireLoadStatus LoadState { get; set; }
        /// <summary>
        /// 错误消息
        /// </summary>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 描述
        /// </summary> 
        public string Summary { get; set; }
        /// <summary>
        /// 投票题的Code集合（用于前台取投票数）
        /// </summary>
        public List<string> VoteQuestionCodes { get; set; }
        /// <summary>
        ///类别(问卷，考试，投票)
        /// </summary>
        public questionnairetype Type { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        public int Versions { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string SerialNumber { get; set; }
        /// <summary>
        /// 是否有修改权限
        /// </summary>
        public bool IsAlterAuthority { get; set; }
        /// <summary>
        /// 状态(草稿/发布/完成)
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 问卷开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 问卷结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 限时（秒）
        /// </summary>
        public Int64? TimeLimit { get; set; }
        /// <summary>
        /// 是否设置结束时间
        /// </summary>
        public bool IsEndTime { get; set; }
        /// <summary>
        /// 是否设置开始时间
        /// </summary>
        public bool IsStartTime { get; set; }
        /// <summary>
        /// 答题已使用时间
        /// </summary>
        public Int64 AnsweredTimes { get; set; }
        /// <summary>
        /// 开始答题小时的设置
        /// </summary>
        public double? StartHour { get; set; }
        /// <summary>
        /// 开始答题分钟的设置
        /// </summary>
        public double? StartMinute { get; set; }
        /// <summary>
        /// 结束答题小时的设置
        /// </summary>
        public double? EndHour { get; set; }
        /// <summary>
        /// 结束答题小时的设置
        /// </summary>
        public double? EndMinute { get; set; }
        /// <summary>
        /// 已答人数
        /// </summary>
        public int AnsweredCount { get; set; }
        /// <summary>
        /// 创建人时间
        /// </summary>
        public DateTimeOffset CreateTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        public string CreatorName { get; set; }
        /// <summary>
        /// 是否匿名登录
        /// </summary>
        public bool IsAllowAnonymous { get; set; }
        /// <summary>
        /// 是否统一发放
        /// </summary>
        public bool IsShowAnswer { get; set; }
        /// <summary>
        /// 用户是否可分享
        /// </summary>
        public bool IsUserShare { get; set; }
        /// <summary>
        /// 题目
        /// </summary>
        public IEnumerable<QuestionModel> Questions { get; set; }
    }
}