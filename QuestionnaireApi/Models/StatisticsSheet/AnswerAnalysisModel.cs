﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.StatisticsSheet
{
    /// <summary>
    /// 答案解析实体类
    /// </summary>
    public class AnswerAnalysisModel
    {
        public string Code { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public int? No { get; set; }
        /// <summary>
        ///标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题型
        /// </summary>
        public string QuestionType { get; set; }
        /// <summary>
        /// 分值
        /// </summary>
        public double Score { get; set; }
        /// <summary>
        /// 您的回答
        /// </summary>
        public string YourAnswer { get; set; }
        /// <summary>
        /// 正确答案
        /// </summary>
        public string RightKey { get; set; }
        /// <summary>
        /// 是否正确
        /// </summary>
        public bool IsRight { get; set; }
    }
}