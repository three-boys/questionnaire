﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.StatisticsSheet
{
    public class OptionStatisticsModel : OptionBaseModel
    {
        /// <summary>
        /// 选择此选项的数量
        /// </summary>
        public int AnswerAmount { get; set; }
        /// <summary>
        /// 答题的比例
        /// </summary>
        public double AnswerRate { get; set; }
    }
}