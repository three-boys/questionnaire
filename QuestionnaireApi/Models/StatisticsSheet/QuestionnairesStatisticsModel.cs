﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.StatisticsSheet
{
    /// <summary>
    /// 问卷统计Model
    /// </summary>
    public class QuestionnairesStatisticsModel
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string SerialNumber { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 已答问卷数
        /// </summary>
        public int AnswerAmount { get; set; }
        /// <summary>
        /// 考试题数
        /// </summary>
        public int ExamAmount { get; set; }
        /// <summary>
        /// 投票题数
        /// </summary>
        public int VoteAmount { get; set; }
        /// <summary>
        /// 总题数
        /// </summary>
        public int AllQuestionAmount { get; set; }
    }
}