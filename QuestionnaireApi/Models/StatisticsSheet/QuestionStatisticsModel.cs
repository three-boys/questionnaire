﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models.StatisticsSheet
{
    /// <summary>
    /// 统计实体类
    /// </summary>
    public class QuestionStatisticsModel : QuestionBaseModel
    {
        public List<OptionStatisticsModel> Options { get; set; }
        public int CorrectNumber { get; set; }
    }
}