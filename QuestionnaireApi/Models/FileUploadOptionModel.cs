﻿using Seagull2.Owin.File.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models
{
    /// <summary>
    /// 上传文件
    /// </summary>
    public class FileUploadOptionModel
    {
        public string Code { get; set; }
        /// <summary>
        /// 题目的Code
        /// </summary>
        public string QuestionCode { get; set; }
        /// <summary>
        /// 是否上传图片
        /// </summary>
        public bool Picture { get; set; }
        /// <summary>
        /// 是否上传文件
        /// </summary>
        public bool File { get; set; }
        //内容
        public List<ClientFileInformation> Files { get; set; }
        /// <summary>
        /// 是否上传压缩文件
        /// </summary>
        public bool Compress { get; set; }
        /// <summary>
        /// 图片扩展
        /// </summary>
        public string ExtensionDetail { get; set; }
        /// <summary>
        /// 最大文件
        /// </summary>
        public Int64? FileMax { get; set; }
    }
}