﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models
{
    /// <summary>
    /// 答案=>选项
    /// </summary>
    public class OptionResultModel : OptionModelBase
    {
        public string Code { get; set; }
        public string QuestionnaireNumber { get; set; }
        public string ConsequenceCode { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string AnswerOption { get; set; }
        /// <summary>
        /// 登录人code
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        ///选项可填空的答案
        /// </summary>
        public string OptionGapFilling { get; set; }
        /// <summary>
        /// 登录人
        /// </summary>
        public string CreatorName { get; set; }
        public DateTimeOffset CreateTime { get; set; }
    }
}