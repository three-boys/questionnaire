﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seagull2.Questionnaire.WebApi.Models
{
    /// <summary>
    /// 问题 的基类
    /// </summary>
    public class QuestionBaseModel
    {
        public string Code { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int? NO { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public string QuestionType { get; set; }
    }
}