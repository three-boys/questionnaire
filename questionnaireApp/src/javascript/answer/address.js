(function (Seagull2Answer) {
    var Address = (function (_super) {
        Seagull2Answer.__extends(Address, _super);
        function Address(_super) {
            this.templateUrl = './views/exam-preview/questions/address.html';
        }
        return Address;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.Address = Address;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));