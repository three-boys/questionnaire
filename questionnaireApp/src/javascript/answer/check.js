(function (Seagull2Answer) {

    var CheckOption = (function (_super) {
        Seagull2Answer.__extends(CheckOption, _super);
        function CheckOption() {
            _super.call(this);

        }
        return CheckOption;
    } (Seagull2Answer.RadioOption));

    var Check = (function (_super) {
        Seagull2Answer.__extends(Check, _super);
        function Check() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/check.html';
            this.optionClass = CheckOption;
            this.abc = 123;
        }

        Check.prototype.setDefault = function (option, val) {
            option.isDefault = val;
        };

        // Check.prototype.setOptionSize = function () {
        //     if (this.atMost === '') {
        //         return true;
        //     } else if (this.atMost < this.atLeast) {
        //         this.atLeast = this.atMost;
        //         this.sizeHint = true;
        //     }
        // };

        //选项互斥 
        Check.prototype.mutualReject = function (isMutual, answerOption) {
            if (isMutual === true) {
                for (var i = 0; i < this.options.length; i++) {
                    if (this.options[i].isMutual === false) {
                        this.options[i].answerOption = false;
                    }
                }
            } else if (isMutual === false) {
                for (var i = 0; i < this.options.length; i++) {
                    if (this.options[i].isMutual === true) {
                        this.options[i].answerOption = false;
                    }
                }
            }
        };

        //至多至少                              
        Check.prototype.rangeNumber = function (answerOption, questions, option) { 
            if (this.atLeast && this.atMost) {
                var numbers = 0;
                for (var i = 0; i < this.options.length; i++) {

                    if (this.options[i].answerOption === true) {
                        numbers = numbers + 1;
                    }
                }
                if (numbers < this.atLeast) {
                    this.leastRangeMessage = true;
                    this.outRangeMessage = false;
                } else if (numbers > this.atMost) {
                    this.outRangeMessage = true;
                    this.leastRangeMessage = false;
                } else {
                    this.outRangeMessage = false;
                    this.leastRangeMessage = false;
                }
            }
            //无条件跳题
            checkUnconditionalSkip(answerOption, questions, this);
            //关联逻辑 
            getRelatedOption(questions, this, option, answerOption);
        };

        //无条件跳题
        function checkUnconditionalSkip(answerOption, questions, question) {
            if (question.unconditionalJMP === true && question.unconditionalJMPTo > question.no && question.unconditionalJMPTo < questions.length) {
                for (var i = question.sortNo + 1; i < question.unconditionalJMPTo; i++) {
                    if (answerOption === true) {
                        questions[i].unconditionalSkipClass = true;
                    } else {
                        var noHide = true;
                        for (var l = 0; l < question.options.length; l++) {
                            if (question.options[l].answerOption === true) {
                                noHide = false;
                            }
                        }
                        if (noHide) {
                            questions[i].unconditionalSkipClass = false;
                        }
                    }
                }
            }
        };

        //关联逻辑 
        function getRelatedOption(questions, currentQuestion, option, answerOption) {
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].related) {
                    if (questions[i].relevanceQuestion.no == currentQuestion.no) {
                        for (var j = 0; j < questions[i].relevanceQuestion.options.length; j++) {
                            if (questions[i].relevanceQuestion.options[j].logicOption) {
                                var noShow = true;
                                for (var l = 0; l < currentQuestion.options.length; l++) {
                                    if(currentQuestion.options[l].answerOption && currentQuestion.options[l].title === questions[i].relevanceQuestion.options[j].title){
                                        noShow = false;
                                    }
                                }
                                if (answerOption && option.title === questions[i].relevanceQuestion.options[j].title) {
                                    questions[i].isRelevanceQuestion = false;
                                }
                                else if(noShow){
                                    questions[i].isRelevanceQuestion = true;
                                }
                            }
                        }
                    }
                }
            }
        };



        return Check;
    } (Seagull2Answer.Radio));

    Seagull2Answer.CheckOption = CheckOption;
    Seagull2Answer.Check = Check;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));