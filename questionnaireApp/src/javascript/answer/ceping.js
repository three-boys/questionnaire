(function (Seagull2Answer) {

    var CepingOption = (function (_super) {
        Seagull2Answer.__extends(CepingOption, _super); 
        function CepingOption() {
            _super.call(this); 
            
        } 
        return CepingOption;
    } (Seagull2Answer.RadioOption));

    var Ceping = (function (_super) {
        Seagull2Answer.__extends(Ceping, _super);
        function Ceping() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/ceping.html';
            this.optionClass = CepingOption;
        }



        return Ceping;
    } (Seagull2Answer.Radio));

    Seagull2Answer.CepingOption = CepingOption;
    Seagull2Answer.Ceping = Ceping;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));