(function (Seagull2Answer) {

    var SortOption = (function (_super) {
        Seagull2Answer.__extends(SortOption, _super);
        function SortOption() {
            _super.call(this);
            // this.isMutual = false;
            // this.sortOperation = false;
        }
        return SortOption;
    } (Seagull2Answer.RadioDownOption));

    var Sort = (function (_super) {
        Seagull2Answer.__extends(Sort, _super);
        function Sort() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/sort.html';
        }

        Sort.prototype.moveUp = function (option) {
            var index = this.options.indexOf(option);
            if (index >= 1) {
                var option = this.options[index];
                this.options[index] = this.options[index - 1];
                this.options[index - 1] = option;
                return true;
            }
            return false;
        };

        Sort.prototype.moveDown = function (option) {
            var index = this.options.indexOf(option);
            if (index >= 0 && index !== (this.options.length - 1)) {
                var option = this.options[index];
                this.options[index] = this.options[index + 1];
                this.options[index + 1] = option;
                return true;
            }
            return false;
        };


        return Sort;
    } (Seagull2Answer.RadioDown));

    Seagull2Answer.SortOption = SortOption;
    Seagull2Answer.Sort = Sort;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));