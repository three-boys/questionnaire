(function (Seagull2Answer) {
    var Map = (function (_super) {
        Seagull2Answer.__extends(Map, _super);
        function Map(_super) {
            this.templateUrl = './views/exam-preview/questions/map.html';
        }
        return Map;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.Map = Map;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));