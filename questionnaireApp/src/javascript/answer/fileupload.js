(function (Seagull2Answer) {
    var FileuploadOption = (function (_super) {
        Seagull2Answer.__extends(FileuploadOption, _super);
        function FileuploadOption() {
            _super.call(this);

        }
        return FileuploadOption;
    } (Seagull2Answer.RadioDownOption));

    var Fileupload = (function (_super) {
        Seagull2Answer.__extends(Fileupload, _super);
        function Fileupload() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/fileupload.html';
            this.optionClass = FileuploadOption; 
        }

        return Fileupload;
    } (Seagull2Answer.RadioDown));

    Seagull2Answer.FileuploadOption = FileuploadOption;
    Seagull2Answer.Fileupload = Fileupload;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));

