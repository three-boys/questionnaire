(function (Seagull2Answer) {

    var questionConstructor = {};

    questionConstructor.likert = function () {
        var result = new Seagull2Answer.Likert();
        return result;
    };

    questionConstructor.radio = function () {
        var result = new Seagull2Answer.Radio();
        return result;
    };

    questionConstructor.check = function () {
        var result = new Seagull2Answer.Check();


        return result;
    };

    questionConstructor.question = function () {
        var result = new Seagull2Answer.Question();

        return result;
    };

    questionConstructor.gapfill = function () {
        var result = new Seagull2Answer.Gapfill();
        return result;
    };

    questionConstructor.matrix = function () {
        var result = new Seagull2Answer.Matrix();


        return result;
    };

    questionConstructor.matrixScale = function () {
        var result = new Seagull2Answer.MatrixScale();
        return result;
    };

    questionConstructor.matrixMultipleChoice = function () {
        var result = new Seagull2Answer.MatrixMultipleChoice();
        return result;
    };

    questionConstructor.matrixMultipleChoices = function () {
        var result = new Seagull2Answer.MatrixMultipleChoices();
        return result;
    };

    questionConstructor.matrixSlider = function () {
        var result = new Seagull2Answer.MatrixSlider();
        return result;
    };

    questionConstructor.matrixComboBox = function () {
        var result = new Seagull2Answer.MatrixComboBox();
        return result;
    };

    questionConstructor.matrixNumerical = function () {
        var result = new Seagull2Answer.MatrixNumerical();
        return result;
    };

    questionConstructor.matrixText = function () {
        var result = new Seagull2Answer.MatrixText();
        return result;
    };

    questionConstructor.fileupload = function () {
        var result = new Seagull2Answer.Fileupload();
        return result;
    };



    questionConstructor.ceping = function () {
        var result = new Seagull2Answer.Ceping();


        return result;
    };

    questionConstructor.cepings = function () {
        var result = new Seagull2Answer.Cepings();


        return result;
    };

    questionConstructor.ceshi = function () {
        var result = new Seagull2Answer.Ceshi();


        return result;
    };

    questionConstructor.ceshiJudge = function () {
        var result = new Seagull2Answer.Ceshi();
        return result;
    };

    questionConstructor.ceshiMultipleChoices = function () {
        var result = new Seagull2Answer.CeshiMultipleChoices();
        return result;
    };

    questionConstructor.ceshiGapfill = function () {
        var result = new Seagull2Answer.CeshiGapfill();

        return result;
    };

    questionConstructor.ceshiQuestion = function () {
        var result = new Seagull2Answer.CeshiQuestion();

        return result;
    };

    questionConstructor.checkPermission = function () {
        var result = new Seagull2Answer.CheckPermission();

        return result;
    };

    questionConstructor.name = function () {
        var result = new Seagull2Answer.Name();

        return result;
    };

    questionConstructor.checkBasicMessage = function () {
        var result = new Seagull2Answer.CheckBasicMessage();


        return result;
    };

    questionConstructor.voteRadio = function () {
        var result = new Seagull2Answer.VoteRadio();


        return result;
    };

    questionConstructor.voteCheck = function () {
        var result = new Seagull2Answer.VoteCheck();


        return result;
    };

    questionConstructor.page = function () {
        var result = new Seagull2Answer.Page();
        return result;
    };

    questionConstructor.cut = function () {
        var result = new Seagull2Answer.Cut();
        return result;
    };

    questionConstructor.sort = function () {
        var result = new Seagull2Answer.Sort();


        return result;
    };

    questionConstructor.sum = function () {
        var result = new Seagull2Answer.Sum();
        return result;
    };

    questionConstructor.slider = function () {
        var result = new Seagull2Answer.Slider();
        return result;
    };

    questionConstructor.qingjing = function () {
        var result = new Seagull2Answer.Qingjing();
        return result;
    };

    questionConstructor.shop = function () {
        var result = new Seagull2Answer.Shop();
        return result;
    };

    questionConstructor.radioDown = function () {
        var result = new Seagull2Answer.RadioDown();


        return result;
    };

    questionConstructor.classRadioDown = function () {
        var result = new Seagull2Answer.ClassRadioDown();
        return result;
    };

    questionConstructor.gender = function () {
        var result = new Seagull2Answer.Gender();
        return result;
    };

    questionConstructor.age = function () {
        var result = new Seagull2Answer.Age();
        return result;
    };

    questionConstructor.province = function () {
        var result = new Seagull2Answer.Province();
        return result;
    };

    questionConstructor.city = function () {
        var result = new Seagull2Answer.City();
        return result;
    };

    questionConstructor.provinceCity = function () {
        var result = new Seagull2Answer.ProvinceCity();
        return result;
    };

    questionConstructor.phone = function () {
        var result = new Seagull2Answer.Phone();
        return result;
    };

    questionConstructor.phoneCheckout = function () {
        var result = new Seagull2Answer.PhoneCheckout();
        return result;
    };

    questionConstructor.email = function () {
        var result = new Seagull2Answer.Email();

        return result;
    };

    questionConstructor.date = function () {
        var result = new Seagull2Answer.Date();
        return result;
    };

    questionConstructor.map = function () {
        var result = new Seagull2Answer.Map();
        return result;
    };

    questionConstructor.profession = function () {
        var result = new Seagull2Answer.Profession();
        return result;
    };

    questionConstructor.industry = function () {
        var result = new Seagull2Answer.Industry();
        return result;
    };

    questionConstructor.school = function () {
        var result = new Seagull2Answer.School();
        return result;
    };

    questionConstructor.address = function () {
        var result = new Seagull2Answer.Address();
        return result;
    };

    var AnswerPaper = (function () {
        function AnswerPaper() {
            this.title = '';
            this.summary = '';
            this.questions = [];
        }

        AnswerPaper.prototype.addQuestion = function (questionType) {
            var constructor = questionConstructor[questionType];
            if (constructor) {
                var item = constructor();
                this.questions.push(item);
            };
            // if (this.related === true){

            // }
        };

        // AnswerPaper.prototype.skipQuestion = function (unconditionalJMPTo,$index) {
        //     if (unconditionalJMPTo > 1) {
        //         for(var i=0; i < qustions.length; i++){
        //             if(i > $index && i < unconditionalJMPTo)
        //             this.questions.splice($index+1, unconditionalJMPTo - $index);
        //         }
        //     };
        // };
        // question|questionNoFilter:viewAnswer.questions

        // AnswerPaper.prototype.unconditionalSkip = function (questions) {
        //     if (this.unconditionalJMPTo > 1){
        //         for (var i = 0; i < questions.length; i++) {                    
        //             this.questions.splice($index, this.unconditionalJMPTo - $index);
        //         }
        //     } 
        // };









        return AnswerPaper;
    } ());
    Seagull2Answer.AnswerPaper = AnswerPaper;
})(Seagull2Answer || (Seagull2Answer = {}));