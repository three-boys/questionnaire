(function (Seagull2Answer) {
    var QuestionBase = (function () {
        function QuestionBase() {
            this.logicOption = false;             
        } 
 
        return QuestionBase;
    } ());
    Seagull2Answer.QuestionBase = QuestionBase;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));