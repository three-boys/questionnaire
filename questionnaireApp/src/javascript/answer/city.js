(function (Seagull2Answer) {
    var City = (function (_super) {
        Seagull2Answer.__extends(City, _super);
        function City(_super) {
            this.templateUrl = './views/exam-preview/questions/city.html';
        }
        return City;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.City = City;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));