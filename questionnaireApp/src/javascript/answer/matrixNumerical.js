(function (Seagull2Answer) {
    var MatrixNumerical = (function (_super) {
        Seagull2Answer.__extends(MatrixNumerical, _super);
        function MatrixNumerical(_super) {
            this.templateUrl = './views/exam-preview/questions/matrixNumerical.html';
        }
        return MatrixNumerical;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.MatrixNumerical = MatrixNumerical;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));