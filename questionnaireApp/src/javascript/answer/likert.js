(function (Seagull2Answer) {

    var LikertOption = (function (_super) {
        Seagull2Answer.__extends(LikertOption, _super);
        function LikertOption() {
            _super.call(this);
        }
        return LikertOption;
    } (Seagull2Answer.RadioOption));

    var Likert = (function (_super) {
        Seagull2Answer.__extends(Likert, _super);
        function Likert() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/likert.html';
            this.aba = '你好';
        }

        Likert.prototype.gradualChangeStar = function ($index) {
            for (var i = 0; i <= $index; i++) {
                this.options[i]['class'] = 'gradual-change-star';
            }
            for (var i = $index + 1; i < this.options.length; i++) {
                this.options[i]['class'] = '';
            }
        };

        Likert.prototype.gradualChange = function ($index) {
            for (var i = 0; i <= $index; i++) {
                this.options[i]['class'] = 'gradual-change';
            }
            for (var i = $index + 1; i < this.options.length; i++) {
                this.options[i]['class'] = '';
            }
        };

        Likert.prototype.gradualChangeNo = function ($index, answerOption,questions,optionJMPTo,option) {
            for (var i = 0; i <= $index; i++) {
                this.options[i].answerOption = true;
            }
            for (var i = $index + 1; i < this.options.length; i++) {
                this.options[i].answerOption = false;
            }
            //量表无条件跳题
            dropDownSkip(questions, optionJMPTo, option,this);
            //关联逻辑
            getRelatedOption(questions, this, option);
        };

        Likert.prototype.answerOption = false;

        //量表无条件跳题
        function dropDownSkip(questions, optionJMPTo, option,qustion) {
            if (qustion.unconditionalJMP === true && qustion.unconditionalJMPTo > qustion.no && qustion.unconditionalJMPTo < questions.length) {
                for (var i = qustion.sortNo + 1; i < qustion.unconditionalJMPTo; i++) {
                    questions[i].unconditionalSkipClass = true;
                }
            }
            if (qustion.optionJMP === true && option.optionJMPTo > qustion.no && option.optionJMPTo < questions.length) {
                for (var i = qustion.sortNo + 1; i < option.optionJMPTo; i++) {
                    if (questions[i].questionType !== "分页" && questions[i].questionType !== "段落说明") {
                        questions[i].unconditionalSkipClass = true;
                    }
                }
            } else if (qustion.unconditionalJMP !== true) {
                for (var i = 0; i < questions.length; i++) {
                    questions[i].unconditionalSkipClass = false;
                }
            }
        };

        function getRelatedOption(questions, currentQuestion, option) {
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].related) {
                    if (questions[i].relevanceQuestion.no == currentQuestion.no) {
                        for (var j = 0; j < questions[i].relevanceQuestion.options.length; j++) {
                            if (questions[i].relevanceQuestion.options[j].logicOption) {
                                if (option.answerOption && option.title === questions[i].relevanceQuestion.options[j].title) {
                                    questions[i].isRelevanceQuestion = false;
                                }
                                else {
                                    questions[i].isRelevanceQuestion = true;
                                }
                            }
                        }
                    }
                }
            }
        };


        return Likert;
    } (Seagull2Answer.Radio));

    Seagull2Answer.LikertOption = LikertOption;
    Seagull2Answer.Likert = Likert;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));