(function (Seagull2Answer) {
    var Cut = (function (_super) {
        Seagull2Answer.__extends(Cut, _super);
        function Cut() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/cut.html';
        }
        return Cut; 
    } (Seagull2Answer.QuestionBase));
    
    Seagull2Answer.Cut = Cut;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));


