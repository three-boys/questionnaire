(function (Seagull2Answer) { 

    var AgeOption = (function (_super) {
        Seagull2Answer.__extends(AgeOption, _super);
        function AgeOption() {
            _super.call(this);            
            
        }
        return AgeOption;
    } (Seagull2Answer.RadioOption));

    var Age = (function (_super) {
        Seagull2Answer.__extends(Age, _super);
        function Age() {
            _super.call(this);

        }

        return Age;
    } (Seagull2Answer.Radio));

    Seagull2Answer.AgeOption = AgeOption;
    Seagull2Answer.Age = Age;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));