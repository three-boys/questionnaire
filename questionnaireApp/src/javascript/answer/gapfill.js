(function (Seagull2Answer) {
    var Gapfill = (function (_super) {
        Seagull2Answer.__extends(Gapfill, _super);
        function Gapfill(_super) {
            this.templateUrl = './views/exam-preview/questions/gapfill.html';
        }
        return Gapfill;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.Gapfill = Gapfill;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));