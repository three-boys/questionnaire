(function (Seagull2Answer) {
    var Sum = (function (_super) {
        Seagull2Answer.__extends(Sum, _super);
        function Sum(_super) {
            this.templateUrl = './views/exam-preview/questions/sum.html';
        }
        return Sum;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.Sum = Sum;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));