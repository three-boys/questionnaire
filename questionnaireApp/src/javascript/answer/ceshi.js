(function (Seagull2Answer) {

    var CeshiOption = (function (_super) {
        Seagull2Answer.__extends(CeshiOption, _super);
        function CeshiOption() {
            _super.call(this);

        }
        return CeshiOption;
    } (Seagull2Answer.RadioOption));

    var Ceshi = (function (_super) {
        Seagull2Answer.__extends(Ceshi, _super);
        function Ceshi() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/ceshi.html';
            this.optionClass = CeshiOption;
        }


        return Ceshi;
    } (Seagull2Answer.Radio)); 
    Seagull2Answer.CeshiOption = CeshiOption;
    Seagull2Answer.Ceshi = Ceshi;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));