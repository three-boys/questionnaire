
(function (Seagull2Answer) {

    var QuestionOption = (function () {
        function QuestionOption() {
            

        }
        return QuestionOption;
    } ());

    var Name = (function (_super) {
        Seagull2Answer.__extends(Name, _super);
        function Name() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/exam-preview/questions/name.html';
            this.optionClass = QuestionOption;
        }

        //填空无条件跳题  
        Name.prototype.gapFillingSkip = function (questions, answerOption, option) {
            if (answerOption !== '' && this.unconditionalJMP === true && this.unconditionalJMPTo > this.no && this.unconditionalJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                    questions[i].unconditionalSkipClass = true;
                }
            } else {
                var noShows = true;
                for (var r = 0; r < this.options.length; r++) {
                    if (this.options[r].answerOption !== '') {
                        noShows = false;
                    }
                }
                if (noShows) {
                    for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                        questions[i].unconditionalSkipClass = false;
                    }
                }
            }
        };



        return Name;
    } (Seagull2Answer.QuestionBase)); 

    Seagull2Answer.QuestionOption = QuestionOption;
    Seagull2Answer.Name = Name;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));






