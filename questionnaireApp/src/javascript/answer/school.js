(function (Seagull2Answer) {
    var School = (function (_super) {
        Seagull2Answer.__extends(School, _super);
        function School(_super) {
            this.templateUrl = './views/exam-preview/questions/school.html';
        }
        return School;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.School = School;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));