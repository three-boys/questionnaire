(function (Seagull2Answer) {
    var MatrixSlider = (function (_super) {
        Seagull2Answer.__extends(MatrixSlider, _super);
        function MatrixSlider(_super) {
            this.templateUrl = './views/exam-preview/questions/matrixSlider.html';
        }
        return MatrixSlider;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.MatrixSlider = MatrixSlider;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));