(function (Seagull2Answer) {

    var MatrixOption = (function () {
        function MatrixOption() {

        }
        return MatrixOption;
    } ());

    var Matrix = (function (_super) {
        Seagull2Answer.__extends(Matrix, _super);
        function Matrix() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/exam-preview/questions/matrix.html';
            this.optionClass = MatrixOption;

        }

        Matrix.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };

        //填空无条件跳题  
        Matrix.prototype.gapFillingSkip = function (questions, answerOption, option) {
            if (answerOption !== '' && this.unconditionalJMP === true && this.unconditionalJMPTo > this.no && this.unconditionalJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                    questions[i].unconditionalSkipClass = true;
                }
            } else {
                var noShows = true;
                for (var r = 0; r < this.options.length; r++) {
                    if (this.options[r].answerOption !== '') {
                        noShows = false;
                    }
                }
                if (noShows) {
                    for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                        questions[i].unconditionalSkipClass = false;
                    }
                }
            }
        };



        return Matrix;
    } (Seagull2Answer.QuestionBase));

    Seagull2Answer.MatrixOption = MatrixOption;
    Seagull2Answer.Matrix = Matrix;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));