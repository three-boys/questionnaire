(function (Seagull2Answer) {

    var CepingsOption = (function (_super) {
        Seagull2Answer.__extends(CepingsOption, _super);
        function CepingsOption() {
            _super.call(this);


        }
        return CepingsOption;
    } (Seagull2Answer.RadioOption));

    var Cepings = (function (_super) {
        Seagull2Answer.__extends(Cepings, _super);
        function Cepings() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/cepings.html';
            this.optionClass = CepingsOption;
        }

        //至多至少 
        Cepings.prototype.rangeNumber = function (answerOption,questions, option) { 
            if (this.atLeast && this.atMost) {
                var numbers = 0;
                for (var i = 0; i < this.options.length; i++) {

                    if (this.options[i].answerOption === true) {
                        numbers = numbers + 1;
                    }
                } 
                if (numbers < this.atLeast) {
                    this.leastRangeMessage = true;
                    this.outRangeMessage = false;
                } else if (numbers > this.atMost) {
                    this.outRangeMessage = true;
                    this.leastRangeMessage = false;
                } else {
                    this.outRangeMessage = false;
                    this.leastRangeMessage = false;
                }
            }
            //无条件跳题
            checkUnconditionalSkip(answerOption,questions, this);
            //关联逻辑 
            getRelatedOption(questions, this, option, answerOption);  
        };

         //无条件跳题
        function checkUnconditionalSkip(answerOption,questions, question) {
            if (question.unconditionalJMP === true && question.unconditionalJMPTo > question.no && question.unconditionalJMPTo < questions.length) {
                for (var i = question.sortNo + 1; i < question.unconditionalJMPTo; i++) {
                    if(answerOption===true){
                        questions[i].unconditionalSkipClass = true;
                    }else {
                        var noHide=true;
                        for(var l=0; l<question.options.length; l++){
                            if(question.options[l].answerOption===true){
                               noHide = false;
                            }
                        }
                        if(noHide){
                            questions[i].unconditionalSkipClass = false;
                        }
                    }
                }
            }
        };

        //关联逻辑 
        function getRelatedOption(questions, currentQuestion, option, answerOption) {
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].related) {
                    if (questions[i].relevanceQuestion.no == currentQuestion.no) {
                        for (var j = 0; j < questions[i].relevanceQuestion.options.length; j++) {
                            if (questions[i].relevanceQuestion.options[j].logicOption) {
                                var noShow = true;
                                for (var l = 0; l < currentQuestion.options.length; l++) {
                                    if(currentQuestion.options[l].answerOption && currentQuestion.options[l].title === questions[i].relevanceQuestion.options[j].title){
                                        noShow = false;
                                    }
                                }
                                if (answerOption && option.title === questions[i].relevanceQuestion.options[j].title) {
                                    questions[i].isRelevanceQuestion = false;
                                }
                                else if(noShow){
                                    questions[i].isRelevanceQuestion = true;
                                }
                            }
                        }
                    }
                }
            }
        };




        return Cepings;
    } (Seagull2Answer.Radio));

    Seagull2Answer.CepingsOption = CepingsOption;
    Seagull2Answer.Cepings = Cepings;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));