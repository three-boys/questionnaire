(function (Seagull2Answer) {

    var QuestionOption = (function () {
        function QuestionOption() {
            

        }
        return QuestionOption;
    } ());

    var Phone = (function (_super) {
        Seagull2Answer.__extends(Phone, _super);
        function Phone() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/exam-preview/questions/phone.html';
            this.optionClass = QuestionOption; 
        }

        var phones=/^[1][34578][0-9]{9}$/;
        Phone.prototype.yz = function (option) {
            if(!phones.test(option.answerOption))
            {
              option.verificationBy=false;
            }else{
                 option.verificationBy=true;
            }
        };

        //填空无条件跳题  
        Phone.prototype.gapFillingSkip = function (questions, answerOption, option) {
            if (answerOption !== '' && this.unconditionalJMP === true && this.unconditionalJMPTo > this.no && this.unconditionalJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                    questions[i].unconditionalSkipClass = true;
                }
            } else {
                var noShows = true;
                for (var r = 0; r < this.options.length; r++) {
                    if (this.options[r].answerOption !== '') {
                        noShows = false;
                    }
                }
                if (noShows) {
                    for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                        questions[i].unconditionalSkipClass = false;
                    }
                }
            }
        };

        return Phone;
    } (Seagull2Answer.QuestionBase));

    Seagull2Answer.QuestionOption = QuestionOption;
    Seagull2Answer.Phone = Phone;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));