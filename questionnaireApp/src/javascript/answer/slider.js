(function (Seagull2Answer) {
    var Slider = (function (_super) {
        Seagull2Answer.__extends(Slider, _super);
        function Slider() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/slider.html';
        }
        Slider.prototype.drag = function (option, item, self) {
            //新浏览器
            if (self) {
                if (self.buttons == 1) {
                    option.answerOption = item;
                }
                //IE8
                if (window.event.button === 1) {
                    option.answerOption = item;
                }
            } else {
                //移动端
                option.answerOption = item;
            }
        };

        return Slider;
    } (Seagull2Answer.QuestionBase));


    Seagull2Answer.Slider = Slider;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));



















// (function (Seagull2Answer) {

//     var SliderOption = (function (_super) {
//         Seagull2Answer.__extends(SliderOption, _super);
//         function SliderOption() {
//             _super.call(this); 

//         }
//         return SliderOption;
//     } (Seagull2Answer.RadioDownOption)); 

//     var Slider = (function (_super) {
//         Seagull2Answer.__extends(Slider, _super);
//         function Slider() {
//             _super.call(this);
//             this.templateUrl = './views/exam-preview/questions/slider.html';
//             this.optionClass = SliderOption;
//         }


//         Slider.prototype.abc = function () {
//             debugger
//         };


//         return Slider;
//     } (Seagull2Answer.RadioDown));

//     Seagull2Answer.SliderOption = SliderOption;
//     Seagull2Answer.Slider = Slider;
// })(window.Seagull2Answer || (window.Seagull2Answer = {}));