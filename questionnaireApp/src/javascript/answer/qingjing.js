(function (Seagull2Answer) {
    var Qingjing = (function (_super) {
        Seagull2Answer.__extends(Qingjing, _super);
        function Qingjing(_super) {
            this.templateUrl = './views/exam-preview/questions/qingjing.html';
        }
        return Qingjing;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.Qingjing = Qingjing;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));