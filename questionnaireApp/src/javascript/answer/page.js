(function (Seagull2Answer) {
    var Page = (function (_super) {
        Seagull2Answer.__extends(Page, _super);
        function Page() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/page.html';
        } 
        return Page;
    } (Seagull2Answer.QuestionBase));   
    Seagull2Answer.Page = Page;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));