(function (Seagull2Answer) {

    var QuestionOption = (function () {
        function QuestionOption() {
            
        }
        return QuestionOption;
    } ());

    var Email = (function (_super) {
        Seagull2Answer.__extends(Email, _super);
        function Email() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/exam-preview/questions/email.html'; 
            this.optionClass = QuestionOption;
        }
        var emails=/^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/;
        Email.prototype.yz = function (option) {
            if(!emails.test(option.answerOption))
            {
              option.verificationBy=false;
            }else{
                 option.verificationBy=true;
            }
        };

        //填空无条件跳题  
        Email.prototype.gapFillingSkip = function (questions, answerOption, option) {
            if (answerOption !== '' && this.unconditionalJMP === true && this.unconditionalJMPTo > this.no && this.unconditionalJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                    questions[i].unconditionalSkipClass = true;
                }
            } else {
                var noShows = true;
                for (var r = 0; r < this.options.length; r++) {
                    if (this.options[r].answerOption !== '') {
                        noShows = false;
                    }
                }
                if (noShows) {
                    for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                        questions[i].unconditionalSkipClass = false;
                    }
                }
            }
        };

        return Email;
    } (Seagull2Answer.QuestionBase));

    Seagull2Answer.QuestionOption = QuestionOption;
    Seagull2Answer.Email = Email;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));
