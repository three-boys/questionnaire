(function (Seagull2Answer) { 

    var IndustryOption = (function (_super) {
        Seagull2Answer.__extends(IndustryOption, _super);
        function IndustryOption() {
            _super.call(this);            
            
        }
        return IndustryOption;
    } (Seagull2Answer.RadioDownOption));

    var Industry = (function (_super) {
        Seagull2Answer.__extends(Industry, _super);
        function Industry() {
            _super.call(this);
           
        }

        return Industry;
    } (Seagull2Answer.RadioDown));

    Seagull2Answer.IndustryOption = IndustryOption;
    Seagull2Answer.Industry = Industry;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));