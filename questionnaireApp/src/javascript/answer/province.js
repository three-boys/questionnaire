(function (Seagull2Answer) {

    var ProvinceOption = (function (_super) {
        Seagull2Answer.__extends(ProvinceOption, _super);
        function ProvinceOption() {
            _super.call(this);            
                       
        }
        return ProvinceOption;
    } (Seagull2Answer.RadioOption));

    var Province = (function (_super) {
        Seagull2Answer.__extends(Province, _super);
        function Province() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/radio.html';
            this.optionClass = ProvinceOption; 
        }

        return Province;
    } (Seagull2Answer.Radio));

    Seagull2Answer.ProvinceOption = ProvinceOption;
    Seagull2Answer.Province = Province;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));