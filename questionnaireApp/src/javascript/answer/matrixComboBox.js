(function (Seagull2Answer) {
    var MatrixComboBox = (function (_super) {
        Seagull2Answer.__extends(MatrixComboBox, _super);
        function MatrixComboBox(_super) {
            this.templateUrl = './views/exam-preview/questions/matrixComboBox.html';
        }
        return MatrixComboBox;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.MatrixComboBox = MatrixComboBox;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));