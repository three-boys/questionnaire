(function (Seagull2Answer) {
    var RadioOption = (function (_super) {
        Seagull2Answer.__extends(RadioOption, _super);
        function RadioOption() {
            _super.call(this);
            
        }
        return RadioOption; 
    } (Seagull2Answer.RadioDownOption));  
 
    var Radio = (function (_super) {
        Seagull2Answer.__extends(Radio, _super);
        function Radio() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/radio.html';
            this.optionClass = RadioOption;  
            this.abb = 123456;         
        }

        


        return Radio;
    } (Seagull2Answer.RadioDown));

    Seagull2Answer.RadioOption = RadioOption;
    Seagull2Answer.Radio = Radio;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));