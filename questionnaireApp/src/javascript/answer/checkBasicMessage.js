
(function (Seagull2Answer) {

    var CheckBasicMessageOption = (function () {
        function CheckBasicMessageOption() {
             this.title = '标题';
             this.rightTitle = '';
             this.rightInputWidth='leftwidthtwenty';
             this.validate='0';
             this.minNumber=0;
            this.maxNumber=0; 
        }
        return CheckBasicMessageOption;
    } ());

    var CheckBasicMessage = (function (_super) {
        Seagull2Answer.__extends(CheckBasicMessage, _super); 
        function CheckBasicMessage() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/CheckBasicMessage.html';
            this.optionClass = CheckBasicMessageOption;
        }

        CheckBasicMessage.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };

        //填空无条件跳题  
        CheckBasicMessage.prototype.gapFillingSkip = function (questions, answerOption, option) {
            if (answerOption !== '' && this.unconditionalJMP === true && this.unconditionalJMPTo > this.no && this.unconditionalJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                    questions[i].unconditionalSkipClass = true;
                }
            } else {
                var noShows = true;
                for (var r = 0; r < this.options.length; r++) {
                    if (this.options[r].answerOption !== '') {
                        noShows = false;
                    }
                }
                if (noShows) {
                    for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                        questions[i].unconditionalSkipClass = false;
                    }
                }
            }
        };



        return CheckBasicMessage;
    } (Seagull2Answer.QuestionBase));

    Seagull2Answer.CheckBasicMessageOption = CheckBasicMessageOption;
    Seagull2Answer.CheckBasicMessage = CheckBasicMessage;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));