(function (Seagull2Answer) {

    var VoteCheckOption = (function (_super) {
        Seagull2Answer.__extends(VoteCheckOption, _super);
        function VoteCheckOption() {
            _super.call(this);

        }
        return VoteCheckOption;
    } (Seagull2Answer.RadioOption));

    var VoteCheck = (function (_super) {
        Seagull2Answer.__extends(VoteCheck, _super);
        function VoteCheck() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/voteCheck.html';
            this.optionClass = VoteCheckOption;
        }

        //选项互斥
        VoteCheck.prototype.mutualReject = function (isMutual, answerOption) {
            if (isMutual === true) {
                for (var i = 0; i < this.options.length; i++) {
                    if (this.options[i].isMutual === false) { 
                        this.options[i].answerOption = false;
                    }
                }
            } else if (isMutual === false) {
                for (var i = 0; i < this.options.length; i++) {
                    if (this.options[i].isMutual === true) {
                        this.options[i].answerOption = false;
                    }
                }
            }
        };

        //至多至少 
        VoteCheck.prototype.rangeNumber = function (answerOption, questions, option) {
            if (this.atLeast && this.atMost) {
                var numbers = 0;
                for (var i = 0; i < this.options.length; i++) {

                    if (this.options[i].answerOption === true) {
                        numbers = numbers + 1;
                    }
                }
                if (numbers < this.atLeast) {
                    this.leastRangeMessage = true;
                    this.outRangeMessage = false;
                } else if (numbers > this.atMost) {
                    this.outRangeMessage = true;
                    this.leastRangeMessage = false;
                } else {
                    this.outRangeMessage = false;
                    this.leastRangeMessage = false;
                }
            }
            //无条件跳题
            checkUnconditionalSkip(answerOption,questions, this)
            //关联逻辑 
            getRelatedOption(questions, this, option, answerOption);
        };

         //无条件跳题
        function checkUnconditionalSkip(answerOption,questions, question) {
            if (question.unconditionalJMP === true && question.unconditionalJMPTo > question.no && question.unconditionalJMPTo < questions.length) {
                for (var i = question.sortNo + 1; i < question.unconditionalJMPTo; i++) {
                    if(answerOption===true){
                        questions[i].unconditionalSkipClass = true;
                    }else {
                        var noHide=true;
                        for(var l=0; l<question.options.length; l++){
                            if(question.options[l].answerOption===true){
                               noHide = false;
                            }
                        }
                        if(noHide){
                            questions[i].unconditionalSkipClass = false;
                        }
                    }
                }
            }
        };

        //关联逻辑 
        function getRelatedOption(questions, currentQuestion, option, answerOption) {
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].related) {
                    if (questions[i].relevanceQuestion.no == currentQuestion.no) {
                        for (var j = 0; j < questions[i].relevanceQuestion.options.length; j++) {
                            if (questions[i].relevanceQuestion.options[j].logicOption) {
                                var noShow = true;
                                for (var l = 0; l < currentQuestion.options.length; l++) {
                                    if(currentQuestion.options[l].answerOption && currentQuestion.options[l].title === questions[i].relevanceQuestion.options[j].title){
                                        noShow = false;
                                    }
                                }
                                if (answerOption && option.title === questions[i].relevanceQuestion.options[j].title) {
                                    questions[i].isRelevanceQuestion = false;
                                }
                                else if(noShow){
                                    questions[i].isRelevanceQuestion = true;
                                }
                            }
                        }
                    }
                }
            }
        };



        return VoteCheck;
    } (Seagull2Answer.Radio));

    Seagull2Answer.VoteCheckOption = VoteCheckOption;
    Seagull2Answer.VoteCheck = VoteCheck;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));

