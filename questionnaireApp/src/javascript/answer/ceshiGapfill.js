(function (Seagull2Answer) {

    var QuestionOption = (function () {
        function QuestionOption() {
           

        }
        return QuestionOption;
    } ());

    var CeshiGapfill = (function (_super) {
        Seagull2Answer.__extends(CeshiGapfill, _super);
        function CeshiGapfill() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/exam-preview/questions/ceshiGapfill.html';
            this.optionClass = QuestionOption;
        }

        



        return CeshiGapfill;
    } (Seagull2Answer.QuestionBase));

    Seagull2Answer.QuestionOption = QuestionOption;
    Seagull2Answer.CeshiGapfill = CeshiGapfill;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));