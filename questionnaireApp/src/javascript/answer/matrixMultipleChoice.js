(function (Seagull2Answer) {
    var MatrixMultipleChoice = (function (_super) {
        Seagull2Answer.__extends(MatrixMultipleChoice, _super);
        function MatrixMultipleChoice(_super) {
            this.templateUrl = './views/exam-preview/questions/matrixMultipleChoice.html';
        }
        return MatrixMultipleChoice;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.MatrixMultipleChoice = MatrixMultipleChoice;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));