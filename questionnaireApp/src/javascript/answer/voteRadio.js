(function (Seagull2Answer) {

    var VoteRadioOption = (function (_super) { 
        Seagull2Answer.__extends(VoteRadioOption, _super); 
        function VoteRadioOption() {
            _super.call(this);
            

        }
        return VoteRadioOption;
    } (Seagull2Answer.RadioOption)); 

    var VoteRadio = (function (_super) {
        Seagull2Answer.__extends(VoteRadio, _super);
        function VoteRadio() {
            _super.call(this);
            this.templateUrl = './views/exam-preview/questions/voteRadio.html';
            this.optionClass = VoteRadioOption;; 
        }


        return VoteRadio;
    } (Seagull2Answer.Radio));

    Seagull2Answer.VoteRadioOption = VoteRadioOption;
    Seagull2Answer.VoteRadio = VoteRadio;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));