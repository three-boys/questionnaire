(function (Seagull2Answer) { 

    var ProfessionOption = (function (_super) {
        Seagull2Answer.__extends(ProfessionOption, _super);
        function ProfessionOption() {
            _super.call(this);            
            
        }
        return ProfessionOption;
    } (Seagull2Answer.RadioDownOption));

    var Profession = (function (_super) {
        Seagull2Answer.__extends(Profession, _super);
        function Profession() {
            _super.call(this);
 
        }

        return Profession;
    } (Seagull2Answer.RadioDown));

    Seagull2Answer.ProfessionOption = ProfessionOption;
    Seagull2Answer.Profession = Profession;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));