
(function (Seagull2Answer) {

    var RadioDownOption = (function () {
        function RadioDownOption() {
            this.answerOption = false;
        }
        return RadioDownOption;
    } ());

    var RadioDown = (function (_super) {
        Seagull2Answer.__extends(RadioDown, _super);
        function RadioDown() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/exam-preview/questions/radioDown.html';
            this.optionClass = RadioDownOption;
        }

        //无条件跳题
        RadioDown.prototype.unconditionalSkip = function (questions, optionJMPTo, answerOption, option) {
            option.answerOption = true;
            for (var m = 0; m < this.options.length; m++) {
                if (this.options[m].title !== option.title) {
                    this.options[m].answerOption = false;
                }
            }
            if (this.unconditionalJMP === true && this.unconditionalJMPTo > this.no && this.unconditionalJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                    questions[i].unconditionalSkipClass = true;
                }
            }
            if (this.optionJMP === true && option.optionJMPTo > this.no && option.optionJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < option.optionJMPTo; i++) {
                    if(questions[i].questionType !== "分页"&& questions[i].questionType !== "段落说明"){
                        questions[i].unconditionalSkipClass = true;
                    }                    
                }
            } else if(this.unconditionalJMP !== true) {
                for (var i = 0; i < questions.length; i++) {
                    questions[i].unconditionalSkipClass = false;
                }
            }
            //关联逻辑 
            getRelatedOption(questions, this, option);
        };
        
        function getRelatedOption(questions, currentQuestion, option) {
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].related) {
                    if (questions[i].relevanceQuestion.no == currentQuestion.no) {
                        for (var j = 0; j < questions[i].relevanceQuestion.options.length; j++) {
                            if (questions[i].relevanceQuestion.options[j].logicOption) {
                                if (option.answerOption && option.title === questions[i].relevanceQuestion.options[j].title) {
                                    questions[i].isRelevanceQuestion = false;
                                }
                                else {
                                    questions[i].isRelevanceQuestion = true;
                                }
                            }
                        }
                    }
                }
            }
        };
        
        //下拉无条件跳题
        RadioDown.prototype.dropDownSkip = function (questions, optionJMPTo) {
            if (this.unconditionalJMP === true && this.unconditionalJMPTo > this.no && this.unconditionalJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                    questions[i].unconditionalSkipClass = true;
                }
            }
            if (this.optionJMP === true && option.optionJMPTo > this.no && option.optionJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < option.optionJMPTo; i++) {
                    if(questions[i].questionType !== "分页"&& questions[i].questionType !== "段落说明"){
                        questions[i].unconditionalSkipClass = true;
                    }                    
                }
            } else if(this.unconditionalJMP !== true) {
                for (var i = 0; i < questions.length; i++) {
                    questions[i].unconditionalSkipClass = false;
                }
            }
            //关联逻辑
            getRelatedOption(questions, this, option);
        };

        



        return RadioDown;
    } (Seagull2Answer.QuestionBase));

    Seagull2Answer.RadioDownOption = RadioDownOption;
    Seagull2Answer.RadioDown = RadioDown;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));