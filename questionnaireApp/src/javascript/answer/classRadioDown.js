(function (Seagull2Answer) {
    var ClassRadioDown = (function (_super) {
        Seagull2Answer.__extends(ClassRadioDown, _super);
        function ClassRadioDown(_super) {
            this.templateUrl = './views/exam-preview/questions/classRadioDown.html';
        }
        return ClassRadioDown;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.ClassRadioDown = ClassRadioDown;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));