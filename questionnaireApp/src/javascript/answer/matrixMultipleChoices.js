(function (Seagull2Answer) {
    var MatrixMultipleChoices = (function (_super) {
        Seagull2Answer.__extends(MatrixMultipleChoices, _super);
        function MatrixMultipleChoices(_super) {
            this.templateUrl = './views/exam-preview/questions/matrixMultipleChoices.html';
        }
        return MatrixMultipleChoices;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.MatrixMultipleChoices = MatrixMultipleChoices;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));