(function (Seagull2Answer) {
    var Shop = (function (_super) {
        Seagull2Answer.__extends(Shop, _super);
        function Shop(_super) {
            this.templateUrl = './views/exam-preview/questions/shop.html';
        }
        return Shop;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.Shop = Shop;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));