(function (Seagull2Answer) {
    var MatrixText = (function (_super) {
        Seagull2Answer.__extends(MatrixText, _super);
        function MatrixText(_super) {
            this.templateUrl = './views/exam-preview/questions/matrixText.html';
        }
        return MatrixText;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.MatrixText = MatrixText;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));