(function (Seagull2Answer) {
    var ProvinceCity = (function (_super) {
        Seagull2Answer.__extends(ProvinceCity, _super);
        function ProvinceCity(_super) {
            this.templateUrl = './views/exam-preview/questions/provinceCity.html';
        }
        return ProvinceCity;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.ProvinceCity = ProvinceCity;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));