(function (Seagull2Answer) {
    var MatrixScale = (function (_super) {
        Seagull2Answer.__extends(MatrixScale, _super);
        function MatrixScale(_super) {
            this.templateUrl = './views/exam-preview/questions/matrixScale.html';
        }
        return MatrixScale;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.MatrixScale = MatrixScale;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));