(function (Seagull2Answer) { 

    var GenderOption = (function (_super) {
        Seagull2Answer.__extends(GenderOption, _super); 
        function GenderOption() {
            _super.call(this);            
            
        }
        return GenderOption;
    } (Seagull2Answer.RadioOption));

    var Gender = (function (_super) {
        Seagull2Answer.__extends(Gender, _super);
        function Gender() {
            _super.call(this);

        }

        return Gender;
    } (Seagull2Answer.Radio));

    Seagull2Answer.GenderOption = GenderOption;
    Seagull2Answer.Gender = Gender;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));
