(function (Seagull2Answer) {
    var Date = (function (_super) {
        Seagull2Answer.__extends(Date, _super);
        function Date(_super) {
            this.templateUrl = './views/exam-preview/questions/date.html';
        }
        return Date;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.Date = Date;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));