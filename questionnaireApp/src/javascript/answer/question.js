

(function (Seagull2Answer) {

    var QuestionOption = (function () {
        function QuestionOption() {

        }
        return QuestionOption;
    } ());

    var Question = (function (_super) {
        Seagull2Answer.__extends(Question, _super);
        function Question() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/exam-preview/questions/question.html';
            this.optionClass = QuestionOption;
        }

        //填空无条件跳题  
        Question.prototype.gapFillingSkip = function (questions, answerOption, option) {
            if (answerOption !== '' && this.unconditionalJMP === true && this.unconditionalJMPTo > this.no && this.unconditionalJMPTo < questions.length) {
                for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                    questions[i].unconditionalSkipClass = true;
                }
            } else {
                var noShows = true;
                for (var r = 0; r < this.options.length; r++) {
                    if (this.options[r].answerOption !== '') {
                        noShows = false;
                    }
                }
                if (noShows) {
                    for (var i = this.sortNo + 1; i < this.unconditionalJMPTo; i++) {
                        questions[i].unconditionalSkipClass = false;
                    }
                }
            }
        };

        //验证
        Question.prototype.fillingVerify = function (option) {
            var verification;
            var integer = /^-?[1-9]d*$/;
            var phone = /^[1][34578][0-9]{9}$/;
            var decimals = /^[0-9]+(.[0-9]{1,3})?$/;
            var email = /^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/;
            var identity = /^\d{15}|\d{18}$/;
            var qq = /^[1-9][0-9]{4,}$/;
            var chinese = /^[\u4e00-\u9fa5]{0,}$/;
            var english = /^[A-Za-z]+$/;
            for (var i = 0; i < this.options.length; i++) {
                if (this.options[i].validate === '整数') {
                    verification = integer;
                }
                if (this.options[i].validate === '手机') {
                    verification = phone;
                }
                if (this.options[i].validate === '小数') {
                    verification = decimals;
                }
                if (this.options[i].validate === '邮件') {
                    verification = email;
                }
                if (this.options[i].validate === '身份证号') {
                    verification = identity;
                }
                if (this.options[i].validate === 'QQ') {
                    verification = qq;
                }
                if (this.options[i].validate === '汉字') {
                    verification = chinese;
                }
                if (this.options[i].validate === '英文') {
                    verification = english;
                }
            }
            if (verification && !verification.test(option.answerOption)) {
                option.verificationBy = false;
            } else {
                option.verificationBy = true;
            }
        };

        return Question;
    } (Seagull2Answer.QuestionBase));

    Seagull2Answer.QuestionOption = QuestionOption;
    Seagull2Answer.Question = Question;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));