(function (Seagull2Answer) {
    var PhoneCheckout = (function (_super) {
        Seagull2Answer.__extends(PhoneCheckout, _super);
        function PhoneCheckout(_super) {
            this.templateUrl = './views/exam-preview/questions/phoneCheckout.html';
        }
        return PhoneCheckout;
    } (Seagull2Answer.QuestionBase));
    Seagull2Answer.PhoneCheckout = PhoneCheckout;
})(window.Seagull2Answer || (window.Seagull2Answer = {}));