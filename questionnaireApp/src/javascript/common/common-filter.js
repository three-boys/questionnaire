(function (window, angular) {
    'use strict';
    var module = angular.module('commonFilter', []);
    module.filter("questionnaireStatus", function () {
        return function (status, uppercase) {
            switch (status) {
                case 1:
                    return '草稿';
                case 2:
                    return '发布';
                case 4:
                    return '完成'
                default:
                    return '无状态';
            }
        };
    });

    module.filter("questionNoFilter", function () {
        return function (question, questions) {
            if (question.questionType === "分页" || questions === null || questions === undefined) {
                return null;
            }
            var pageCount = 0;
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].questionType === "分页" || questions[i].questionType === "段落说明") {
                    pageCount += 1;
                } else if (questions[i] === question) {
                    questions[i].no = (i + 1) - pageCount;
                    return questions[i].no;
                }
            }
        }
    });

    module.filter("pageNoFilter", function () {
        return function (question, questions) {
            if (question.questionType !== "分页" || questions === null || questions === undefined) {
                return null;
            }
            var pageCount = 0;
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].questionType !== "分页") {
                    pageCount += 1;
                } else if (questions[i] === question && questions.length > 1) {
                    questions[i].No = (i + 1) - pageCount;
                }
            }
            if (questions.length - pageCount > 1) {
                return '第' + question.No + '页/共' + (questions.length - pageCount) + '页';
            }
        }
    });

    module.filter("questionAnswerFilter", function () {
        return function (question, questions) {
            var pageCount = 0;
            for (var i = 0; i < questions.length; i++) {
                if (questions[i].questionType !== "分页" && questions[i].questionType !== "段落说明") {
                    pageCount += 1;
                }
            }
            return pageCount;
        }
    });

    // module.filter("noQuestion", function () {
    //     return function (question, questions) {
    //         if (question.questionType !== "分页" || questions === null || questions === undefined) {
    //             return null;
    //         }
    //         var pageCount = 0;
    //         for (var i = 0; i < questions.length; i++) {
    //             if (questions[i].questionType !== "分页") {
    //                 pageCount += 1;
    //             } else if (questions[i] === question && questions.length > 1) {
    //                 questions[i].No = (i + 1) - pageCount;
    //             }
    //         }
    //         if (questions.length - pageCount > 1) {
    //             return '第' + question.No + '页/共' + (questions.length - pageCount) + '页';
    //         }
    //     }
    // });

    //获得templateUrl
    module.filter("questionTemplate", function () {
        return function (questionType, uppercase) {
            switch (questionType) {
                case "单选题":
                    return './views/design/questions/radio.html';
                case "多选题":
                    return './views/design/questions/check.html';
                case "考试单选题":
                    return './views/design/questions/ceshi.html';
                case "考试多选题":
                    return './views/design/questions/ceshiMultipleChoices.html';
                case "下拉框单选":
                    return './views/design/questions/radioDown.html';
                case "评分单选题":
                    return './views/design/questions/ceping.html';
                case "评分多选题":
                    return './views/design/questions/cepings.html';
                case "投票多选题":
                    return './views/design/questions/voteCheck.html';
                case "投票单选题":
                    return './views/design/questions/voteRadio.html';
                case "单项填空":
                    return './views/design/questions/question.html';
                case "矩阵填空":
                    return './views/design/questions/matrix.html';
                case "量表题":
                    return './views/design/questions/likert.html';
                // case "判断题":
                //     return './views/design/questions/ceshiJudge.html';
                case "考试单项填空题":
                    return './views/design/questions/ceshiQuestion.html';
                case "考试多项填空题":
                    return './views/design/questions/ceshiGapfill.html';
                case "考试简答":
                    return './views/design/questions/checkPermission.html';
                case "考试姓名":
                    return './views/design/questions/name.html';
                case "基本信息":
                    return './views/design/questions/checkBasicMessage.html';
                case "分页":
                    return './views/design/questions/page.html';
                case "段落说明":
                    return './views/design/questions/cut.html';
                case "滚动条":
                    return './views/design/questions/slider.html';
                case "排序":
                    return './views/design/questions/sort.html';
                case "上传文件":
                    return './views/design/questions/fileupload.html';
                case "手机":
                    return './views/design/questions/phone.html';
                case "Email":
                    return './views/design/questions/email.html';

                default:
                    return '';

            }
        };
    });
    //添加查看过滤器
    //获得templateUrl
    module.filter("previewquestionTemplat", function () {
        return function (questionType, uppercase) {
            switch (questionType) {
                case "单选题":
                    return './views/exam-preview/questions/radio.html';
                case "多选题":
                    return './views/exam-preview/questions/check.html';
                case "考试单选题":
                    return './views/exam-preview/questions/ceshi.html';
                case "考试多选题":
                    return './views/exam-preview/questions/ceshiMultipleChoices.html';
                case "下拉框单选":
                    return './views/exam-preview/questions/radioDown.html';
                case "评分单选题":
                    return './views/exam-preview/questions/ceping.html';
                case "评分多选题":
                    return './views/exam-preview/questions/cepings.html';
                case "投票多选题":
                    return './views/exam-preview/questions/voteCheck.html';
                case "投票单选题":
                    return './views/exam-preview/questions/voteRadio.html';
                case "单项填空":
                    return './views/exam-preview/questions/question.html';
                case "矩阵填空":
                    return './views/exam-preview/questions/matrix.html';
                case "量表题":
                    return './views/exam-preview/questions/likert.html';
                // case "判断题":
                //     return './views/exam-preview/questions/ceshiJudge.html';
                case "考试单项填空题":
                    return './views/exam-preview/questions/ceshiQuestion.html';
                case "考试多项填空题":
                    return './views/exam-preview/questions/ceshiGapfill.html';
                case "考试简答":
                    return './views/exam-preview/questions/checkPermission.html';
                case "考试姓名":
                    return './views/exam-preview/questions/name.html';
                case "基本信息":
                    return './views/exam-preview/questions/checkBasicMessage.html';
                case "分页":
                    return './views/exam-preview/questions/page.html';
                case "段落说明":
                    return './views/exam-preview/questions/cut.html';
                case "滚动条":
                    return './views/exam-preview/questions/slider.html';
                case "排序":
                    return './views/exam-preview/questions/sort.html';
                case "上传文件":
                    return './views/exam-preview/questions/fileupload.html';
                case "手机":
                    return './views/exam-preview/questions/phone.html';
                case "Email":
                    return './views/exam-preview/questions/email.html';

                default:
                    return '';

            }
        };
    });
    module.filter("numberToArray", function () {
        return function (minValue, maxValue) {
            var data = [];
            for (var i = minValue; i <= maxValue; i++) {
                data.push(i);
            }
            return data;
        };
    });
    module.filter("replaceString", function () {
        return function (data) {
            return data.replace(/\./g, ',');
        };
    });
    module.filter("extensionsOperator", function () {
        return function (extensions, pictureExtensions) {
            if (extensions && pictureExtensions) {
                var exs = pictureExtensions.split('.');
                for (var i = 0; i < exs.length; i++) {
                    if (extensions === ("." + exs[i]) && exs[i] !== "") {
                        return true;
                    }
                }
            }

            return false;
        };
    });
    module.filter("paging", function () {
        return function (question, showQuestions, currentPage) {
            if (showQuestions.length === 0 && question.questionType === "分页" && currentPage === question.no) {
                return true;
            }
            for (var i = 0; i < showQuestions.length; i++) {
                if ((question.questionType !== "分页" && showQuestions[i].no === question.no) || (question.questionType === "分页" && currentPage === question.no)) {
                    return true;
                }
            }
            return false;
        };
    });
    //判断是否验证通过,未通过false,通过true
    module.filter("verifyIsPass", function () {
        return function (question, questions) {
            for (var i = 0; i < questions.length; i++) {
                if (question.no === questions[i].no && question.title === questions[i].title) {
                    return false;
                }
            };
            return true;
        }
    });
    module.filter("formatSeconds", function () {
        return function (second_time) {
            if (!angular.isNumber(second_time)) return false;
            var second_timestr = parseInt(second_time) < 10 ? "0" + parseInt(second_time) : parseInt(second_time);
            var time = "00:00:" + second_timestr;
            if (parseInt(second_time) > 60) {

                var second = parseInt(second_time) % 60;
                var min = parseInt(second_time / 60);
                var minstr = min < 10 ? "0" + min : min;
                var secondstr = second < 10 ? "0" + second : second;
                time = "00:" + minstr + ":" + secondstr;

                if (min > 60) {
                    min = parseInt(second_time / 60) % 60;
                    var hour = parseInt(parseInt(second_time / 60) / 60);

                    var hourstr = hour < 10 ? "0" + hour : hour;
                    minstr = min < 10 ? "0" + min : min;
                    time = hourstr + ":" + minstr + ":" + secondstr;

                    if (hour > 24) {
                        hour = parseInt(parseInt(second_time / 60) / 60) % 24;
                        var day = parseInt(parseInt(parseInt(second_time / 60) / 60) / 24);
                        hourstr = hour < 10 ? "0" + hour : hour;
                        var daystr = day < 10 ? "0" + day : day;
                        time = daystr + ":" + hourstr + ":" + minstr + ":" + secondstr;
                    }
                }


            }

            return time;
        };
    });
    //返回投票选项的投票数
    module.filter("voteCountResultFilter", function () {
        return function (option, voteResults) {
            if (voteResults) {
                for (var i = 0; i < voteResults.length; i++) {
                    if (option.questionCode === voteResults[i].questionCode && option.title === voteResults[i].optionTitle) {
                        return voteResults[i].pollNumber;
                    }
                };
            }
            return 0;
        }
    });
    //返回投票选项的比例
    module.filter("votePercentageResultFilter", function () {
        return function (option, voteResults) {
            if (voteResults) {
                for (var i = 0; i < voteResults.length; i++) {
                    if (option.questionCode === voteResults[i].questionCode && option.title === voteResults[i].optionTitle) {
                        return (voteResults[i].percentage * 100).toFixed(2);
                    }
                };
            }
            return 0;
        }
    });
} (window, window.angular)); 