﻿require.config({
    map: {
        '*': {
            'ie8css': './node_modules/requirecss-branch-seagull2/ie8css.1.0.0.min.js',
            'css': './node_modules/requirecss-branch-seagull2/css.1.0.0.min.js'
        }
    },
    waitSeconds: 0,
    //配置angular的路径
    paths: {
        'angular': './node_modules/angular/angular.min',
        'angular-cookies': './node_modules/angular-cookies/angular-cookies.min',
        'angular-ui-router': './node_modules/angular-ui-router/release/angular-ui-router.min',
        'angular-ui-tree': './node_modules/angular-ui-tree/dist/angular-ui-tree.min',
        'angular-async-loader': './node_modules/angular-async-loader/dist/angular-async-loader.min',
        'jslinq': './node_modules/jslinq/build/jslinq.min',
        'jquery': './node_modules/jquery/dist/jquery.min',
        'webuploader': './node_modules/webuploader/dist/webuploader.min',
        'urijs': './node_modules/urijs/src',
        'ng.ueditor': './node_modules/angular-ueditor/dist/angular-ueditor.min',
        'ZeroClipboard': '../ueditor/third-party/zeroclipboard/ZeroClipboard.min',
        'ueditor': '../ueditor/ueditor.all',
        'angular-seagull2-common': './node_modules/angular-seagull2-common/angular-seagull2-common',
        'angular-seagull2-oauth': './node_modules/angular-seagull2-oauth/angular-seagull2-oauth.min',
        'angular-seagull2-workflow': './node_modules/angular-seagull2-workflow/angular-seagull2-workflow',
        'common-filter': './javascript/common/common-filter',
        'echarts': './node_modules/echarts/dist/echarts.min',
        'ng-echarts': './javascript/ng-echarts',
        'angular-datepicker': './node_modules/datepicker-branch-seagull2/datepicker-branch-seagull2.min',
        //问卷       
        'seagull2Question.QuestionsPaper': './javascript/question/questionsPaper',
        'seagull2Question.Radio': './javascript/question/radio',
        'seagull2Question.Check': './javascript/question/check',
        'seagull2Question.Question': './javascript/question/question',
        'seagull2Question.Gapfill': './javascript/question/gapfill',
        'seagull2Question.Matrix': './javascript/question/matrix',
        'seagull2Question.MatrixScale': './javascript/question/matrixScale',
        'seagull2Question.MatrixMultipleChoice': './javascript/question/matrixMultipleChoice',
        'seagull2Question.MatrixMultipleChoices': './javascript/question/matrixMultipleChoices',
        'seagull2Question.MatrixSlider': './javascript/question/matrixSlider',
        'seagull2Question.MatrixComboBox': './javascript/question/matrixComboBox',
        'seagull2Question.MatrixNumerical': './javascript/question/matrixNumerical',
        'seagull2Question.MatrixText': './javascript/question/matrixText',
        'seagull2Question.Fileupload': './javascript/question/fileupload',
        'seagull2Question.Likert': './javascript/question/likert',
        'seagull2Question.Ceping': './javascript/question/ceping',
        'seagull2Question.Cepings': './javascript/question/cepings',
        'seagull2Question.Ceshi': './javascript/question/ceshi',
        'seagull2Question.CeshiJudge': './javascript/question/ceshiJudge',
        'seagull2Question.CeshiMultipleChoices': './javascript/question/ceshiMultipleChoices',
        'seagull2Question.CeshiGapfill': './javascript/question/ceshiGapfill',
        'seagull2Question.CeshiQuestion': './javascript/question/ceshiQuestion',
        'seagull2Question.CheckPermission': './javascript/question/checkPermission',
        'seagull2Question.Name': './javascript/question/name',
        'seagull2Question.CheckBasicMessage': './javascript/question/checkBasicMessage',
        'seagull2Question.VoteRadio': './javascript/question/voteRadio',
        'seagull2Question.VoteCheck': './javascript/question/voteCheck',
        'seagull2Question.Page': './javascript/question/page',
        'seagull2Question.Cut': './javascript/question/cut',
        'seagull2Question.Sort': './javascript/question/sort',
        'seagull2Question.Sum': './javascript/question/sum',
        'seagull2Question.Slider': './javascript/question/slider',
        'seagull2Question.Qingjing': './javascript/question/qingjing',
        'seagull2Question.Shop': './javascript/question/shop',
        'seagull2Question.RadioDown': './javascript/question/radioDown',
        'seagull2Question.ClassRadioDown': './javascript/question/classRadioDown',
        'seagull2Question.Gender': './javascript/question/gender',
        'seagull2Question.Age': './javascript/question/age',
        'seagull2Question.Province': './javascript/question/province',
        'seagull2Question.City': './javascript/question/city',
        'seagull2Question.ProvinceCity': './javascript/question/provinceCity',
        'seagull2Question.Phone': './javascript/question/phone',
        'seagull2Question.PhoneCheckout': './javascript/question/phoneCheckout',
        'seagull2Question.Email': './javascript/question/email',
        'seagull2Question.Date': './javascript/question/date',
        'seagull2Question.Map': './javascript/question/map',
        'seagull2Question.Profession': './javascript/question/profession',
        'seagull2Question.Industry': './javascript/question/industry',
        'seagull2Question.School': './javascript/question/school',
        'seagull2Question.Address': './javascript/question/address',
        'qrcode': './javascript/jsqrcode/qrcode',
        'ngTouch': './javascript/ngTouch',
        //答卷       
        'seagull2Answer.AnswerPaper': './javascript/answer/answerPaper',
        'seagull2Answer.Radio': './javascript/answer/radio',
        'seagull2Answer.Check': './javascript/answer/check',
        'seagull2Answer.Question': './javascript/answer/question',
        'seagull2Answer.Gapfill': './javascript/answer/gapfill',
        'seagull2Answer.Matrix': './javascript/answer/matrix',
        'seagull2Answer.MatrixScale': './javascript/answer/matrixScale',
        'seagull2Answer.MatrixMultipleChoice': './javascript/answer/matrixMultipleChoice',
        'seagull2Answer.MatrixMultipleChoices': './javascript/answer/matrixMultipleChoices',
        'seagull2Answer.MatrixSlider': './javascript/answer/matrixSlider',
        'seagull2Answer.MatrixComboBox': './javascript/answer/matrixComboBox',
        'seagull2Answer.MatrixNumerical': './javascript/answer/matrixNumerical',
        'seagull2Answer.MatrixText': './javascript/answer/matrixText',
        'seagull2Answer.Fileupload': './javascript/answer/fileupload',
        'seagull2Answer.Likert': './javascript/answer/likert',
        'seagull2Answer.Ceping': './javascript/answer/ceping',
        'seagull2Answer.Cepings': './javascript/answer/cepings',
        'seagull2Answer.Ceshi': './javascript/answer/ceshi',
        'seagull2Answer.CeshiJudge': './javascript/answer/ceshiJudge',
        'seagull2Answer.CeshiMultipleChoices': './javascript/answer/ceshiMultipleChoices',
        'seagull2Answer.CeshiGapfill': './javascript/answer/ceshiGapfill',
        'seagull2Answer.CeshiQuestion': './javascript/answer/ceshiQuestion',
        'seagull2Answer.CheckPermission': './javascript/answer/checkPermission',
        'seagull2Answer.Name': './javascript/answer/name',
        'seagull2Answer.CheckBasicMessage': './javascript/answer/checkBasicMessage',
        'seagull2Answer.VoteRadio': './javascript/answer/voteRadio',
        'seagull2Answer.VoteCheck': './javascript/answer/voteCheck',
        'seagull2Answer.Page': './javascript/answer/page',
        'seagull2Answer.Cut': './javascript/answer/cut',
        'seagull2Answer.Sort': './javascript/answer/sort',
        'seagull2Answer.Sum': './javascript/answer/sum',
        'seagull2Answer.Slider': './javascript/answer/slider',
        'seagull2Answer.Qingjing': './javascript/answer/qingjing',
        'seagull2Answer.Shop': './javascript/answer/shop',
        'seagull2Answer.RadioDown': './javascript/answer/radioDown',
        'seagull2Answer.ClassRadioDown': './javascript/answer/classRadioDown',
        'seagull2Answer.Gender': './javascript/answer/gender',
        'seagull2Answer.Age': './javascript/answer/age',
        'seagull2Answer.Province': './javascript/answer/province',
        'seagull2Answer.City': './javascript/answer/city',
        'seagull2Answer.ProvinceCity': './javascript/answer/provinceCity',
        'seagull2Answer.Phone': './javascript/answer/phone',
        'seagull2Answer.PhoneCheckout': './javascript/answer/phoneCheckout',
        'seagull2Answer.Email': './javascript/answer/email',
        'seagull2Answer.Date': './javascript/answer/date',
        'seagull2Answer.Map': './javascript/answer/map',
        'seagull2Answer.Profession': './javascript/answer/profession',
        'seagull2Answer.Industry': './javascript/answer/industry',
        'seagull2Answer.School': './javascript/answer/school',
        'seagull2Answer.Address': './javascript/answer/address',
    },
    //这个配置是你在引入依赖的时候的包名
    shim: {
        'angular': { exports: 'angular' },
        'angular-cookies': { exports: 'angular-cookies', deps: ['angular'] },
        'angular-ui-router': { deps: ['angular'] },
        'angular-ui-tree': { deps: ['angular', 'css!./node_modules/angular-ui-tree/dist/angular-ui-tree.min'] },
        'angular-datepicker': { deps: ['angular', 'css!./node_modules/datepicker-branch-seagull2/datepicker-branch-seagull2.css'] },
        'ng.ueditor': { deps: ['angular', 'ueditor'] },
        'ueditor': { deps: ['./javascript/ueditor.config', '../ueditor/requirejs-ueditor'] },
        'angular-seagull2-oauth': { deps: ['angular', 'angular-cookies', 'angular-ui-router', 'urijs/uri', 'angular-seagull2-common'] },
        'angular-seagull2-common': {
            deps: [
                'angular',
                'urijs/uri',
                'angular-ui-tree',
                'css!./node_modules/angular-seagull2-common/angular-seagull2-common',
                'ie8css!./node_modules/angular-seagull2-common/angular-seagull2-common.ie8']
        },
        'angular-seagull2-workflow': {
            deps: [
                'angular',
                'urijs/uri',
                'angular-seagull2-common',
                'angular-seagull2-oauth',
                'css!./node_modules/angular-seagull2-workflow/angular-seagull2-workflow',
                'ie8css!./node_modules/angular-seagull2-workflow/angular-seagull2-workflow.ie8']
        },
        'common-filter': { deps: ['angular'] },
        //问卷
        'seagull2Question.QuestionsPaper': { deps: ['./javascript/question/extends'] },
        'seagull2Question.Radio': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.RadioDown'] },
        'seagull2Question.Check': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Radio'] },
        'seagull2Question.Question': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Gapfill': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Matrix': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.MatrixScale': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.MatrixMultipleChoice': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.MatrixMultipleChoices': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.MatrixSlider': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.MatrixComboBox': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.MatrixNumerical': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.MatrixText': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Fileupload': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.RadioDown'] },
        'seagull2Question.Likert': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Check'] },
        'seagull2Question.Ceping': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Check'] },
        'seagull2Question.Cepings': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Check'] },
        'seagull2Question.Ceshi': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Radio'] },
        'seagull2Question.CeshiJudge': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.CeshiMultipleChoices': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Radio'] },
        'seagull2Question.CeshiGapfill': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.CeshiQuestion': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.CheckPermission': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Name': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.CheckBasicMessage': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.VoteRadio': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Radio'] },
        'seagull2Question.VoteCheck': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Radio'] },
        'seagull2Question.Page': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Cut': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Sort': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.RadioDown'] },
        'seagull2Question.Sum': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Slider': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.RadioDown'] },
        'seagull2Question.Qingjing': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Shop': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.RadioDown': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.ClassRadioDown': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Gender': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Radio'] },
        'seagull2Question.Age': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.Radio'] },
        'seagull2Question.Province': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.RadioDown'] },
        'seagull2Question.City': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.ProvinceCity': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Phone': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.PhoneCheckout': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Email': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Date': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Map': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Profession': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.RadioDown'] },
        'seagull2Question.Industry': { deps: ['./javascript/question/extends', './javascript/question/questionBase', 'seagull2Question.RadioDown'] },
        'seagull2Question.School': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'seagull2Question.Address': { deps: ['./javascript/question/extends', './javascript/question/questionBase'] },
        'qrcode': { exports: 'qrcode' },
        'ngTouch': {
            deps: [
                'angular' ]
        },
        //答卷
        'seagull2Answer.AnswerPaper': { deps: ['./javascript/answer/extends'] },
        'seagull2Answer.Radio': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.RadioDown'] },
        'seagull2Answer.Check': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Radio'] },
        'seagull2Answer.Question': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Gapfill': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Matrix': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.MatrixScale': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.MatrixMultipleChoice': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.MatrixMultipleChoices': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.MatrixSlider': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.MatrixComboBox': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.MatrixNumerical': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.MatrixText': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Fileupload': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.RadioDown'] },
        'seagull2Answer.Likert': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Check'] },
        'seagull2Answer.Ceping': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Check'] },
        'seagull2Answer.Cepings': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Check'] },
        'seagull2Answer.Ceshi': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Radio'] },
        'seagull2Answer.CeshiJudge': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.CeshiMultipleChoices': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Radio'] },
        'seagull2Answer.CeshiGapfill': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.CeshiQuestion': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.CheckPermission': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Name': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.CheckBasicMessage': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.VoteRadio': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Radio'] },
        'seagull2Answer.VoteCheck': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Radio'] },
        'seagull2Answer.Page': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Cut': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Sort': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.RadioDown'] },
        'seagull2Answer.Sum': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Slider': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.RadioDown'] },
        'seagull2Answer.Qingjing': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Shop': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.RadioDown': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.ClassRadioDown': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Gender': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Radio'] },
        'seagull2Answer.Age': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.RadioDown'] },
        'seagull2Answer.Province': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.Radio'] },
        'seagull2Answer.City': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.ProvinceCity': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Phone': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.PhoneCheckout': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Email': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Date': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Map': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Profession': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.RadioDown'] },
        'seagull2Answer.Industry': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase', 'seagull2Answer.RadioDown'] },
        'seagull2Answer.School': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'seagull2Answer.Address': { deps: ['./javascript/answer/extends', './javascript/answer/questionBase'] },
        'ng-echarts': {
            deps: [
                'angular',
                'echarts']
        }
    }
});

require(['angular', 'webuploader', 'echarts',
    './javascript/app-routes'],
    function (angular, webuploader, echarts) {
        angular.element(document).ready(function () {
            angular.bootstrap(document, ['app']);
            angular.element(document).find('html').addClass('ng-app');
        });
        window.WebUploader = webuploader;
        window.echarts = echarts;
    });