﻿define(function (require) {
    var app = require('javascript/app');
    var appName = "questionnaireApp";
    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/questionnaires');
        //设计新的调查
        $stateProvider.state('designnew', {
            url: '/designnew/:serialNumber',
            templateUrl: './views/design/designnew.html',
            controller: 'designnew-controller',
            controllerUrl: './javascript/controllers/designnew-controller.js',
            dependencies: ['css!../' + appName + '/css/index.css'],
            requiredLogin: false
        });

        //我的问卷
        $stateProvider.state('questionnaires', {
            url: '/questionnaires',
            templateUrl: './views/my/questionnaires.html',
            controller: 'questionnaires-controller',
            controllerUrl: './javascript/controllers/questionnaires-controller.js',
            dependencies: ['css!../' + appName + '/css/answer.css', 'ie8css!../' + appName + '/css/answer_ie8.css'],
            requiredLogin: false
        });

        //问卷类型选择
        $stateProvider.state('testTyle', {
            url: '/testTyle',
            templateUrl: './views/my/testTyle.html',
            controller: 'questionnaires-controller',
            controllerUrl: './javascript/controllers/questionnaires-controller.js',
            dependencies: ['css!../' + appName + '/css/select.css'],
            requiredLogin: false
        });

        //答题-匿名
        $stateProvider.state('exam-preview', {
            url: '/exam-preview/:serialNumber',
            templateUrl: './views/exam-preview/exam-preview-page.html',
            controller: 'preview-controller',
            controllerUrl: './javascript/controllers/preview-controller.js',
            dependencies: ['css!../' + appName + '/css/answer.css', 'ie8css!../' + appName + '/css/answer_ie8.css'],
            requiredLogin: false
        }); 
        //统计页面
        $stateProvider.state('statistics-sheet', {
            url: '/statistics-sheet/:serialNumber',
            templateUrl: './views/statistics-sheet/questionnaires-statistics.html',
            controller: 'statistics-sheet-controller',
            controllerUrl: './javascript/controllers/statistics-sheet-controller.js',
            dependencies: ['css!../' + appName + '/css/statistics.css'],
            requiredLogin: false
        });
        //答案解析页面
        $stateProvider.state('answerAnalysis', {
            url: '/answerAnalysis/:serialNumber/:userId',
            templateUrl: './views/statistics-sheet/questionnaires-answerAnalysis.html',
            controller: 'answerAnalysis-controller',
            controllerUrl: './javascript/controllers/answerAnalysis-controller.js',
            dependencies: ['css!../' + appName + '/css/select.css'],
            requiredLogin: false
        }); 
        //答案卡
        $stateProvider.state('answerSheet', {
            url: '/answerSheet/:serialNumber/:userId',
            templateUrl: './views/statistics-sheet/questionnaires-answer-sheet.html',
            controller: 'answerAnalysis-controller',
            controllerUrl: './javascript/controllers/answerSheet-controller.js',
            dependencies: ['css!../' + appName + '/css/select.css'],
            requiredLogin: false
        }); 
        //所有答题信息
        $stateProvider.state('questionnaires-answered-details', {
            url: '/answered-details/:serialNumber',
            templateUrl: './views/statistics-sheet/questionnaires-answered-details.html',
            controller: 'answerAnalysis-controller',
            controllerUrl: './javascript/controllers/questionnaires-answered-details-controller.js',
            dependencies: ['css!../' + appName + '/css/select.css'],
            requiredLogin: false
        });
    }]);
});


