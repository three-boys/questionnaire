﻿define(function (require, exports, module) {
  var angular = require('angular');
  var asyncLoader = require('angular-async-loader');

  require('angular-ui-router');
  require('angular-seagull2-common');
  require('ng.ueditor');
  require('angular-seagull2-workflow');
  require('common-filter');
  require('ng-echarts');
  require('angular-datepicker');
  var app = angular.module('app', ['ui.router', 'angular-seagull2-common', 'angular-seagull2-workflow', 'ng.ueditor', 'commonFilter', 'ng-echarts', 'angular-datepicker']);

  // var commonConfig = require('../node_modules/text/text!/THRWebApp/common-config.json');
  var config = require('../node_modules/text/text!config.json');
  app.config(['configureProvider', function (configureProvider) {
    // configureProvider.configure(commonConfig);
    configureProvider.configure(config);
  }]);

  asyncLoader.configure(app);
  module.exports = app;
});
