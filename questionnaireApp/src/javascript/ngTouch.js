(function (window, angular) {
    "use strict";
    angular.module("ngTouch", [])
        .directive('ngTouchstart', [function () {
            return function (scope, element, attr) {

                element.on('touchstart', function (event) {
                    scope.$apply(function () {
                        scope.$eval(attr.ngTouchstart);
                    });
                });
            };
        }]).directive('ngTouchmove', [function () {
            return function (scope, element, attr) {

                element.on('touchmove', function (event) {
                    scope.$apply(function () {
                        scope.$eval(attr.ngTouchmove);
                    });
                });
            };
        }]).directive('ngTouchend', [function () {
            return function (scope, element, attr) {

                element.on('touchend', function (event) {
                    scope.$apply(function () {
                        scope.$eval(attr.ngTouchend);
                    });
                });
            };
        }]);
})(window, window.angular);