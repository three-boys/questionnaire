define(['require'],
    function (require) {
        var app = require('javascript/app');
        var config = {
            ConsequencesUrl: '/StatisticsSheet/Consequences',
            ExcelConsequencesUrl: '/StatisticsSheet/ExcelConsequences',
            AnswerSheetUrl: '/AnswerSheet/QuestionsType',
        };
        app.controller('answerAnalysis-controller', ['$scope', '$http', 'sogModal', 'seagull2Url', '$timeout', 'ModelStateDictionary', 'ValidateHelper', 'sogValidator', '$stateParams', '$state',
            'configure', '$sce'
            , function ($scope, $http, sogModal, seagull2Url, $timeout, ModelStateDictionary, ValidateHelper, sogValidator, $stateParams, $state,
                configure, $sce) {
                if ($stateParams["serialNumber"]) { 
                    LoadConsequences($stateParams["serialNumber"]);
                    QuestionsType($stateParams["serialNumber"]);
                }
                function LoadConsequences(serialNumber) {
                    var url = seagull2Url.getPlatformUrl(config.ConsequencesUrl) + "?serialNumber=" + serialNumber;
                    $scope.loading = true;
                    $http.get(url).success(function (response) {
                        $scope.loading = false;
                        $scope.consequences = response;

                    }).error(function (data, header, config, status) {
                        $scope.loading = false;
                        sogModal.openErrorDialog(data);
                    });
                }
                 //定义链接地址
                $scope.excelUrl=seagull2Url.getPlatformUrl(config.ExcelConsequencesUrl) + "?serialNumber="+$stateParams["serialNumber"];
                
                //加载问卷类型
                 function QuestionsType(serialNumber) {
                    var url = seagull2Url.getPlatformUrl(config.AnswerSheetUrl) + "?serialNumber=" + serialNumber;
                    $http.get(url).success(function (response) {
                        $scope.questionsType = response.type;

                    }).error(function (data, header, config, status) {
                        sogModal.openErrorDialog(data);
                    });
                }
                //答案解析
                $scope.toAsweredSheet = function (creator) {
                    var url = $state.href("answerAnalysis", { serialNumber: $stateParams["serialNumber"], userId: creator });
                    window.open(url, '_blank');
                }
                //跳转到答案解析s
                $scope.answerSheet = function (creator) {
                    var url = $state.href("answerSheet", { serialNumber: $stateParams["serialNumber"], userId: creator });
                    window.open(url, '_blank');
                }

            }]);
    });