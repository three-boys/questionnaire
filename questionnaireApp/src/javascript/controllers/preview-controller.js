define(['require', 'qrcode', 'ngTouch',
	'seagull2Answer.AnswerPaper',
	'seagull2Answer.Radio',
	'seagull2Answer.Check',
	'seagull2Answer.Question',
	'seagull2Answer.Gapfill',
	'seagull2Answer.Matrix',
	'seagull2Answer.MatrixScale',
	'seagull2Answer.MatrixMultipleChoice',
	'seagull2Answer.MatrixMultipleChoices',
	'seagull2Answer.MatrixSlider',
	'seagull2Answer.MatrixComboBox',
	'seagull2Answer.MatrixNumerical',
	'seagull2Answer.MatrixText',
	'seagull2Answer.Fileupload',
	'seagull2Answer.Likert',
	'seagull2Answer.Ceping',
	'seagull2Answer.Cepings',
	'seagull2Answer.Ceshi',
	'seagull2Answer.CeshiJudge',
	'seagull2Answer.CeshiMultipleChoices',
	'seagull2Answer.CeshiGapfill',
	'seagull2Answer.CeshiQuestion',
	'seagull2Answer.CheckPermission',
	'seagull2Answer.Name',
	'seagull2Answer.CheckBasicMessage',
	'seagull2Answer.VoteRadio',
	'seagull2Answer.VoteCheck',
	'seagull2Answer.Page',
	'seagull2Answer.Cut',
	'seagull2Answer.Sort',
	'seagull2Answer.Sum',
	'seagull2Answer.Slider',
	'seagull2Answer.Qingjing',
	'seagull2Answer.Shop',
	'seagull2Answer.RadioDown',
	'seagull2Answer.ClassRadioDown',
	'seagull2Answer.Gender',
	'seagull2Answer.Age',
	'seagull2Answer.Province',
	'seagull2Answer.City',
	'seagull2Answer.ProvinceCity',
	'seagull2Answer.Phone',
	'seagull2Answer.PhoneCheckout',
	'seagull2Answer.Email',
	'seagull2Answer.Date',
	'seagull2Answer.Map',
	'seagull2Answer.Profession',
	'seagull2Answer.Industry',
	'seagull2Answer.School',
	'seagull2Answer.Address',
],
	function (require) {
		var app = require('javascript/app');

		var $Uri;
		if (typeof require === 'function') {
			$Uri = require('urijs/uri');
		} else {
			$Uri = URI;
		}

		var config = {
			LoadQuestionnairesUrl: '/AnswerSheet/Questions',
			DliveryUrl: '/AnswerSheet/Dlivery',
			VoteResultUrl: '/AnswerSheet/LoadVoteResult'
		};
		app.controller('preview-controller', function ($sce, $scope, sogModal, $stateParams, $http, seagull2Url,
			$interval, $location, configure, $state, oAuth, $window, oAuthToken) {
			if ($stateParams["serialNumber"]) {
				LoadData($stateParams["serialNumber"]);
			}
			$scope.trustAsHtml = function (content) {
				return $sce.trustAsHtml(content);
			};

			//当前项目配置信息
			var projectInfo = {
				answeredResidueTimes: -1
			}
			projectInfo = configure.getConfig(projectInfo, 'projectInfo');
			$scope.answeredResidueTimes = projectInfo.answeredResidueTimes;

			$scope.viewAnswer = new window.Seagull2Answer.AnswerPaper();
			//加载投票的结果 
			function LoadVoteResult(questionCodes) {
				$http.get(seagull2Url.getPlatformUrl(config.VoteResultUrl),
					{
						params: {
							questionCodes: questionCodes,
						}
					}).success(function (response) {
						$scope.voteResult = response;
					}).error(function (data, header, config, status) {
					});
			}
			//加载我的问卷 
			function LoadData(serialNumber) {
				$scope.loading = true;
				$http.get(seagull2Url.getPlatformUrl(config.LoadQuestionnairesUrl),
					{
						params: {
							serialNumber: serialNumber,
						}
					}).success(function (response) {
						if (response == "null") {
							$state.go("not-found-page");
						}
						var token = oAuthToken.getToken();
						var isLogin = !!token && token.expires_time > new Date();
						//	return !!token && token.expires_time > new Date();
						// if (!oAuth.isAuthenticated() && !response.isAllowAnonymous) {
						if (!isLogin) {
							// var loginUrl = (oAuth.getOAuthUrl() + "?response_type=" + oAuth.getOAuthModel() + "&redirect_uri=" + encodeURIComponent($location.absUrl())+"&client_id=");
							// $window.location.href = loginUrl; 

							// var data = {
							// 	client_id: oAuth.getClient_id(),
							// 	redirect_uri: $location.absUrl(),
							// 	response_type: 'code'
							// };

							// var redirectUri = new $Uri(oAuth.getOAuthUrl());

							// redirectUri.addQuery(data);

							// $window.location.href = redirectUri;
						}
						if (response && response.loadState === 16 && response.isShowAnswer === false) {
							$state.go("answerSheet", { serialNumber: response.serialNumber });
						}
						if (response && response.loadState === 16 && response.isShowAnswer) {
							$state.go("answerAnalysis", { serialNumber: response.serialNumber });
						}
						$scope.loading = false;
						$scope.loadState = response.loadState;
						$scope.startTime = response.startTime;
						$scope.endTime = response.endTime;
						$scope.isUserShare = response.isUserShare;
						$scope.isShowAnswer = response.isShowAnswer;
						if (response && response.loadState <= 1) {
							$scope.viewAnswer.code = response.code;
							$scope.viewAnswer.type = response.type;
							$scope.viewAnswer.versions = response.versions;
							$scope.viewAnswer.timeLimit = response.timeLimit;
							$scope.viewAnswer.title = response.title;
							$scope.viewAnswer.serialNumber = response.serialNumber;
							$scope.isEndTime = response.isEndTime;
							$scope.isStartTime = response.isStartTime;
							//假数据
							$scope.questionShare = response.questionShare;
							$scope.viewAnswer.summary = response.summary;
							//	console.log(angular.toJson(response));
							if (response.questions && response.questions.length > 0) {
								showCurrentPageQuestionFun(response.questions);
								$scope.viewAnswer.questions = [];
								for (var i = 0; i < response.questions.length; i++) {
									$scope.viewAnswer.addQuestion(getQuestionType(response.questions[i].questionType));
									response.questions[i].optionClass = getQuestionoptionClass(response.questions[i].questionType);
									angular.copy(response.questions[i], $scope.viewAnswer.questions[i]);
								}
							}
							if (response.voteQuestionCodes && response.voteQuestionCodes.length > 0) {
								LoadVoteResult(response.voteQuestionCodes);
							}
						}
					}).error(function (data, header, config, status) {
						$scope.loading = false;
						sogModal.openErrorDialog(data);
					});
			}
			//下一页
			$scope.nextPage = function () {
				$scope.currentPage += 1;
				showCurrentPageQuestionFun($scope.viewAnswer.questions);
			}
			//当前页码
			$scope.currentPage = 1;
			$scope.maxPage = 0;
			//当前页
			$scope.currentPageData = null;
			//当前显示的题目数组
			$scope.showQuestions = [];
			function showCurrentPageQuestionFun(questions) {
				//由于$scope.showQuestions里面保存的是上一次的数据，并不包含此次选中的内容，所以需要转换一下
				var tranferData = [];
				for (var k = 0; k < $scope.showQuestions.length; k++) {
					for (var j = 0; j < questions.length; j++) {
						if ($scope.showQuestions[k].no === questions[j].no && $scope.showQuestions[k].title === questions[j].title) {
							tranferData.push(questions[j]);
						}
					}
				}

				verificationFun(tranferData);
				if ($scope.notPassVerification.length > 0) {
					$scope.currentPage -= 1;
					sogModal.openConfirmDialog("验证未通过", "填写不符合要求");
					return;
				}
				$scope.maxPage = 0;
				$scope.showQuestions = [];
				var flag = 1;
				if (questions) {
					for (var i = 0; i < questions.length; i++) {
						if (questions[i].questionType === "分页") {
							$scope.maxPage += 1;
						}
						if (questions[i].questionType === "分页" && (questions[i].no === $scope.currentPage)) {
							flag = 2;
							$scope.standingMaxTime = questions[i].standingMaxTime;
							$scope.standingMinTime = questions[i].standingMinTime;
							$scope.currentPageData = questions[i];
						}
						if (questions[i].questionType === "分页" && (questions[i].no > $scope.currentPage)) {
							flag = 3;
						}
						if (questions[i].questionType !== "分页" && flag === 2) {
							$scope.showQuestions.push(questions[i]);
						}
					}
				}

				standingMaxTimeFun();
				standingMinTimeFun();
			}
			//当前最大停留时间
			$scope.standingMaxTime = null;
			//本页等待时间计算
			function standingMaxTimeFun() {
				if (!$scope.standingMaxTime) return false;
				$interval(function () {
					if ($scope.standingMaxTime > 0) {
						$scope.standingMaxTime -= 1;

					} else {
						$scope.currentPage += 1;
						showCurrentPageQuestionFun($scope.viewAnswer.questions);
					}
				}, 1000, ($scope.currentPageData.standingMaxTime + 1))
			}
			//本页至少停留时间
			$scope.standingMinTime = null;
			function standingMinTimeFun() {
				if (!$scope.standingMinTime) return false;
				$interval(function () {
					if ($scope.standingMinTime > 0) {
						$scope.standingMinTime -= 1;
					}
				}, 1000, ($scope.currentPageData.standingMinTime + 1))
			}
			function getQuestionoptionClass(qtype) {
				switch (qtype) {
					case "单选题":
						return Seagull2Answer.RadioOption;
					case "多选题":
						return Seagull2Answer.CheckOption;
					case "考试单选题":
						return Seagull2Answer.CeshiOption;
					case "考试多选题":
						return Seagull2Answer.CeshiMultipleChoicesOption;
					case "下拉框单选":
						return Seagull2Answer.RadioDownOption;
					case "评分单选题":
						return Seagull2Answer.CepingOption;
					case "评分多选题":
						return Seagull2Answer.CepingsOption;
					case "投票多选题":
						return Seagull2Answer.VoteCheckOption;
					case "投票单选题":
						return Seagull2Answer.VoteRadioOption;
					case "单项填空":
						return Seagull2Answer.QuestionOption;
					case "矩阵填空":
						return Seagull2Answer.MatrixOption;
					case "量表题":
						return Seagull2Answer.LikertOption;
					case "判断题":
						return Seagull2Answer.CeshiJudge;
					case "考试单项填空题":
						return Seagull2Answer.QuestionOption;
					case "考试多项填空题":
						return Seagull2Answer.QuestionOption;
					case "考试简答":
						return Seagull2Answer.QuestionOption;
					case "考试姓名":
						return Seagull2Answer.QuestionOption;
					case "基本信息":
						return Seagull2Answer.CheckBasicMessageOption;
					case "分页":
						return Seagull2Answer.Page;
					case "段落说明":
						return Seagull2Answer.Cut;
					case "滚动条":
						return Seagull2Answer.Slider;
					case "排序":
						return Seagull2Answer.SortOption;
					case "上传文件":
						return Seagull2Answer.Fileupload;
					case "手机":
						return Seagull2Answer.QuestionOption;
					default:
						return '';

				}
			}
			function getQuestionType(qtype) {
				switch (qtype) {
					case "单选题":
						return 'radio';
					case "多选题":
						return 'check';
					case "考试单选题":
						return 'ceshi';
					case "考试多选题":
						return 'ceshiMultipleChoices';
					case "下拉框单选":
						return 'radioDown';
					case "评分单选题":
						return 'ceping';
					case "评分多选题":
						return 'cepings';
					case "投票多选题":
						return 'voteCheck';
					case "投票单选题":
						return 'voteRadio';
					case "单项填空":
						return 'question';
					case "矩阵填空":
						return 'matrix';
					case "量表题":
						return 'likert';
					case "判断题":
						return 'ceshiJudge';
					case "考试单项填空题":
						return 'ceshiQuestion';
					case "考试多项填空题":
						return 'ceshiGapfill';
					case "考试简答":
						return 'checkPermission';
					case "考试姓名":
						return 'name';
					case "基本信息":
						return 'checkBasicMessage';
					case "分页":
						return 'page';
					case "段落说明":
						return 'cut';
					case "滚动条":
						return 'slider';
					case "排序":
						return 'sort';
					case "上传文件":
						return 'fileupload';
					case "手机":
						return 'phone';
					case "Email":
						return 'email';
					case '性别':
						return 'gender';
					case '年龄段':
						return 'age';
					case '省份':
						return 'province';
					case '职业':
						return 'profession';
					case '行业':
						return 'industry';
					default:
						return '';

				}
			}
			//验证不通过的题目数组
			$scope.notPassVerification = [];
			//验证方法
			function verificationFun(questions) {
				$scope.notPassVerification = [];
				if (questions) {
					for (var i = 0; i < questions.length; i++) {
						//
						if (questions[i].questionType !== "分页" && (questions[i].required === true)) {
							if (!verificationRequired(questions[i])) {
								$scope.notPassVerification.push(questions[i]);
							}
						}
						if (questions[i].questionType === "手机" || questions[i].questionType === "Email" || questions[i].questionType === "单项填空") {
							if (!verificationPhone(questions[i])) {
								$scope.notPassVerification.push(questions[i]);
							}
						}

					}
				}
			}
			//非空验证
			function verificationRequired(question) {
				var flag = false;
				if (question.questionForm === 'choiceQuestion') {
					for (var i = 0; i < question.options.length; i++) {
						if (question.options[i].answerOption === true) {
							flag = true;
						}
					}
				}
				if (question.questionForm === 'nonselective') {
					for (var i = 0; i < question.options.length; i++) {
						if (question.options[i].answerOption && question.options[i].answerOption.length > 0) {
							flag = true;
						}
					}
				}
				return flag;
			}

			//手机号验证,Email验证，填空
			function verificationPhone(question) {
				for (var i = 0; i < question.options.length; i++) {
					if (question.options[i].verificationBy === true || question.options[i].questionType === null) {
						return true;
					}
				}
				return false;
			}

			// $scope.result = null;
			$scope.commit = function (data) {
				verificationFun(data.questions);
				commitData(data);
			}
			//提交问卷
			function commitData(data) {
				if ($scope.notPassVerification.length === 0) {
					$http.post(seagull2Url.getPlatformUrl(config.DliveryUrl), data).success(function (response) {
						$scope.loading = false;
						sogModal.openAlertDialog('提示！', "保存完成！");
						if (!$scope.isShowAnswer) {
							$state.go("answerSheet", { serialNumber: $scope.viewAnswer.serialNumber });
						}
						if ($scope.isShowAnswer) {
							$state.go("answerAnalysis", { serialNumber: $scope.viewAnswer.serialNumber });
						}

					}).error(function (data, header, config, status) {
						$scope.loading = false;
						sogModal.openErrorDialog(data);
					});
				} else {
					sogModal.openConfirmDialog("验证未通过", "填写不符合要求,请重新填写。");
				}
			}
			//分享二维码 
			$scope.share = function () {
				$scope.IsShareQrContainer = true;
				$scope.shareUrl = $location.absUrl();
				document.getElementById("shareQrContainer").innerHTML = "";
				var qrcode = new QRCode(document.getElementById("shareQrContainer"), {
					width: 250,
					height: 250
				});
				qrcode.clear();
				qrcode.makeCode($location.absUrl());
			}
			$scope.copyShareUrl = function () {
				var errorInput = document.getElementById("shareQrId");
				errorInput.select();
				try {
					document.execCommand("Copy");
					sogModal.openAlertDialog("复制成功", "复制成功");
				} catch (e) {
					sogModal.openAlertDialog("友情提示", "复制失败, 请选中内容手动复制。");
				}
			}
			//答题时间（单位秒）
			$scope.answeredTimes = 0;
			$scope.checkLast = function ($last) {
				if ($last) {
					answeredTimesFun();
				}
			}
			function answeredTimesFun() {
				$interval(function () {
					$scope.answeredTimes += 1;
					//答题限制时间结束，程序自动提交答卷
					if (($scope.viewAnswer.timeLimit - $scope.answeredTimes) == 0) {
						commitData($scope.viewAnswer);
					}
				}, 1000)
			}
		});
	});

