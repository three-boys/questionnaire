define(['require'],
    function (require) {
        var app = require('javascript/app');
        var config = {
            ShowAnswerAnalysisUrl: '/StatisticsSheet/ShowAnswerAnalysis',
            LoadQuestionnairesUrl: '/AnswerSheet/Questions',
            LoadAnswerUrl: '/StatisticsSheet/LoadAnswer'
        };
        app.controller('answerAnalysis-controller', function ($scope, $http, oAuth, sogModal, seagull2Url, $timeout, ModelStateDictionary, ValidateHelper, sogValidator, $stateParams, $state,
            configure, $sce) {
            $scope.chartConfig = {
                dataLoaded: true
            };
            if ($stateParams["serialNumber"]) {
                // checkIsAuthenticated($stateParams["serialNumber"]);
                ShowAnswerAnalysis($stateParams["serialNumber"]);
                LoadAnswer($stateParams["serialNumber"]);
            }
            $scope.selectTitle = function (content) {
                if (content) {
                    return content.replace(/<[^>]+>/g, "");//去掉所有的html标记
                }
            };
            $scope.trustAsHtml = function (content) {
                return $sce.trustAsHtml(content);
            };
            function ShowAnswerAnalysis(serialNumber) {
                var url = seagull2Url.getPlatformUrl(config.ShowAnswerAnalysisUrl) + "?serialNumber=" + serialNumber;
                if ($stateParams["userId"]) {
                    url += ("&userId=" + $stateParams["userId"]);
                }
                $http.get(url).success(function (response) {
                    $scope.data = response;
                    amountfun(response);
                }).error(function (data, header, config, status) {
                    sogModal.openErrorDialog(data);
                });
            }
            $scope.amount = 0;
            $scope.questionCount = 0;
            function amountfun(data) {
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $scope.amount += item.score;
                    $scope.questionCount += 1;
                }
            }
            //加载我的问卷 
            function checkIsAuthenticated(serialNumber) {
                $http.get(seagull2Url.getPlatformUrl(config.LoadQuestionnairesUrl),
                    {
                        params: {
                            serialNumber: serialNumber,
                        }
                    }).success(function (response) {
                        if (!oAuth.isAuthenticated() && !response.isAllowAnonymous) {
                            $state.go('answerAnalysis-auth', { serialNumber: serialNumber })
                        } else {
                            ShowAnswerAnalysis($stateParams["serialNumber"]);
                            LoadAnswer($stateParams["serialNumber"]);
                        }
                    }).error(function (data, header, config, status) {
                    });
            }
            //
            function LoadAnswer(serialNumber) {
                var url = seagull2Url.getPlatformUrl(config.LoadAnswerUrl) + "?serialNumber=" + serialNumber;
                if ($stateParams["userId"]) {
                    url += ("&userId=" + $stateParams["userId"]);
                }
                $http.get(url).success(function (response) {
                    $scope.answerData = response;

                }).error(function (data, header, config, status) {
                    sogModal.openErrorDialog(data);
                });
            }
            //

        });
    });