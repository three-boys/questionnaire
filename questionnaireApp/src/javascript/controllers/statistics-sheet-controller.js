define(['require'],
    function (require) {
        var app = require('javascript/app');
        var config = {
            LoadQuestionStatisticsUrl: '/StatisticsSheet/LoadQuestionStatistics',
            ExcelUrl: '/StatisticsSheet/Excel',
            DetailsPageUrl: '/StatisticsSheet/DetailsPage',
        };
        app.controller('statistics-sheet-controller', ['$scope', '$http', 'sogModal', 'seagull2Url', '$timeout', 'ModelStateDictionary', 'ValidateHelper', 'sogValidator', '$stateParams', '$state',
            'configure', '$sce'
            , function ($scope, $http, sogModal, seagull2Url, $timeout, ModelStateDictionary, ValidateHelper, sogValidator, $stateParams, $state,
                configure, $sce) {
                $scope.chartConfig = {
                    dataLoaded: false
                };
                if ($stateParams["serialNumber"]) {
                    LoadQuestionStatistics($stateParams["serialNumber"]);
                }
                if ($stateParams["code"]) {
                    LoadQuestionStatistics($stateParams["code"]);
                }
                $scope.trustAsHtml = function (content) {
                    return $sce.trustAsHtml(content);
                };
                $scope.showPieChart = function (question) {
                    $scope.currentPieQuestion = question.no;
                    $scope.chartConfig.dataLoaded = true;
                }
                $scope.selectTitle = function (content) {
                    if (content) {
                        return content.replace(/<[^>]+>/g, "");//去掉所有的html标记
                    }
                };
                //定义链接地址
                $scope.excelUrl = seagull2Url.getPlatformUrl(config.ExcelUrl) + "?serialNumber=" + $stateParams["serialNumber"];
                //当前题目用户饼图
                $scope.currentPieQuestion = null;
                function LoadQuestionStatistics(serialNumber) {
                    $scope.loading = true;
                    $http.get(seagull2Url.getPlatformUrl(config.LoadQuestionStatisticsUrl) + "?serialNumber=" + serialNumber).success(function (response) {
                        $scope.loading = false;
                        if (response === "null") {
                            $scope.dataIsNull = true;
                            return;
                        }
                        $scope.data = response;
                        for (var j = 0; j < response.length; j++) {
                            if (response[j].questionType != "分页") {
                                if ($scope.currentPieQuestion == null) {
                                    $scope.currentPieQuestion = response[j].no;
                                }
                                var values = [];
                                var legendData = [];

                                for (var m = 0; m < response[j].options.length; m++) {
                                    //饼状图结构
                                    var single = {
                                        value: response[j].options[m].answerAmount,
                                        name: response[j].options[m].title
                                    }
                                    legendData.push(response[j].options[m].title);
                                    values.push(single);
                                }
                                var data = {
                                    no: response[j].no, values: getOption(response[j].title.replace(/<[^>]+>/g, ""), values, legendData)
                                }
                                $scope.charts.push(data);
                            }
                        }

                    }).error(function (data, header, config, status) {
                        $scope.loading = false;
                        sogModal.openErrorDialog(data);
                    });
                }
                $scope.showPie = function (question) {
                    $scope.currentTableQuestion = question.no;
                }
                $scope.charts = [];
                function getOption(chartsTitle, chartsData, legendData) {
                    return {
                        title: {
                            text: chartsTitle,
                            x: 'center'
                        },
                        tooltip: {
                            trigger: 'item',
                            formatter: "{a} <br/>{b} : {c} ({d}%)"
                        },
                        legend: {
                            orient: 'vertical',
                            left: 'left',
                            data: legendData
                        },
                        series: [
                            {
                                name: '数据来自问卷调查',
                                type: 'pie',
                                radius: '55%',
                                center: ['50%', '60%'],
                                data: chartsData,
                                itemStyle: {
                                    emphasis: {
                                        shadowBlur: 10,
                                        shadowOffsetX: 0,
                                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                                    }
                                }
                            }
                        ]
                    };
                }
                //配置分页基本参数
                $scope.pagination = {
                    currentPage: 1,
                    itemsPerPage: 10,
                    randomNum: $stateParams["random"] ? $stateParams["random"] : 0
                };
                //定义全局code
                var pageCode = null;
                function DetailsPage() {

                    if (pageCode == null) {
                        return;
                    }
                    $http.get(seagull2Url.getPlatformUrl(config.DetailsPageUrl) + "?code=" + pageCode,
                        {
                            params: {
                                PageIndex: $scope.pagination.currentPage,
                                PageSize: $scope.pagination.itemsPerPage
                            }
                        }).success(function (response) {
                            $scope.dataPage = response.data;
                            $scope.pagination.totalItems = response.totalItems;
                        }).error(function (data, header, config, status) {
                            $scope.loading = false;
                            sogModal.openErrorDialog(data);
                        });
                }
                //详情弹出框
                $scope.detailsDialog = function (obj, index, code) {
                    var addr = "views/statistics-sheet/questionnaires-details.html";

                    var template = "<div ng-include=\"\'" + addr + "\'\"></div>";
                    pageCode = code;

                    var promise = sogModal.openDialog(template, '题目详情：', null, $scope, {
                        obj,
                        index,
                        code
                    }, {
                            containerStyle: {
                                width: '100%;'
                            }
                        });
                    promise.then(function () { });
                    DetailsPage();
                };
                $scope.$watch('pagination.currentPage + pagination.itemsPerPage + pagination.randomNum',
                    DetailsPage);
                //跳转到答题卡
                $scope.answerSheet = function (creator) {
                    $state.go("answerSheet", { serialNumber: $stateParams["serialNumber"], userId: creator });
                }
            }]);
    });