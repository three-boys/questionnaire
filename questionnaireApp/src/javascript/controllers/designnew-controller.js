define(['require',
	'seagull2Question.QuestionsPaper',
	'seagull2Question.Radio',
	'seagull2Question.Check',
	'seagull2Question.Question',
	'seagull2Question.Gapfill',
	'seagull2Question.Matrix',
	'seagull2Question.MatrixScale',
	'seagull2Question.MatrixMultipleChoice',
	'seagull2Question.MatrixMultipleChoices',
	'seagull2Question.MatrixSlider',
	'seagull2Question.MatrixComboBox',
	'seagull2Question.MatrixNumerical',
	'seagull2Question.MatrixText',
	'seagull2Question.Fileupload',
	'seagull2Question.Likert',
	'seagull2Question.Ceping',
	'seagull2Question.Cepings',
	'seagull2Question.Ceshi',
	'seagull2Question.CeshiJudge',
	'seagull2Question.CeshiMultipleChoices',
	'seagull2Question.CeshiGapfill',
	'seagull2Question.CeshiQuestion',
	'seagull2Question.CheckPermission',
	'seagull2Question.Name',
	'seagull2Question.CheckBasicMessage',
	'seagull2Question.VoteRadio',
	'seagull2Question.VoteCheck',
	'seagull2Question.Page',
	'seagull2Question.Cut',
	'seagull2Question.Sort',
	'seagull2Question.Sum',
	'seagull2Question.Slider',
	'seagull2Question.Qingjing',
	'seagull2Question.Shop',
	'seagull2Question.RadioDown',
	'seagull2Question.ClassRadioDown',
	'seagull2Question.Gender',
	'seagull2Question.Age',
	'seagull2Question.Province',
	'seagull2Question.City',
	'seagull2Question.ProvinceCity',
	'seagull2Question.Phone',
	'seagull2Question.PhoneCheckout',
	'seagull2Question.Email',
	'seagull2Question.Date',
	'seagull2Question.Map',
	'seagull2Question.Profession',
	'seagull2Question.Industry',
	'seagull2Question.School',
	'seagull2Question.Address',
],
	function (require) {
		var app = require('javascript/app');
		var config = {
			LoadQuestionnairesUrl: '/Questionnaire/Load',
			AddOrUpdateUrl: '/Questionnaire/AddOrUpdate'
		};
		var operationType = {
			Add: 'Add',
			Delete: 'Delete',
			Update: 'Update', 
			Copy: 'Copy'
		}
		app.controller('designnew-controller', ['$sce', '$scope', 'sogModal', '$stateParams', '$http', 'seagull2Url', '$state',
			function ($sce, $scope, sogModal, $stateParams, $http, seagull2Url, $state) {
				$scope.viewModel = new window.Seagull2Question.QuestionsPaper();
				var currentNumber = $stateParams["serialNumber"];
				if (currentNumber) {
					LoadData(currentNumber);
				}

				$scope.trustAsHtml = function (content) {
					return $sce.trustAsHtml(content);
				};
				$scope.selectTitle = function (content) {
					if (content) {
						return content.replace(/<[^>]+>/g, "");//去掉所有的html标记
					}
				};
				$scope.completed = function () {
					$scope.ISdisabled = false;
					$scope.ISdisabled = !$scope.ISdisabled;
					UpdateData($scope.viewModel, operationType.Update, true);
					//$state.go("questionnaires");
				};
				$scope.save = function () {
					//console.log(angular.toJson($scope.viewModel));
					$scope.ISdisabled = false;
					$scope.ISdisabled = !$scope.ISdisabled; 
					UpdateData($scope.viewModel, operationType.Update);
				};
				function getYear(dayObj) {
					if (!dayObj) return false;
					return (dayObj.getYear() < 1900) ? (1900 + dayObj.getYear()) : dayObj.getYear();
				}
				$scope.editInModalUeditor = function (obj, prototypeName, title) {
					var template = '<div class="ueditor" style="width: 99.9%; height: 300px;" ng-model="content"></div>' +
						' <div class="modal-footer">' +
						'<button type="button" class="btn btn-primary" ng-click="confirm(content)">确定</button>' +
						'</div>';
					var promise = sogModal.openDialog(template, title || '请输入', null, $scope, {
						content: obj[prototypeName]
					}, null, null);

					promise.then(function (content) {
						obj[prototypeName] = content;
					});
				};

				//关联逻辑
				$scope.relevanceDialog = function (obj, index) {
					var addr="views/design/share/popupWindow.html";
					var template = "<div ng-include=\"\'"+addr+ "\'\"></div>";
					var promise = sogModal.openDialog(template, '创建关联逻辑：', null, $scope, {
						obj,
						index
					}, {
							containerStyle: {
								width: '50%;'
							}
						});
					promise.then(function () { });
				};
				$scope.addInput = function (question) {
					question.addOption();
				}

				//跳转到预览页面
                $scope.toPreview = function (serialNumber) {
                    var url = $state.href("exam-preview", { serialNumber: serialNumber });
                    window.open(url, '_blank');
                }

				//答案设置
				$scope.theanswerset = function (obj, index) {
					var addrtheans="views/design/share/theanswerset.html";
					var template = "<div ng-include=\"\'"+addrtheans+"\'\"></div>";
					var promise = sogModal.openDialog(template, '答案设置：', null, $scope, {
						obj,
						index
					}, {
							containerStyle: {
								width: '50%;'
							}
						});
					promise.then(function () { });
				};
				//加载我的问卷
				function LoadData(serialNumber) {
					$scope.loading = true;
					$http.get(seagull2Url.getPlatformUrl(config.LoadQuestionnairesUrl), {
						params: {
							serialNumber: serialNumber,
						}
					}).success(function (response) {
						//	console.log(angular.toJson(response));
						$scope.loading = false;
						if (response && response.isAlterAuthority === false) {
							$state.go("not-found-page");
						}
						$scope.viewModel.title = response.title;
						$scope.viewModel.serialNumber = response.serialNumber;
						$scope.viewModel.summary = response.summary;
						$scope.viewModel.timeLimit = response.timeLimit;
						$scope.viewModel.isStartTime = response.isStartTime; 
						$scope.viewModel.isAllowAnonymous = response.isAllowAnonymous;
						$scope.viewModel.isShowAnswer = response.isShowAnswer;
						$scope.viewModel.isUserShare = response.isUserShare;
						// //假数据
						// $scope.questionShare = response.questionShare;
						$scope.viewModel.isEndTime = response.isEndTime;
						$scope.viewModel.startTime = response.startTime;
						$scope.viewModel.startHour = response.startHour;
						$scope.viewModel.startMinute = response.startMinute;
						$scope.viewModel.endTime = response.endTime;
						$scope.viewModel.endHour = response.endHour;
						$scope.viewModel.endMinute = response.endMinute;
						if (response.questions && response.questions.length == 0) {
							$scope.viewModel.addQuestion('page');
						} else if (response.questions && response.questions.length > 0) {
							$scope.viewModel.questions = [];
							for (var i = 0; i < response.questions.length; i++) {
								$scope.viewModel.addQuestion(getQuestionType(response.questions[i].questionType));
								response.questions[i].optionClass = getQuestionoptionClass(response.questions[i].questionType);
								angular.copy(response.questions[i], $scope.viewModel.questions[i]);
							}
						}

					}).error(function (data, header, config, status) {
						sogModal.openErrorDialog(data);
					});
				}
				function getQuestionoptionClass(qtype) {
					switch (qtype) {
						case "单选题":
							return Seagull2Question.RadioOption;
						case "多选题":
							return Seagull2Question.CheckOption;
						case "考试单选题":
							return Seagull2Question.CeshiOption;
						case "考试多选题":
							return Seagull2Question.CeshiMultipleChoicesOption;
						case "下拉框单选":
							return Seagull2Question.RadioDownOption;
						case "评分单选题":
							return Seagull2Question.CepingOption;
						case "评分多选题":
							return Seagull2Question.CepingsOption;
						case "投票多选题":
							return Seagull2Question.VoteCheckOption;
						case "投票单选题":
							return Seagull2Question.VoteRadioOption;
						case "单项填空":
							return Seagull2Question.QuestionOption;
						case "矩阵填空":
							return Seagull2Question.MatrixOption;
						case "量表题":
							return Seagull2Question.LikertOption;
						case "判断题":
							return Seagull2Question.CeshiJudge;
						case "考试单项填空题":
							return Seagull2Question.QuestionOption;
						case "考试多项填空题":
							return Seagull2Question.QuestionOption;
						case "考试简答":
							return Seagull2Question.QuestionOption;
						case "考试姓名":
							return Seagull2Question.QuestionOption;
						case "基本信息":
							return Seagull2Question.CheckBasicMessageOption;
						case "分页":
							return Seagull2Question.Page;
						case "段落说明":
							return Seagull2Question.Cut;
						case "滚动条":
							return Seagull2Question.Slider;
						case "排序":
							return Seagull2Question.SortOption;
						case "上传文件":
							return Seagull2Question.Fileupload;
						case "手机":
							return Seagull2Question.QuestionOption;
						case "Email":
							return Seagull2Question.QuestionOption;
						default:
							return '';

					}
				}
				function getQuestionType(qtype) {
					switch (qtype) {
						case "单选题":
							return 'radio';
						case "多选题":
							return 'check';
						case "考试单选题":
							return 'ceshi';
						case "考试多选题":
							return 'ceshiMultipleChoices';
						case "下拉框单选":
							return 'radioDown';
						case "评分单选题":
							return 'ceping';
						case "评分多选题":
							return 'cepings';
						case "投票多选题":
							return 'voteCheck';
						case "投票单选题":
							return 'voteRadio';
						case "单项填空":
							return 'question';
						case "矩阵填空":
							return 'matrix';
						case "量表题":
							return 'likert';
						case "判断题":
							return 'ceshiJudge';
						case "考试单项填空题":
							return 'ceshiQuestion';
						case "考试多项填空题":
							return 'ceshiGapfill';
						case "考试简答":
							return 'checkPermission';
						case "考试姓名":
							return 'name';
						case "基本信息":
							return 'checkBasicMessage';
						case "分页":
							return 'page';
						case "段落说明":
							return 'cut';
						case "滚动条":
							return 'slider';
						case "排序":
							return 'sort';
						case "上传文件":
							return 'fileupload';
						case "手机":
							return 'phone';
						case "Email":
							return 'email';
						case '性别':
							return 'gender';
						case '年龄段':
							return 'age';
						case '省份':
							return 'province';
						case '职业':
							return 'profession';
						case '行业':
							return 'industry';
						default:
							return '';

					}
				}
				//修改方法
				function UpdateData(params, operation, IsSave = false) {
					// alert(params.startTime + "|||" + params.endTime);
					$scope.loading = true;
					$http.post(seagull2Url.getPlatformUrl(config.AddOrUpdateUrl) + "?operationType=" + operation, params).success(function (response) {
						switch (operation) {
							case operationType.Update:
								$scope.loading = false;
								//sogModal.openAlertDialog('提示！', "保存完成！");
								break;
							default:
								break;
						}
						if (IsSave) {
							var promise = sogModal.openAlertDialog("提示！", "保存完成！");
							promise.then(function (v) {
								$state.go("questionnaires");
							}, function (v) {
							});

						}
						else {
							sogModal.openAlertDialog('提示！', "保存完成！");
							$scope.ISdisabled = false;
							$scope.ISdisabled = $scope.ISdisabled;
						}
						//LoadData(currentNumber);

					}).error(function (data, header, config, status) {
						$scope.loading = false;
						$scope.ISdisabled = false;
						$scope.ISdisabled = $scope.ISdisabled;
						sogModal.openErrorDialog(data);
					});
				}

				$scope.timeSetting = function () {
					var timeaddr="views/design/share/time-setting.html";
					var template = "<div ng-include=\"\'"+timeaddr+"\'\"></div>";
					var promise = sogModal.openDialog(template, '问卷设置：', null, $scope, null, {
						containerStyle: {
							width: '50%;'
						}
					});
					promise.then(function () { });
				}
				//问卷标题、问卷说明编辑
				$scope.editingTitle = false;

				$scope.pictureExtensions = [
					{ name: " 图片文件", values: ['.gif', '.png', '.jpg', '.jpeg', '.bmp'] },
					{ name: " 文档文件", values: ['.doc', '.docx', '.pdf', '.xls', '.xlsx', '.ppt', '.pptx', '.txt'] },
					{ name: " 压缩文件", values: ['.rar', '.zip', '.gzip'] }
				];
			}
		]);
	});
