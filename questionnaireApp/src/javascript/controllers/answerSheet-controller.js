define(['require'],
    function (require) {
        var app = require('javascript/app');
        var config = {
            AnswerSheetUrl: '/StatisticsSheet/answerSheet',
            LoadAnswerUrl: '/StatisticsSheet/LoadAnswer',
            GetFilesUrl: '/StatisticsSheet/GetFiles',
            LoadQuestionnairesUrl: '/AnswerSheet/Questions',
        };
        app.controller('answerAnalysis-controller', function ($scope, $http, sogModal, oAuth, seagull2Url, $timeout, ModelStateDictionary, ValidateHelper, sogValidator, $stateParams, $state,
            configure, $sce) {
            $scope.chartConfig = {
                dataLoaded: true
            };
            if ($stateParams["serialNumber"]) {
                // checkIsAuthenticated($stateParams["serialNumber"]);
                AnswerSheet($stateParams["serialNumber"]);
                LoadAnswer($stateParams["serialNumber"]);
            }
            $scope.trustAsHtml = function (content) {
                return $sce.trustAsHtml(content);
            };


            //加载我的问卷 
            function checkIsAuthenticated(serialNumber) {
                $http.get(seagull2Url.getPlatformUrl(config.LoadQuestionnairesUrl),
                    {
                        params: {
                            serialNumber: serialNumber,
                        }
                    }).success(function (response) {
                        if (!oAuth.isAuthenticated() && !response.isAllowAnonymous) {
                            $state.go('answerSheet-auth', { serialNumber: serialNumber })
                        } else {
                            AnswerSheet($stateParams["serialNumber"]);
                            LoadAnswer($stateParams["serialNumber"]);
                        }
                    }).error(function (data, header, config, status) {
                    });
            }

            //
            $scope.loadFiles = function (resourceId) {
                var url = (seagull2Url.getPlatformUrl(config.GetFilesUrl) + "?resourceId=" + resourceId);
                var params = { resourceId: resourceId };
                $http.get(url, { cache: false }).success(function (response) {
                    $scope.files = response;
                }).error(function (data, header, config, status) {
                    sogModal.openErrorDialog(data);
                });
            };

            //
            function AnswerSheet(serialNumber) {
                var url = seagull2Url.getPlatformUrl(config.AnswerSheetUrl) + "?serialNumber=" + serialNumber;
                if ($stateParams["userId"]) {
                    url += ("&userId=" + $stateParams["userId"]);
                }
                $http.get(url).success(function (response) {
                    $scope.data = response;
                    amountfun(response);
                }).error(function (data, header, config, status) {
                    sogModal.openErrorDialog(data);
                });
            }
            $scope.selectTitle = function (content) {
                if (content) {
                    return content.replace(/<[^>]+>/g, "");//去掉所有的html标记
                }
            };
            $scope.amount = 0;
            function amountfun(data) {

                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $scope.amount += item.score;
                }
            }
            //
            function LoadAnswer(serialNumber) {
                var url = seagull2Url.getPlatformUrl(config.LoadAnswerUrl) + "?serialNumber=" + serialNumber;
                if ($stateParams["userId"]) {
                    url += ("&userId=" + $stateParams["userId"]);
                }
                $http.get(url).success(function (response) {
                    $scope.answerData = response;

                }).error(function (data, header, config, status) {
                    sogModal.openErrorDialog(data);
                });
            }
            //

        });
    });