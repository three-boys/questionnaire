define(['require', 'qrcode'],
    function (require) {
        var app = require('javascript/app');

        var config = {
            LoadQuestionnairesUrl: '/Questionnaire/Load',
            AddOrUpdateUrl: '/Questionnaire/AddOrUpdate',
            SetQuestionnairesStatusUrl: '/Questionnaire/SetQuestionnairesStatus',
            CheckConsequenceCountUrl: '/StatisticsSheet/CheckConsequenceCount'
        };
        var operationType = {
            Add: 'Add',
            Delete: 'Delete',
            Update: 'Update',
            Copy: 'Copy',
            UpdateStatus: 'UpdateStatus'
        }
        app.controller('questionnaires-controller', function ($scope, $http, sogModal, seagull2Url, $timeout, ModelStateDictionary, ValidateHelper, sogValidator, $stateParams, $state,
                configure, $sce) {
                //配置分页基本参数
                $scope.pagination = {
                    currentPage: 1,
                    itemsPerPage: 10,
                    randomNum: $stateParams["random"] ? $stateParams["random"] : 0
                };
                //加载我的问卷
                LoadData();

                //弹出窗口
                $scope.openDialog = function (questionnaireType) {
                    $scope.type = questionnaireType;
                    var tempaddr="views/my/popupWindow.html";
                    var template = "<div ng-include=\"\'"+tempaddr+"\'\"></div>";
                    var promise = sogModal.openDialog(template, '创建考试问卷：', null, $scope, { containerStyle: { width: '50%;' } },
                        function (data, defer) {
                            if (!data || !data.title || data.title.length < 1) {
                                var message = {
                                    "message": "验证错误。",
                                    "modelState": {
                                        "问卷名称": ["问卷名称不能为空。"]
                                    }
                                };
                                sogModal.openBadRequestDialog(message);
                                return;
                            }
                            UpdateData(data, operationType.Add);
                        });
                };

                //修改
                $scope.updateStatus = function (params, newStatus) {

                    if (newStatus === 4) {
                        var promise = sogModal.openConfirmDialog("是否继续？", '状态设为"停止"后将不能填写，是否继续？');
                        promise.then(function (v) {
                            params.status = newStatus;
                            UpdateData(params, operationType.UpdateStatus);
                        }, function (v) {
                        });
                    } else {
                        params.status = newStatus;
                        UpdateData(params, operationType.UpdateStatus);
                    }

                }
                //删除
                $scope.deleteStatus = function (params) {
                    var promise = sogModal.openConfirmDialog("提示！", "确定删除？");
                    promise.then(function (v) {
                        UpdateData(params, operationType.Delete);
                    }, function (v) {
                    });

                }
                //复制
                $scope.copyData = function (params) {
                    UpdateData(params, operationType.Copy);
                }
                $scope.consequenceCount = function (item) {
                    $http.get(seagull2Url.getPlatformUrl(config.CheckConsequenceCountUrl) + "?serialNumber=" + item.serialNumber).success(function (response) {
                        item.answeredCount = response;
                    }).error(function (data, header, config, status) {
                    });
                }
                //加载我的问卷
                function LoadData() {
                    $scope.loading = true;
                    $scope.data = [];
                    $http.get(seagull2Url.getPlatformUrl(config.LoadQuestionnairesUrl),
                        {
                            params: {
                                PageIndex: $scope.pagination.currentPage,
                                PageSize: $scope.pagination.itemsPerPage
                            }
                        }).success(function (response) {
                            $scope.loading = false;
                            $scope.data = response.data;
                            $scope.pagination.totalItems = response.totalItems;
                        }).error(function (data, header, config, status) {
                            $scope.loading = false;
                            sogModal.openErrorDialog(data);
                        });
                }
                //修改方法
                function UpdateData(params, operation) {
                    if (operationType.UpdateStatus !== operation) {
                        $scope.loading = true;
                    }
                    $http.post(seagull2Url.getPlatformUrl(config.AddOrUpdateUrl) + "?operationType=" + operation, params).success(function (response) {
                        switch (operation) {
                            case operationType.Add:
                                $state.go("designnew", { serialNumber: response.serialNumber });
                                break;
                            case operationType.Delete:
                                $scope.loading = false;
                                LoadData();
                                sogModal.openAlertDialog('提示！', "删除完成！");
                                break;
                            case operationType.Update:
                                LoadData();
                                sogModal.openAlertDialog('提示！', "保存完成！");
                                break;
                            case operationType.Copy:
                                LoadData();
                                sogModal.openAlertDialog('提示！', "复制完成！");
                                break;
                            case operationType.UpdateStatus:
                                if (params.status === 2) {
                                    sogModal.openAlertDialog('问卷成功运行!', "问卷成功运行!   (点击'分享'按钮可分享此问卷)");
                                }
                                break;
                            default: LoadData(); break;
                        }
                    }).error(function (data, header, config, status) {
                        sogModal.openErrorDialog(data);
                    });
                }

                $scope.$watch('pagination.currentPage + pagination.itemsPerPage + pagination.randomNum',
                    LoadData);
                //跳转到设计页面
                $scope.toDesign = function (serialNumber) {
                    var url = $state.href("designnew", { serialNumber: serialNumber });
                    window.open(url, '_blank');
                }
                //跳转到预览页面 
                $scope.toPreview = function (serialNumber) {
                    var url = $state.href("exam-preview", { serialNumber: serialNumber });
                    window.open(url, '_blank');
                }
                //
                $scope.toTestTyle = function () {
                    $state.go("testTyle");
                }
                //跳转到统计页面
                $scope.toStatistics = function (serialNumber) {
                    var url = $state.href("statistics-sheet", { serialNumber: serialNumber });
                    window.open(url, '_blank');
                }
                //跳转到答案解析
                $scope.answeredDetails = function (serialNumber) {
                    var url = $state.href("questionnaires-answered-details", { serialNumber: serialNumber });
                    window.open(url, '_blank');
                }

                //当前项目配置信息
                var projectInfo = {
                    questionnaireUrl: "",
                    webUrlBase: ""
                }
                projectInfo = configure.getConfig(projectInfo, 'projectInfo');
                $scope.shareBaseUrl = projectInfo.webUrlBase + projectInfo.questionnaireUrl;

                $scope.share = function (url) {
                    if (!url) return;
                    $scope.IsShareQrContainer = true;
                    $scope.shareUrl = url;
                    document.getElementById("shareQrContainer").innerHTML = "";
                    var qrcode = new QRCode(document.getElementById("shareQrContainer"), {
                        width: 250,
                        height: 250
                    });
                    qrcode.clear();
                    qrcode.makeCode(url);
                }
                $scope.copyShareUrl = function () {
                    var errorInput = document.getElementById("shareQrId");
                    errorInput.select();
                    try {
                        document.execCommand("Copy");
                        sogModal.openAlertDialog("复制成功", "复制成功");
                    } catch (e) {
                        sogModal.openAlertDialog("友情提示", "复制失败, 请选中内容手动复制。");
                    }
                }
            });
    });