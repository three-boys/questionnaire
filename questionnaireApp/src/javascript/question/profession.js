(function (Seagull2Question) { 

    var ProfessionOption = (function (_super) {
        Seagull2Question.__extends(ProfessionOption, _super);
        function ProfessionOption() {
            _super.call(this);            
            
        }
        return ProfessionOption;
    } (Seagull2Question.RadioDownOption));

    var Profession = (function (_super) {
        Seagull2Question.__extends(Profession, _super);
        function Profession() {
            _super.call(this);
            this.title = '您目前从事的职业：'; 
        }

        return Profession;
    } (Seagull2Question.RadioDown));

    Seagull2Question.ProfessionOption = ProfessionOption;
    Seagull2Question.Profession = Profession;
})(window.Seagull2Question || (window.Seagull2Question = {}));