(function (Seagull2Question) {
    var ProvinceCity = (function (_super) {
        Seagull2Question.__extends(ProvinceCity, _super);
        function ProvinceCity(_super) {
            this.templateUrl = './views/design/questions/provinceCity.html';
        }
        return ProvinceCity;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.ProvinceCity = ProvinceCity;
})(window.Seagull2Question || (window.Seagull2Question = {}));