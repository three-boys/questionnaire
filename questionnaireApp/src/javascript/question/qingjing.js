(function (Seagull2Question) {
    var Qingjing = (function (_super) {
        Seagull2Question.__extends(Qingjing, _super);
        function Qingjing(_super) {
            this.templateUrl = './views/design/questions/qingjing.html';
        }
        return Qingjing;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.Qingjing = Qingjing;
})(window.Seagull2Question || (window.Seagull2Question = {}));