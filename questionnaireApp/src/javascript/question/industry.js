(function (Seagull2Question) { 

    var IndustryOption = (function (_super) {
        Seagull2Question.__extends(IndustryOption, _super);
        function IndustryOption() {
            _super.call(this);            
            
        }
        return IndustryOption;
    } (Seagull2Question.RadioDownOption));

    var Industry = (function (_super) {
        Seagull2Question.__extends(Industry, _super);
        function Industry() {
            _super.call(this);
            this.title = '您目前从事的行业：'; 
        }

        return Industry;
    } (Seagull2Question.RadioDown));

    Seagull2Question.IndustryOption = IndustryOption;
    Seagull2Question.Industry = Industry;
})(window.Seagull2Question || (window.Seagull2Question = {}));