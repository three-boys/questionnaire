(function (Seagull2Question) {
    var Address = (function (_super) {
        Seagull2Question.__extends(Address, _super);
        function Address(_super) {
            this.templateUrl = './views/design/questions/address.html';
        }
        return Address;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.Address = Address;
})(window.Seagull2Question || (window.Seagull2Question = {}));