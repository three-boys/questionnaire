// (function (Seagull2Question) {
//     var Phone = (function (_super) {
//         Seagull2Question.__extends(Phone, _super);
//         function Phone(_super) {
//             this.templateUrl = './views/design/questions/phone.html';
//             this.questionType = '手机';
//         }
//         return Phone;
//     } (Seagull2Question.QuestionBase));
//     Seagull2Question.Phone = Phone;
// })(window.Seagull2Question || (window.Seagull2Question = {}));



(function (Seagull2Question) {

    var QuestionOption = (function () {
        function QuestionOption() {
            // this.title = '';
           this.optionProportionHeight='oneline'; 
            this.optionProportionWidth='defaultwidth';
            this.isDefauleUnderLinestyle=false;
            this.underlineStyle='';
            this.isDefauleValue=false;
            this.defaultContent='';
            this.validate='0';
            this.minNumber=0;
            this.maxNumber=0;
            this.notRepeat=false;

        }
        return QuestionOption;
    } ());

    var Phone = (function (_super) {
        Seagull2Question.__extends(Phone, _super);
        function Phone() {
            _super.call(this);
            this.title = '请输入您的手机号码：';
            this.options = [];
            this.templateUrl = './views/design/questions/phone.html';
            this.questionType = '手机';
            this.optionClass = QuestionOption;
            this.questionForm = 'nonselective';
        }

         Phone.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };


        return Phone;
    } (Seagull2Question.QuestionBase));

    Seagull2Question.QuestionOption = QuestionOption;
    Seagull2Question.Phone = Phone;
})(window.Seagull2Question || (window.Seagull2Question = {}));