(function (Seagull2Question) { 

    var GenderOption = (function (_super) {
        Seagull2Question.__extends(GenderOption, _super); 
        function GenderOption() {
            _super.call(this);            
            
        }
        return GenderOption;
    } (Seagull2Question.RadioOption));

    var Gender = (function (_super) {
        Seagull2Question.__extends(Gender, _super);
        function Gender() {
            _super.call(this);
            this.title = '您的性别：';
        }

        return Gender;
    } (Seagull2Question.Radio));

    Seagull2Question.GenderOption = GenderOption;
    Seagull2Question.Gender = Gender;
})(window.Seagull2Question || (window.Seagull2Question = {}));
