(function (Seagull2Question) {
    var QuestionBase = (function () {
        function QuestionBase() { 
            this.editing = false;
            this.title = '请在此输入问题标题';
            this.required = false;
            this.unconditionalJMP = false;
            this.unconditionalJMPTo = undefined;
            this.related = false;
            this.hasReminder = false;
            this.reminder = '';
            this.optionJMP = false;
            this.questionType = '未知';
            this.relevanceQuestion = '';
            this.whetherSelect = 'choice';
            this.logicOption = false;             
        } 

        return QuestionBase;
    } ());
    Seagull2Question.QuestionBase = QuestionBase;
})(window.Seagull2Question || (window.Seagull2Question = {}));