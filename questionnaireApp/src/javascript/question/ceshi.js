(function (Seagull2Question) {

    var CeshiOption = (function (_super) {
        Seagull2Question.__extends(CeshiOption, _super); 
        function CeshiOption() {
            _super.call(this);
            this.isCorrect = false;
 
        }
        return CeshiOption;
    } (Seagull2Question.RadioOption));

    var Ceshi = (function (_super) {
        Seagull2Question.__extends(Ceshi, _super);
        function Ceshi() {
            _super.call(this);
            this.templateUrl = './views/design/questions/ceshi.html';
            this.questionType = '考试单选题';
            this.selectivity = 'single';
            this.category='exam';
            this.optionClass = CeshiOption;
            this.maxSelected = undefined;
            this.questionForm = 'choiceQuestion';
            this.score = '5';
        }

        Ceshi.prototype.setDefault = function (option, val) {
            option.isDefault = val;
        };

        Ceshi.prototype.setCorrect = function (option, val) {
            if (val === false) {
                option.isCorrect = val;
            } else if (val === true) {
                for (var i = 0; i < this.options.length; i++) {
                    this.options[i].isCorrect = this.options[i] === option;
                }
            }
        };

        return Ceshi;
    } (Seagull2Question.Radio));

    Seagull2Question.CeshiOption = CeshiOption;
    Seagull2Question.Ceshi = Ceshi;
})(window.Seagull2Question || (window.Seagull2Question = {}));