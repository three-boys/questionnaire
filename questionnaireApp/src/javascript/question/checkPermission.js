// (function (Seagull2Question) {
//     var CheckPermission = (function (_super) {
//         Seagull2Question.__extends(CheckPermission, _super);
//         function CheckPermission(_super) {
//             this.templateUrl = './views/design/questions/checkPermission.html';
//             this.questionType = '考试简答';
//         }
//         return CheckPermission;
//     } (Seagull2Question.QuestionBase));
//     Seagull2Question.CheckPermission = CheckPermission;
// })(window.Seagull2Question || (window.Seagull2Question = {}));


(function (Seagull2Question) {

    var QuestionOption = (function () {
        function QuestionOption() {
            // this.title = '';
            this.optionProportionHeight = 'oneline';
            this.optionProportionWidth = 'defaultwidth';
            this.isDefauleUnderLinestyle = false;
            this.underlineStyle = '';
            this.isDefauleValue = false;
            this.defaultContent = '';
            this.isContainsAnswer = false;
            this.theAnswer = '';

        }
        return QuestionOption;
    } ());

    var CheckPermission = (function (_super) {
        Seagull2Question.__extends(CheckPermission, _super);
        function CheckPermission() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/design/questions/checkPermission.html';
            this.questionType = '考试简答';
            this.optionClass = QuestionOption;
            this.category = 'exam';
            this.questionForm = 'nonselective';
            this.score = '5';
        }

        CheckPermission.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };


        return CheckPermission;
    } (Seagull2Question.QuestionBase));

    Seagull2Question.QuestionOption = QuestionOption;
    Seagull2Question.CheckPermission = CheckPermission;
})(window.Seagull2Question || (window.Seagull2Question = {}));






