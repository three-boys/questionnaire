(function (Seagull2Question) {
    var MatrixComboBox = (function (_super) {
        Seagull2Question.__extends(MatrixComboBox, _super);
        function MatrixComboBox(_super) {
            this.templateUrl = './views/design/questions/matrixComboBox.html';
        }
        return MatrixComboBox;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.MatrixComboBox = MatrixComboBox;
})(window.Seagull2Question || (window.Seagull2Question = {}));