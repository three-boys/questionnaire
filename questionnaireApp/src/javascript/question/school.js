(function (Seagull2Question) {
    var School = (function (_super) {
        Seagull2Question.__extends(School, _super);
        function School(_super) {
            this.templateUrl = './views/design/questions/school.html';
        }
        return School;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.School = School;
})(window.Seagull2Question || (window.Seagull2Question = {}));