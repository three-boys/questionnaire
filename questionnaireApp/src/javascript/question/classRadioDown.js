(function (Seagull2Question) {
    var ClassRadioDown = (function (_super) {
        Seagull2Question.__extends(ClassRadioDown, _super);
        function ClassRadioDown(_super) {
            this.templateUrl = './views/design/questions/classRadioDown.html';
        }
        return ClassRadioDown;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.ClassRadioDown = ClassRadioDown;
})(window.Seagull2Question || (window.Seagull2Question = {}));