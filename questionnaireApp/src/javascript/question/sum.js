(function (Seagull2Question) {
    var Sum = (function (_super) {
        Seagull2Question.__extends(Sum, _super);
        function Sum(_super) {
            this.templateUrl = './views/design/questions/sum.html';
        }
        return Sum;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.Sum = Sum;
})(window.Seagull2Question || (window.Seagull2Question = {}));