(function (Seagull2Question) {
    var PhoneCheckout = (function (_super) {
        Seagull2Question.__extends(PhoneCheckout, _super);
        function PhoneCheckout(_super) {
            this.templateUrl = './views/design/questions/phoneCheckout.html';
        }
        return PhoneCheckout;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.PhoneCheckout = PhoneCheckout;
})(window.Seagull2Question || (window.Seagull2Question = {}));