(function (Seagull2Question) {
    var City = (function (_super) {
        Seagull2Question.__extends(City, _super);
        function City(_super) {
            this.templateUrl = './views/design/questions/city.html';
        }
        return City;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.City = City;
})(window.Seagull2Question || (window.Seagull2Question = {}));