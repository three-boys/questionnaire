(function (Seagull2Question) {

    var CheckOption = (function (_super) {
        Seagull2Question.__extends(CheckOption, _super);
        function CheckOption() {
            _super.call(this);
            this.isMutual = false;
        }
        return CheckOption;
    } (Seagull2Question.RadioOption));

    var Check = (function (_super) {
        Seagull2Question.__extends(Check, _super);
        function Check() {
            _super.call(this);
            this.templateUrl = './views/design/questions/check.html';
            this.questionType = '多选题';
            this.selectivity = 'multiple';
            this.optionClass = CheckOption;
            this.maxSelected = undefined;
            this.atLeast = '';
            this.atMost = '';
            this.questionForm = 'choiceQuestion';
            this.sizeHint = false;
        }

        Check.prototype.setDefault = function (option, val) {
            option.isDefault = val;
        };

        Check.prototype.setOptionSize = function () {
            if (this.atMost === '') {
                return true;
            } else if (this.atMost < this.atLeast) {
                this.atLeast = this.atMost;
                this.sizeHint = true;
            }
        };

        return Check;
    } (Seagull2Question.Radio));

    Seagull2Question.CheckOption = CheckOption;
    Seagull2Question.Check = Check;
})(window.Seagull2Question || (window.Seagull2Question = {}));