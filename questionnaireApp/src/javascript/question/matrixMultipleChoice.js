(function (Seagull2Question) {
    var MatrixMultipleChoice = (function (_super) {
        Seagull2Question.__extends(MatrixMultipleChoice, _super);
        function MatrixMultipleChoice(_super) {
            this.templateUrl = './views/design/questions/matrixMultipleChoice.html';
        }
        return MatrixMultipleChoice;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.MatrixMultipleChoice = MatrixMultipleChoice;
})(window.Seagull2Question || (window.Seagull2Question = {}));