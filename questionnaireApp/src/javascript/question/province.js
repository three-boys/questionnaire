(function (Seagull2Question) {

    var ProvinceOption = (function (_super) {
        Seagull2Question.__extends(ProvinceOption, _super);
        function ProvinceOption() {
            _super.call(this);            
            this.alloweFillIn = false;
            this.requiredFillIn = false; 
            this.reminder = '';            
        }
        return ProvinceOption;
    } (Seagull2Question.RadioOption));

    var Province = (function (_super) {
        Seagull2Question.__extends(Province, _super);
        function Province() {
            _super.call(this);
            this.templateUrl = './views/design/questions/radio.html';
            this.questionType = '单选题';
            this.title = '您所在的省份：';
            this.selectivity = 'single';
            this.optionClass = ProvinceOption;
            this.optionProportion='arrangeEight';
            this.questionForm = 'choiceQuestion'; 
        }

        return Province;
    } (Seagull2Question.Radio));

    Seagull2Question.ProvinceOption = ProvinceOption;
    Seagull2Question.Province = Province;
})(window.Seagull2Question || (window.Seagull2Question = {}));