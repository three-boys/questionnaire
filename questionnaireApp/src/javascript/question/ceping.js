(function (Seagull2Question) {

    var CepingOption = (function (_super) {
        Seagull2Question.__extends(CepingOption, _super); 
        function CepingOption() {
            _super.call(this); 
            this.title='选项';
            this.noRecord = false; 
            this.score = '5';
        } 
        return CepingOption;
    } (Seagull2Question.RadioOption));

    var Ceping = (function (_super) {
        Seagull2Question.__extends(Ceping, _super);
        function Ceping() {
            _super.call(this);
            this.templateUrl = './views/design/questions/ceping.html';
            this.questionType = '评分单选题';
            this.selectivity = 'single';
            this.optionClass = CepingOption;
            this.maxSelected = undefined;
            this.questionForm = 'choiceQuestion';
        }

        Ceping.prototype.setDefault = function (option, val) {
            option.isDefault = val;
        };

        Ceping.prototype.setRecord = function (option, val) {
            option.noRecord = val;           
            option.score = undefined;
        };


        return Ceping;
    } (Seagull2Question.Radio));

    Seagull2Question.CepingOption = CepingOption;
    Seagull2Question.Ceping = Ceping;
})(window.Seagull2Question || (window.Seagull2Question = {}));