
(function (Seagull2Question) {

    var CheckBasicMessageOption = (function () {
        function CheckBasicMessageOption() {
             this.title = '标题';
             this.rightTitle = '';
             this.rightInputWidth='leftwidthtwenty';
             this.validate='0';
             this.minNumber=0;
            this.maxNumber=0; 
        }
        return CheckBasicMessageOption;
    } ());

    var CheckBasicMessage = (function (_super) {
        Seagull2Question.__extends(CheckBasicMessage, _super);
        function CheckBasicMessage() {
            _super.call(this);
            this.title = '基本信息：';
            this.options = [];
            this.titleAllWidth='widthonehundred';
            this.titleWidth='leftwidththirty';
            this.rightTitleWidth='leftwidththirty';
            this.templateUrl = './views/design/questions/CheckBasicMessage.html';
            this.questionType = '基本信息';
            this.optionClass = CheckBasicMessageOption;
            this.questionForm = 'nonselective';
        }

        CheckBasicMessage.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };



        return CheckBasicMessage;
    } (Seagull2Question.QuestionBase));

    Seagull2Question.CheckBasicMessageOption = CheckBasicMessageOption;
    Seagull2Question.CheckBasicMessage = CheckBasicMessage;
})(window.Seagull2Question || (window.Seagull2Question = {}));