(function (Seagull2Question) {
    var MatrixText = (function (_super) {
        Seagull2Question.__extends(MatrixText, _super);
        function MatrixText(_super) {
            this.templateUrl = './views/design/questions/matrixText.html';
        }
        return MatrixText;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.MatrixText = MatrixText;
})(window.Seagull2Question || (window.Seagull2Question = {}));