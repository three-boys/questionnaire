(function (Seagull2Question) {

    var CeshiMultipleChoicesOption = (function (_super) {
        Seagull2Question.__extends(CeshiMultipleChoicesOption, _super); 
        function CeshiMultipleChoicesOption() {
            _super.call(this);
            this.isCorrect = false;  
            this.title = '选项';
        }
        return CeshiMultipleChoicesOption;
    } (Seagull2Question.Radio)); 

    var CeshiMultipleChoices = (function (_super) {  
        Seagull2Question.__extends(CeshiMultipleChoices, _super);
        function CeshiMultipleChoices() {
            _super.call(this);
            this.templateUrl = './views/design/questions/ceshiMultipleChoices.html';
            this.questionType = '考试多选题';
            this.selectivity = 'multiple';
            this.category='exam';
            this.optionClass = CeshiMultipleChoicesOption;
            this.maxSelected = undefined;
            this.questionForm = 'choiceQuestion';
            this.score = '5';
        }

        CeshiMultipleChoices.prototype.setDefault = function (option, val) {
            option.isDefault = val;
        };

        CeshiMultipleChoices.prototype.setCorrect = function (option, val) {
            option.isCorrect = val;
        };

        return CeshiMultipleChoices;
    } (Seagull2Question.Radio));

    Seagull2Question.CeshiMultipleChoicesOption = CeshiMultipleChoicesOption;
    Seagull2Question.CeshiMultipleChoices = CeshiMultipleChoices;
})(window.Seagull2Question || (window.Seagull2Question = {}));