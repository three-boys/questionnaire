
(function (seagull2Question) {

    var RadioDownOption = (function () {
        function RadioDownOption() {
            this.title = '选项';
            this.optionJMPTo = 0;
            this.isDefault = false;
            this.optionGapFilling = '';
        }
        return RadioDownOption;
    } ());

    var RadioDown = (function (_super) {
        seagull2Question.__extends(RadioDown, _super);
        function RadioDown() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/design/questions/radioDown.html';
            this.questionType = '下拉框单选';
            this.selectivity = 'single';
            this.optionProportion = 'arrangeOne'
            this.optionClass = RadioDownOption;
            this.questionForm = 'choiceQuestion';
        }

        RadioDown.prototype.removeOption = function (option) {
            var index = this.options.indexOf(option);
            if (index >= 0) {
                this.options.splice(index, 1);
                return true;
            }
            return false;
        };

        RadioDown.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };

        RadioDown.prototype.remove = function (option) {
            return seagull2Question.arrayHelper.__remove(this.options, option);
        };

        RadioDown.prototype.moveUp = function (option) {
            return seagull2Question.arrayHelper.__moveUp(this.options, option);
        };

        RadioDown.prototype.moveDown = function (option) {
            return seagull2Question.arrayHelper.__moveDown(this.options, option);
        };

        RadioDown.prototype.setDefault = function (option, val) {

            if (val === false) {
                option.isDefault = val;
            } else if (val === true) {
                for (var i = 0; i < this.options.length; i++) {
                    this.options[i].isDefault = this.options[i] === option;
                }
            }
        };

        RadioDown.prototype.eliminateDefault = function () {
            for (var i = 0; i < this.options.length; i++) {
                this.options[i].isDefault = false;
            }
        };



        return RadioDown;
    } (seagull2Question.QuestionBase));

    seagull2Question.RadioDownOption = RadioDownOption;
    seagull2Question.RadioDown = RadioDown;
})(window.Seagull2Question || (window.Seagull2Question = {}));