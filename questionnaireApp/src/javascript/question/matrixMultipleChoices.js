(function (Seagull2Question) {
    var MatrixMultipleChoices = (function (_super) {
        Seagull2Question.__extends(MatrixMultipleChoices, _super);
        function MatrixMultipleChoices(_super) {
            this.templateUrl = './views/design/questions/matrixMultipleChoices.html';
        }
        return MatrixMultipleChoices;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.MatrixMultipleChoices = MatrixMultipleChoices;
})(window.Seagull2Question || (window.Seagull2Question = {}));