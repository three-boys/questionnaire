(function (Seagull2Question) {
    var MatrixScale = (function (_super) {
        Seagull2Question.__extends(MatrixScale, _super);
        function MatrixScale(_super) {
            this.templateUrl = './views/design/questions/matrixScale.html';
        }
        return MatrixScale;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.MatrixScale = MatrixScale;
})(window.Seagull2Question || (window.Seagull2Question = {}));