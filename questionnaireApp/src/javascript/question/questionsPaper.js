(function (Seagull2Question) {

    var questionConstructor = {};

    questionConstructor.radio = function () {
        var result = new Seagull2Question.Radio();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.check = function () {
        var result = new Seagull2Question.Check();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.question = function () {
        var result = new Seagull2Question.Question();
        result.addOption();
        return result;
    };

    questionConstructor.gapfill = function () {
        var result = new Seagull2Question.Gapfill();
        return result;
    };

    questionConstructor.matrix = function () {
        var result = new Seagull2Question.Matrix();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.matrixScale = function () {
        var result = new Seagull2Question.MatrixScale();
        return result;
    };

    questionConstructor.matrixMultipleChoice = function () {
        var result = new Seagull2Question.MatrixMultipleChoice();
        return result;
    };

    questionConstructor.matrixMultipleChoices = function () {
        var result = new Seagull2Question.MatrixMultipleChoices();
        return result;
    };

    questionConstructor.matrixSlider = function () {
        var result = new Seagull2Question.MatrixSlider();
        return result;
    };

    questionConstructor.matrixComboBox = function () {
        var result = new Seagull2Question.MatrixComboBox();
        return result;
    };

    questionConstructor.matrixNumerical = function () {
        var result = new Seagull2Question.MatrixNumerical();
        return result;
    };

    questionConstructor.matrixText = function () {
        var result = new Seagull2Question.MatrixText();
        return result;
    };

    questionConstructor.fileupload = function () { 
        var result = new Seagull2Question.Fileupload();
        // result.addOption().pictureExtension = ['.gif', '.png', '.jpg', '.jpeg', '.bmp', '.doc', '.docx', '.pdf', '.xls', '.xlsx', '.ppt', '.pptx', '.txt' ,'.rar', '.zip', '.gzip'];
        // result.addOption().pictureExtension = ['.doc', '.docx', '.pdf', '.xls', '.xlsx', '.ppt', '.pptx', '.txt'];
        // result.addOption().pictureExtension = ['.rar', '.zip', '.gzip'];
        var extensions = [
            { name: " 图片文件", values: ['.gif', '.png', '.jpg', '.jpeg', '.bmp'] },
            { name: " 文档文件", values: ['.doc', '.docx', '.pdf', '.xls', '.xlsx', '.ppt', '.pptx', '.txt'] },
            { name: " 压缩文件", values: ['.rar', '.zip', '.gzip'] }
        ];
        result.addOption().pictureExtension = extensions;
        return result;
    };

    questionConstructor.likert = function () {
        var result = new Seagull2Question.Likert();
        result.addOption().title = '很不满意';
        result.addOption().title = '不满意';
        result.addOption().title = '一般';
        result.addOption().title = '满意';
        result.addOption().title = '很满意';
        return result;
    };

    questionConstructor.ceping = function () {
        var result = new Seagull2Question.Ceping();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.cepings = function () {
        var result = new Seagull2Question.Cepings();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.ceshi = function () {
        var result = new Seagull2Question.Ceshi();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.ceshiJudge = function () {
        var result = new Seagull2Question.Ceshi();
        result.addOption().title = '对';
        result.addOption().title = '错';
        return result;
    };

    questionConstructor.ceshiMultipleChoices = function () {
        var result = new Seagull2Question.CeshiMultipleChoices();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.ceshiGapfill = function () {
        var result = new Seagull2Question.CeshiGapfill();
        result.addOption();
        return result;
    };

    questionConstructor.ceshiQuestion = function () {
        var result = new Seagull2Question.CeshiQuestion();
        result.addOption();
        return result;
    };

    questionConstructor.checkPermission = function () {
        var result = new Seagull2Question.CheckPermission();
        result.addOption();
        return result;
    };

    questionConstructor.name = function () {
        var result = new Seagull2Question.Name();
        result.addOption();
        return result;
    };

    questionConstructor.checkBasicMessage = function () {
        var result = new Seagull2Question.CheckBasicMessage();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.voteRadio = function () {
        var result = new Seagull2Question.VoteRadio();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.voteCheck = function () {
        var result = new Seagull2Question.VoteCheck();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.page = function () {
        var result = new Seagull2Question.Page();
        return result;
    };

    questionConstructor.cut = function () {
        var result = new Seagull2Question.Cut();
        return result;
    };

    questionConstructor.sort = function () {
        var result = new Seagull2Question.Sort();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.sum = function () {
        var result = new Seagull2Question.Sum();
        return result;
    };

    questionConstructor.slider = function () {
        var result = new Seagull2Question.Slider();
        return result;
    };

    questionConstructor.qingjing = function () {
        var result = new Seagull2Question.Qingjing();
        return result;
    };

    questionConstructor.shop = function () {
        var result = new Seagull2Question.Shop();
        return result;
    };

    questionConstructor.radioDown = function () {
        var result = new Seagull2Question.RadioDown();
        result.addOption();
        result.addOption();
        return result;
    };

    questionConstructor.classRadioDown = function () {
        var result = new Seagull2Question.ClassRadioDown();
        return result;
    };

    questionConstructor.gender = function () {
        var result = new Seagull2Question.Gender();
        result.addOption().title = '女';
        result.addOption().title = '男';
        return result;
    };

    questionConstructor.age = function () {
        var result = new Seagull2Question.Age();
        result.addOption().title = '18岁以下';
        result.addOption().title = '18~25';
        result.addOption().title = '26~30';
        result.addOption().title = '31~40';
        result.addOption().title = '41~50';
        result.addOption().title = '51~60';
        result.addOption().title = '60以上';
        return result;
    };

    questionConstructor.province = function () {
        var result = new Seagull2Question.Province();
        result.addOption().title = '安徽';
        result.addOption().title = '北京';
        result.addOption().title = '重庆';
        result.addOption().title = '福建';
        result.addOption().title = '甘肃';
        result.addOption().title = '广东';
        result.addOption().title = '广西';
        result.addOption().title = '贵州';
        result.addOption().title = '海南';
        result.addOption().title = '河北';
        result.addOption().title = '黑龙江';
        result.addOption().title = '河南';
        result.addOption().title = '香港';
        result.addOption().title = '湖北';
        result.addOption().title = '湖南';
        result.addOption().title = '江苏';
        result.addOption().title = '江西';
        result.addOption().title = '吉林';
        result.addOption().title = '辽宁';
        result.addOption().title = '澳门';
        result.addOption().title = '内蒙古';
        result.addOption().title = '宁夏';
        result.addOption().title = '青海';
        result.addOption().title = '山东';
        result.addOption().title = '上海';
        result.addOption().title = '山西';
        result.addOption().title = '陕西';
        result.addOption().title = '四川';
        result.addOption().title = '台湾';
        result.addOption().title = '天津';
        result.addOption().title = '新疆';
        result.addOption().title = '西藏';
        result.addOption().title = '云南';
        result.addOption().title = '浙江';
        result.addOption().title = '海外';
        return result;
    };

    questionConstructor.city = function () {
        var result = new Seagull2Question.City();
        return result;
    };

    questionConstructor.provinceCity = function () {
        var result = new Seagull2Question.ProvinceCity();
        return result;
    };

    questionConstructor.phone = function () {
        var result = new Seagull2Question.Phone();
        result.addOption();
        return result;
    };

    questionConstructor.phoneCheckout = function () {
        var result = new Seagull2Question.PhoneCheckout();
        return result;
    };

    questionConstructor.email = function () {
        var result = new Seagull2Question.Email();
        result.addOption();
        return result;
    };

    questionConstructor.date = function () {
        var result = new Seagull2Question.Date();
        return result;
    };

    questionConstructor.map = function () {
        var result = new Seagull2Question.Map();
        return result;
    };

    questionConstructor.profession = function () {
        var result = new Seagull2Question.Profession();
        result.addOption().title = '全日制学生';
        result.addOption().title = '生产人员';
        result.addOption().title = '销售人员';
        result.addOption().title = '市场/公关人员';
        result.addOption().title = '客服人员';
        result.addOption().title = '行政/后勤人员';
        result.addOption().title = '人力资源';
        result.addOption().title = '财务/审计人员';
        result.addOption().title = '文职/办事人员';
        result.addOption().title = '技术/研发人员';
        result.addOption().title = '管理人员';
        result.addOption().title = '教师';
        result.addOption().title = '顾问/咨询';
        result.addOption().title = '专业人士(如会计师、律师、建筑师、医护人员、记者等)';
        result.addOption().title = '其他';
        return result;
    };

    questionConstructor.industry = function () {
        var result = new Seagull2Question.Industry();
        result.addOption().title = 'IT/软硬件服务/电子商务/因特网运营';
        result.addOption().title = '快速消费品(食品/饮料/化妆品)';
        result.addOption().title = '批发/零售';
        result.addOption().title = '服装/纺织/皮革';
        result.addOption().title = '家具/工艺品/玩具';
        result.addOption().title = '教育/培训/科研/院校';
        result.addOption().title = '家电';
        result.addOption().title = '通信/电信运营/网络设备/增值服务';
        result.addOption().title = '制造业';
        result.addOption().title = '汽车及零配件';
        result.addOption().title = '餐饮/娱乐/旅游/酒店/生活服务';
        result.addOption().title = '办公用品及设备';
        result.addOption().title = '会计/审计';
        result.addOption().title = '法律';
        result.addOption().title = '银行/保险/证券/投资银行/风险基金';
        result.addOption().title = '电子技术/半导体/集成电路';
        result.addOption().title = '仪器仪表/工业自动化';
        result.addOption().title = '贸易/进出口';
        result.addOption().title = '机械/设备/重工';
        result.addOption().title = '制药/生物工程/医疗设备/器械';
        result.addOption().title = '医疗/护理/保健/卫生';
        result.addOption().title = '广告/公关/媒体/艺术';
        result.addOption().title = '出版/印刷/包装';
        result.addOption().title = '房地产开发/建筑工程/装潢/设计';
        result.addOption().title = '物业管理/商业中心';
        result.addOption().title = '中介/咨询/猎头/认证';
        result.addOption().title = '交通/运输/物流';
        result.addOption().title = '航天/航空/能源/化工';
        result.addOption().title = '农业/渔业/林业';
        result.addOption().title = '其他行业'; 
        return result;
    };

    questionConstructor.school = function () {
        var result = new Seagull2Question.School();
        return result;
    };

    questionConstructor.address = function () {
        var result = new Seagull2Question.Address();
        return result;
    };

    function clone(obj) {
        // Handle the 3 simple types, and null or undefined
        if (null === obj || "object" != typeof obj) return obj;

        // Handle Date
        if (obj instanceof Date) {
            var copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }

        // Handle Array
        if (obj instanceof Array) {
            var copyArray = [];
            for (var i = 0, len = obj.length; i < len; ++i) {
                copyArray[i] = clone(obj[i]);
            }
            return copyArray;
        }

        // Handle Object
        if (obj instanceof Object) {
            var copyObject = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copyObject[attr] = clone(obj[attr]);
            }
            return copyObject;
        }

        throw new Error("Unable to copy obj! Its type isn't supported.");
    }

    var QuestionsPaper = (function () {
        function QuestionsPaper() {
            this.title = '';
            this.summary = '';
            this.questions = [];
        }

        QuestionsPaper.prototype.addQuestion = function (questionType) {

            var constructor = questionConstructor[questionType];
            if (constructor) {
                var item = constructor();
                this.questions.push(item);
                if (this.questions.length !== 1) {
                    this.beginEdit(item);
                }
            };
        };

        QuestionsPaper.prototype.beginEdit = function (question) {
            for (var i = 0; i < this.questions.length; i++) {
                this.questions[i].editing = this.questions[i] === question;
            }
        };

        QuestionsPaper.prototype.endEdit = function (question) {

            if (question) {
                question.editing = false;
                return true;
            }

            return false;
        };

        QuestionsPaper.prototype.copy = function (question) {
            var index = this.questions.indexOf(question);
            if (index >= 0) {
                var cloneObject = clone(question);
                this.questions.splice(index + 1, 0, cloneObject);
                this.beginEdit(cloneObject);
                return true;
            }
            return false;
        };

        QuestionsPaper.prototype.remove = function (question) {
            return Seagull2Question.arrayHelper.__remove(this.questions, question);
        };

        QuestionsPaper.prototype.moveUp = function (question) {
            var index = this.questions.indexOf(question);
            if (index > 1) {
                var question = this.questions[index];
                this.questions[index] = this.questions[index - 1];
                this.questions[index - 1] = question;
                return true;
            }
            return false;
        };

        QuestionsPaper.prototype.moveDown = function (question) {
            var index = this.questions.indexOf(question);
            if (index > 0 && index !== (this.questions.length - 1)) {
                var question = this.questions[index];
                this.questions[index] = this.questions[index + 1];
                this.questions[index + 1] = question;
                return true;
            }
            return false;
            // return Seagull2Question.arrayHelper.__moveDown(this.questions, question);
        };

        QuestionsPaper.prototype.moveToFirst = function (question) {
            var index = this.questions.indexOf(question);
            if (index > 0) {
                // this.questions.unshift(this.questions[0],this.questions[index]);
                this.questions.splice(1, 0, this.questions[index]);
                this.questions.splice(index + 1, 1);
                return true;
            }
            return false;
        };

        QuestionsPaper.prototype.moveToLast = function (question) {
            var index = this.questions.indexOf(question);
            if (index > 0) {
                this.questions.push(this.questions[index]);
                this.questions.splice(index, 1);
                return true;
            }
            return false;
        };

        QuestionsPaper.prototype.setAllRequired = function (required) {
            for (var i = 0; i < this.questions.length; i++) {
                this.questions[i].required = required;
            }
        };

        QuestionsPaper.prototype.opts = {
            min: 0,
            max: 99999,
            precision: 0.1
        };










        return QuestionsPaper;
    } ());
    Seagull2Question.QuestionsPaper = QuestionsPaper;
})(Seagull2Question || (Seagull2Question = {}));