(function (Seagull2Question) {

    var VoteCheckOption = (function (_super) {
        Seagull2Question.__extends(VoteCheckOption, _super); 
        function VoteCheckOption() {
            _super.call(this);

        }
        return VoteCheckOption;
    } (Seagull2Question.RadioOption));

    var VoteCheck = (function (_super) { 
        Seagull2Question.__extends(VoteCheck, _super); 
        function VoteCheck() {
            _super.call(this);
            this.templateUrl = './views/design/questions/voteCheck.html';
            this.questionType = '投票多选题';
            this.selectivity = 'multiple';
            this.category='vote';
            this.optionClass = VoteCheckOption;
            this.maxSelected = undefined;
            this.pollNumber = true;
            this.percentage = true; 
            this.atLeast = '';
            this.atMost = '';
            this.questionForm = 'choiceQuestion';
            this.sizeHint = false;
        }

        VoteCheck.prototype.setDefault = function (option, val) {
            option.isDefault = val;
        };

        VoteCheck.prototype.setOptionSize = function () {
            if(this.atMost === ''){
                return true;
            }else if(this.atMost<this.atLeast){
                this.atLeast = this.atMost;
                this.sizeHint = true;
            }           
        };

        return VoteCheck;
    } (Seagull2Question.Radio));

    Seagull2Question.VoteCheckOption = VoteCheckOption;
    Seagull2Question.VoteCheck = VoteCheck;
})(window.Seagull2Question || (window.Seagull2Question = {}));

