(function (Seagull2Question) {
    var Gapfill = (function (_super) {
        Seagull2Question.__extends(Gapfill, _super);
        function Gapfill(_super) {
            this.templateUrl = './views/design/questions/gapfill.html';
        }
        return Gapfill;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.Gapfill = Gapfill;
})(window.Seagull2Question || (window.Seagull2Question = {}));