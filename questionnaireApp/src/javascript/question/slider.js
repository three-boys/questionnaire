(function (Seagull2Question) {
    var Slider = (function (_super) {
        Seagull2Question.__extends(Slider, _super);
        function Slider() {
            _super.call(this);
            this.templateUrl = './views/design/questions/slider.html';
            this.questionType = '滚动条';
            this.minValue = '0';
            this.maxValue = '100';
            this.minValueShow = '不满意';
            this.maxValueShow = '满意';
            this.questionForm = 'nonselective';
        }
        return Slider;
    } (Seagull2Question.QuestionBase));

    Seagull2Question.Slider = Slider;
})(window.Seagull2Question || (window.Seagull2Question = {}));