
(function (Seagull2Question) {

    var QuestionOption = (function () {
        function QuestionOption() {
            // this.title = ''; 
            this.optionProportionHeight='oneline'; 
            this.optionProportionWidth='defaultwidth';
            this.isDefauleUnderLinestyle=false;
            this.underlineStyle='';
            this.isDefauleValue=false;
            this.defaultContent='';

        }
        return QuestionOption;
    } ());

    var Name = (function (_super) {
        Seagull2Question.__extends(Name, _super);
        function Name() {
            _super.call(this);
            this.title = '您的姓名：'; 
            this.options = [];
            this.templateUrl = './views/design/questions/name.html';
            this.questionType = '考试姓名';
            this.optionClass = QuestionOption;
            this.questionForm = 'nonselective';
        }

         Name.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };


        return Name;
    } (Seagull2Question.QuestionBase));

    Seagull2Question.QuestionOption = QuestionOption;
    Seagull2Question.Name = Name;
})(window.Seagull2Question || (window.Seagull2Question = {}));






