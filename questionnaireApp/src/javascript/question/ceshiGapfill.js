(function (Seagull2Question) {

    var QuestionOption = (function () {
        function QuestionOption() {
            this.title = '您的问题：';
           this.theAnswer=''; 
           this.score=5;
           this.isContainsAnswer=false;
           this.rightInputWidth='10';

        }
        return QuestionOption;
    } ());

    var CeshiGapfill = (function (_super) {
        Seagull2Question.__extends(CeshiGapfill, _super);
        function CeshiGapfill() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/design/questions/ceshiGapfill.html';
            this.questionType = '考试多项填空题';
            this.optionClass = QuestionOption;
            this.questionForm = 'nonselective';
        }

         CeshiGapfill.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };
        



        return CeshiGapfill;
    } (Seagull2Question.QuestionBase));

    Seagull2Question.QuestionOption = QuestionOption;
    Seagull2Question.CeshiGapfill = CeshiGapfill;
})(window.Seagull2Question || (window.Seagull2Question = {}));