(function (Seagull2Question) {
    var Map = (function (_super) {
        Seagull2Question.__extends(Map, _super);
        function Map(_super) {
            this.templateUrl = './views/design/questions/map.html';
        }
        return Map;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.Map = Map;
})(window.Seagull2Question || (window.Seagull2Question = {}));