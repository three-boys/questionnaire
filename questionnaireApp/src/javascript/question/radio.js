(function (Seagull2Question) {
    var RadioOption = (function (_super) {
        Seagull2Question.__extends(RadioOption, _super);
        function RadioOption() {
            _super.call(this);
            this.alloweFillIn = false;
            this.requiredFillIn = false;
            this.reminder = '';
        }
        return RadioOption;
    } (Seagull2Question.RadioDownOption));

    var Radio = (function (_super) {
        Seagull2Question.__extends(Radio, _super);
        function Radio() {
            _super.call(this);

            this.templateUrl = './views/design/questions/radio.html';
            this.questionType = '单选题';
            this.selectivity = 'single';
            this.optionClass = RadioOption;
            this.questionNumber = '';
            this.score = '5';
            this.questionForm = 'choiceQuestion';
        }
        return Radio;
    } (Seagull2Question.RadioDown));

    Seagull2Question.RadioOption = RadioOption;
    Seagull2Question.Radio = Radio;
})(window.Seagull2Question || (window.Seagull2Question = {}));