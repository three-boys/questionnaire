(function (Seagull2Question) {
    var MatrixNumerical = (function (_super) {
        Seagull2Question.__extends(MatrixNumerical, _super);
        function MatrixNumerical(_super) {
            this.templateUrl = './views/design/questions/matrixNumerical.html';
        }
        return MatrixNumerical;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.MatrixNumerical = MatrixNumerical;
})(window.Seagull2Question || (window.Seagull2Question = {}));