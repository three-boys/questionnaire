(function (Seagull2Question) {
    var Cut = (function (_super) {
        Seagull2Question.__extends(Cut, _super);
        function Cut() {
            _super.call(this);
            this.templateUrl = './views/design/questions/cut.html';
            this.questionType = '段落说明';
            this.title = '请在此输入说明文字';
            this.questionForm = 'nonselective'; 
        }
        return Cut; 
    } (Seagull2Question.QuestionBase));
    
    Seagull2Question.Cut = Cut;
})(window.Seagull2Question || (window.Seagull2Question = {}));


