(function (Seagull2Question) {

    var SortOption = (function (_super) {
        Seagull2Question.__extends(SortOption, _super);
        function SortOption() {
            _super.call(this);            
            this.isMutual = false;
        }
        return SortOption;
    } (Seagull2Question.RadioDownOption));

    var Sort = (function (_super) {
        Seagull2Question.__extends(Sort, _super);
        function Sort() {
            _super.call(this);
            this.templateUrl = './views/design/questions/sort.html';
            this.questionType = '排序';
            this.optionClass = SortOption;
            this.maxSelected = undefined;
            this.atLeast = '';
            this.atMost = '';
            this.questionForm = 'choiceQuestion';
            this.sizeHint = false;
        }

        Sort.prototype.setDefault = function (option, val) {
            option.isDefault = val;
        };

        Sort.prototype.setOptionSize = function () {
            if(this.atMost === ''){
                return true;
            }else if(this.atMost<this.atLeast){
                this.atLeast = this.atMost;
                this.sizeHint = true;
            }           
        };

        return Sort;
    } (Seagull2Question.RadioDown));

    Seagull2Question.SortOption = SortOption;
    Seagull2Question.Sort = Sort;
})(window.Seagull2Question || (window.Seagull2Question = {}));