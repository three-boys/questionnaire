(function (Seagull2Question) {
    var Page = (function (_super) {
        Seagull2Question.__extends(Page, _super);
        function Page() {
            _super.call(this);
            this.templateUrl = './views/design/questions/page.html';
            this.questionType = '分页';
            this.standingMinTime = ''; 
            this.standingMaxTime = '';
            this.title = '';
            this.questionForm = 'nonselective';
        } 
        return Page;
    } (Seagull2Question.QuestionBase));   
    Seagull2Question.Page = Page;
})(window.Seagull2Question || (window.Seagull2Question = {}));