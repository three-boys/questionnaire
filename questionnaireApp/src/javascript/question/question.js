

(function (Seagull2Question) {

    var QuestionOption = (function () {
        function QuestionOption() {
            // this.title = '';
            this.optionProportionHeight='oneline'; 
            this.optionProportionWidth='defaultwidth';
            this.isDefauleUnderLinestyle=false;
            this.underlineStyle='';
            this.isDefauleValue=false;
            this.defaultContent='';
            this.validate='0'; 
            this.minNumber=0;
            this.maxNumber=0;
            this.notRepeat=false;

        }
        return QuestionOption;
    } ());

    var Question = (function (_super) {
        Seagull2Question.__extends(Question, _super);
        function Question() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/design/questions/question.html';
            this.questionType = '单项填空';
            this.optionClass = QuestionOption;
            this.questionForm = 'nonselective';
        }

         Question.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };


        return Question;
    } (Seagull2Question.QuestionBase));

    Seagull2Question.QuestionOption = QuestionOption;
    Seagull2Question.Question = Question;
})(window.Seagull2Question || (window.Seagull2Question = {}));