(function (Seagull2Question) {
    var MatrixSlider = (function (_super) {
        Seagull2Question.__extends(MatrixSlider, _super);
        function MatrixSlider(_super) {
            this.templateUrl = './views/design/questions/matrixSlider.html';
        }
        return MatrixSlider;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.MatrixSlider = MatrixSlider;
})(window.Seagull2Question || (window.Seagull2Question = {}));