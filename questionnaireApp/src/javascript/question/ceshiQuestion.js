
(function (Seagull2Question) {

    var QuestionOption = (function () {
        function QuestionOption() {
            // this.title = '';
            this.optionProportionHeight='oneline'; 
            this.optionProportionWidth='defaultwidth';
            this.isDefauleUnderLinestyle=false;
            this.underlineStyle='';
            this.isDefauleValue=false;
            this.defaultContent='';
            this.isContainsAnswer=false;
            this.theAnswer='';

        }
        return QuestionOption;
    } ());

    var CeshiQuestion = (function (_super) {
        Seagull2Question.__extends(CeshiQuestion, _super);
        function CeshiQuestion() {
            _super.call(this);
            this.options = [];
            this.templateUrl = './views/design/questions/ceshiQuestion.html';
            this.questionType = '考试单项填空题';
            this.category='exam';
            this.optionClass = QuestionOption;
            this.questionForm = 'nonselective';
            this.score = '5';
        }

         CeshiQuestion.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };


        return CeshiQuestion;
    } (Seagull2Question.QuestionBase));

    Seagull2Question.QuestionOption = QuestionOption;
    Seagull2Question.CeshiQuestion = CeshiQuestion;
})(window.Seagull2Question || (window.Seagull2Question = {}));






