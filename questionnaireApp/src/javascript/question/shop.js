(function (Seagull2Question) {
    var Shop = (function (_super) {
        Seagull2Question.__extends(Shop, _super);
        function Shop(_super) {
            this.templateUrl = './views/design/questions/shop.html';
        }
        return Shop;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.Shop = Shop;
})(window.Seagull2Question || (window.Seagull2Question = {}));