// (function (Seagull2Question) {
//     var Matrix = (function (_super) {
//         Seagull2Question.__extends(Matrix, _super);
//         function Matrix(_super) {
//             this.templateUrl = './views/design/questions/matrix.html';
//             this.questionType = '矩阵填空';
//         }
//         return Matrix; 
//     } (Seagull2Question.QuestionBase));
//     Seagull2Question.Matrix = Matrix;
// })(window.Seagull2Question || (window.Seagull2Question = {}));



(function (Seagull2Question) {

    var MatrixOption = (function () {
        function MatrixOption() {
             this.title = '标题';
             this.rightTitle = '';
             this.rightInputWidth='leftwidthtwenty';
             this.validate='0';
             this.minNumber=0;
            this.maxNumber=0;   
        }
        return MatrixOption;
    } ());

    var Matrix = (function (_super) {
        Seagull2Question.__extends(Matrix, _super);
        function Matrix() {
            _super.call(this);
            this.options = [];
            this.titleAllWidth='widthonehundred';
            this.titleWidth='leftwidththirty';
            this.rightTitleWidth='leftwidththirty';
            this.templateUrl = './views/design/questions/matrix.html';
            this.questionType = '矩阵填空';
            this.optionClass = MatrixOption;
            this.questionForm = 'nonselective';

        }

        Matrix.prototype.addOption = function (index) {
            if (index < 0 || index >= this.options.length) {
                throw "出错啦";
            }

            var result = new this.optionClass();

            if (index || index === 0) {

                this.options.splice(index + 1, 0, result);
            } else {
                this.options.push(result);
            }

            return result;
        };



        return Matrix;
    } (Seagull2Question.QuestionBase));

    Seagull2Question.MatrixOption = MatrixOption;
    Seagull2Question.Matrix = Matrix;
})(window.Seagull2Question || (window.Seagull2Question = {}));