(function (Seagull2Question) {

    var LikertOption = (function (_super) {
        Seagull2Question.__extends(LikertOption, _super);
        function LikertOption() {
            _super.call(this);
            this.noRecord = false;
            this.score = '';
        }
        return LikertOption;
    } (Seagull2Question.RadioOption));

    var Likert = (function (_super) {
        Seagull2Question.__extends(Likert, _super);
        function Likert() {
            _super.call(this);
            this.templateUrl = './views/design/questions/likert.html';
            this.questionType = '量表题';
            this.optionClass = LikertOption;
            this.likertPattern = 'likertPatternNo';
            this.questionForm = 'choiceQuestion';
        }

        Likert.prototype.setRecord = function (option, val) {
            option.noRecord = val;
            option.score = undefined;
        };




        return Likert;
    } (Seagull2Question.Radio));

    Seagull2Question.LikertOption = LikertOption;
    Seagull2Question.Likert = Likert;
})(window.Seagull2Question || (window.Seagull2Question = {}));