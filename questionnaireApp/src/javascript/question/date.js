(function (Seagull2Question) {
    var Date = (function (_super) {
        Seagull2Question.__extends(Date, _super);
        function Date(_super) {
            this.templateUrl = './views/design/questions/date.html';
        }
        return Date;
    } (Seagull2Question.QuestionBase));
    Seagull2Question.Date = Date;
})(window.Seagull2Question || (window.Seagull2Question = {}));