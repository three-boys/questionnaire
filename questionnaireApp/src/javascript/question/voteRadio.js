(function (Seagull2Question) {

    var VoteRadioOption = (function (_super) { 
        Seagull2Question.__extends(VoteRadioOption, _super); 
        function VoteRadioOption() {
            _super.call(this);
            

        }
        return VoteRadioOption;
    } (Seagull2Question.RadioOption)); 

    var VoteRadio = (function (_super) {
        Seagull2Question.__extends(VoteRadio, _super);
        function VoteRadio() {
            _super.call(this);
            this.templateUrl = './views/design/questions/voteRadio.html';
            this.questionType = '投票单选题';
            this.selectivity = 'single';
            this.category='vote';
            this.optionClass = VoteRadioOption;
            this.maxSelected = undefined;
            this.pollNumber = true;
            this.percentage = true;
            this.questionForm = 'choiceQuestion'; 
        }


        return VoteRadio;
    } (Seagull2Question.Radio));

    Seagull2Question.VoteRadioOption = VoteRadioOption;
    Seagull2Question.VoteRadio = VoteRadio;
})(window.Seagull2Question || (window.Seagull2Question = {}));