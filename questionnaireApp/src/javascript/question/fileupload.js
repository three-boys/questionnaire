(function (Seagull2Question) {
    var FileuploadOption = (function () {
        function FileuploadOption() {
            this.pictureExtension = [
                { name: " 图片文件", values: ['.gif', '.png', '.jpg', '.jpeg', '.bmp'] },
                { name: " 文档文件", values: ['.doc', '.docx', '.pdf', '.xls', '.xlsx', '.ppt', '.pptx', '.txt'] },
                { name: " 压缩文件", values: ['.rar', '.zip', '.gzip'] }
            ];
            this.extensionDetail = '';
            this.fileMax = '1024';
        }
        return FileuploadOption;
    } ());

    var Fileupload = (function (_super) {
        Seagull2Question.__extends(Fileupload, _super);
        function Fileupload() {
            _super.call(this);
            this.templateUrl = './views/design/questions/fileupload.html';
            this.questionType = '上传文件';
            this.title = '请上传文件';
            this.optionClass = FileuploadOption;
            this.questionForm = 'fileUpload';
        }
        var indexOf = function (arr, item) {
            if (!arr) {
                return -1;
            }
            for (var i = 0; i < arr.length; i++) {
                if (("." + arr[i]) === item) {
                    return i;
                }
            }
            return -1;
        }
        Fileupload.prototype.allOption = function (allChecked, extensions, option) {
            if (!extensions) return false;
            if (!option.extensionDetail) {
                option.extensionDetail = '';
            }
            if (allChecked == true) {
                option.extensi = true;
                for (var j = 0; j < extensions.length; j++) {
                    var extensionDetailArray = [];
                    if (option.extensionDetail) {
                        extensionDetailArray = option.extensionDetail.split('.');
                    }
                    if (indexOf(extensionDetailArray, extensions[j]) === -1) {
                        if (extensions[j]) {
                            option.extensionDetail += extensions[j];
                        }
                    }
                }
                //option.extensionDetail += extensions.join('');
            } else {
                option.extensi = false;
                for (var i = 0; i < extensions.length; i++) {
                    if (option.extensionDetail) {
                        if (i === extensions.length - 1) {
                            option.extensionDetail = option.extensionDetail.replace(extensions[i], '');
                        }
                        option.extensionDetail = option.extensionDetail.replace(extensions[i] + ".", '.');
                    }
                }
            }
        };

        Fileupload.prototype.optionSelect = function (extension, extensi) {
            if (extensi == true) {
                this.extensionDetail = this.extensionDetail.replace(extension, '');
            } else {
                this.extensionDetail = this.extensionDetail + extension;
            }
        };


        return Fileupload;
    } (Seagull2Question.RadioDown));

    Seagull2Question.FileuploadOption = FileuploadOption;
    Seagull2Question.Fileupload = Fileupload;
})(window.Seagull2Question || (window.Seagull2Question = {}));

