(function (Seagull2Question) {

    var CepingsOption = (function (_super) {
        Seagull2Question.__extends(CepingsOption, _super); 
        function CepingsOption() {
            _super.call(this);
             this.title='选项';
            this.score = '5';

        }
        return CepingsOption;
    } (Seagull2Question.RadioOption));

    var Cepings = (function (_super) {
        Seagull2Question.__extends(Cepings, _super);
        function Cepings() {
            _super.call(this);
            this.templateUrl = './views/design/questions/cepings.html';
            this.questionType = '评分多选题';
            this.selectivity = 'multiple';
            this.optionClass = CepingsOption;
            this.maxSelected = undefined;
            this.atLeast = '';
            this.atMost = '';
            this.questionForm = 'choiceQuestion';
            this.sizeHint = false;
        }

        Cepings.prototype.setDefault = function (option, val) {
            option.isDefault = val;
        };

        Cepings.prototype.setOptionSize = function () {
            if(this.atMost === ''){
                return true;
            }else if(this.atMost<this.atLeast){
                this.atLeast = this.atMost;
                this.sizeHint = true;
            }           
        };

        return Cepings;
    } (Seagull2Question.Radio));

    Seagull2Question.CepingsOption = CepingsOption;
    Seagull2Question.Cepings = Cepings;
})(window.Seagull2Question || (window.Seagull2Question = {}));