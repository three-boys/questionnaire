(function (Seagull2Question) { 

    var AgeOption = (function (_super) {
        Seagull2Question.__extends(AgeOption, _super);
        function AgeOption() {
            _super.call(this);            
            
        }
        return AgeOption;
    } (Seagull2Question.RadioOption));

    var Age = (function (_super) {
        Seagull2Question.__extends(Age, _super);
        function Age() {
            _super.call(this);
            this.title = '您的年龄段：'; 
            this.optionProportion='arrangeEight';
        }

        return Age;
    } (Seagull2Question.Radio));

    Seagull2Question.AgeOption = AgeOption;
    Seagull2Question.Age = Age;
})(window.Seagull2Question || (window.Seagull2Question = {}));