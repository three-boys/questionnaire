angular.module('ng-echarts', [])
    .directive('ngEcharts', [function () {
        return {
            link: function (scope, element, attrs, ctrl) {
                function refreshChart() {
                    var theme = (scope.config && scope.config.theme)
                        ? scope.config.theme : 'default';
                    var chart = echarts.init(element[0], theme);
                    chart.setOption(scope.option);
                    chart.resize();
                };

                if(scope.option){
                    refreshChart();
                }
            },
            scope: {
                option: '=ecOption',
                config: '=ecConfig'
            },
            restrict: 'EA'
        }
    }]);
